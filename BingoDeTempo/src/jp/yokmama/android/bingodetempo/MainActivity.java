package jp.yokmama.android.bingodetempo;

import java.util.List;
import java.util.Random;

import jp.yokmama.android.bingodetempo.bean.HoleItem;
import jp.yokmama.android.bingodetempo.bean.StoreItem;
import jp.yokmama.android.bingodetempo.bingolist.BingoListFragment;
import jp.yokmama.android.bingodetempo.bingolog.LogFragment;
import jp.yokmama.android.bingodetempo.bingomain.BingoFragment;
import jp.yokmama.android.bingodetempo.bingomgr.MgrFragment;
import jp.yokmama.android.bingodetempo.core.Consts;
import jp.yokmama.android.bingodetempo.core.MyPreferenceManager;
import jp.yokmama.android.bingodetempo.core.ServerUtilities;
import jp.yokmama.android.bingodetempo.db.MyContentProvider;
import jp.yokmama.android.bingodetempo.db.TableColumns.HoleLog;
import jp.yokmama.android.bingodetempo.db.TableColumns.Store;
import jp.yokmama.android.bingodetempo.utils.DatabaseHelper;
import jp.yokmama.android.bingodetempo.utils.DialogHelper;
import jp.yokmama.android.bingodetempo.utils.NfcHelper;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.Menu;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {
	private static final String TAG = MainActivity.class.getSimpleName();
	private static final long SPANTIME = 1000 * 60 * 5;

	private NfcHelper mNfc;

	private BingoFragment mBingoFragment;

	private BingoListFragment mBingoLogFragment;

	private LogFragment mLogFragment;

	private MyPreferenceManager mPreference;

	private Handler.Callback mCallback = new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == Consts.MSG_ADMITCOUNT) {
				int count = (Integer) msg.obj;
				mPreference.setAdmitCount(count);
				mPreference.commit();
				if (mBingoFragment != null && mBingoFragment.mHandler != null) {
					mBingoFragment.mHandler
							.sendEmptyMessage(Consts.MSG_RELOAD_LIST);
				}
			}
			return false;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mPreference = new MyPreferenceManager(this);

		if (!mPreference.isInitialized()) {
			Intent intent = new Intent(this, RegistrationActivity.class);
			startActivity(intent);
			finish();
		} else {
			FragmentTabHost host = (FragmentTabHost) findViewById(android.R.id.tabhost);
			host.setup(this, getSupportFragmentManager(), R.id.content);

			TabSpec tabSpec1 = host.newTabSpec("tab1").setIndicator(
					getString(R.string.tab_name_bingo));
			host.addTab(tabSpec1, BingoFragment.class, null);

			TabSpec tabSpec2 = host.newTabSpec("tab2").setIndicator(
					getString(R.string.tab_name_ticket));
			host.addTab(tabSpec2, BingoListFragment.class, null);

			TabSpec tabSpec3 = host.newTabSpec("tab3").setIndicator(
					getString(R.string.tab_name_log));
			host.addTab(tabSpec3, LogFragment.class, null);

			TabSpec tabSpec4 = host.newTabSpec("tab4").setIndicator(
					getString(R.string.tab_name_mgr));
			host.addTab(tabSpec4, MgrFragment.class, null);

			mNfc = new NfcHelper(getApplicationContext());
			if (!mNfc.isOnBoardNfc()) {
				Toast.makeText(getApplicationContext(), "NFC載ってない端末だよ！！",
						Toast.LENGTH_SHORT).show();
			} else if (!mNfc.isEnableNfc(this)) {
				Toast.makeText(getApplicationContext(), "NFC無効になってるよ！！",
						Toast.LENGTH_SHORT).show();
			}

			ServerUtilities.getAdmit(this, new Handler(mCallback));
		}
	}

	@Override
	public void onAttachFragment(Fragment fragment) {
		super.onAttachFragment(fragment);

		if (fragment instanceof BingoFragment) {
			mBingoFragment = (BingoFragment) fragment;
		} else if (fragment instanceof BingoFragment) {
			mLogFragment = (LogFragment) fragment;
		} else if (fragment instanceof BingoListFragment) {
			mBingoLogFragment = (BingoListFragment) fragment;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		mNfc.enable(this, NFCRecieveActivity.class);
		Intent intent = getIntent();
		if (NfcHelper.isNfcIntent(intent)) {
			processIntent(intent);
		}
	}

	@Override
	public void onPause() {
		mNfc.disable(this);
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	private void processIntent(Intent intent) {
		long receiveNfcTime = intent.getLongExtra(Consts.KEY_NFCCHECK_TIME, 0);
		if (receiveNfcTime <= (System.currentTimeMillis() - Consts.NFC_CHECK_TIME_DIFF)) {
			// 読み取り可能時間を過ぎているので無視する
			return;
		}
		String IDm = NfcHelper.getIDm(intent);

		StoreItem store = DatabaseHelper.getStore(this, IDm);
		if (store != null) {
			long now = System.currentTimeMillis();
			if ((now - store.getModifiedDate()) > SPANTIME) {

				ContentValues updateValue = new ContentValues();
				updateValue.put(Store.MODIFIED_DATE, now);
				getContentResolver().update(
						ContentUris.withAppendedId(
								MyContentProvider.STORE_CONTENT_URI,
								store.getId()), updateValue, null, null);

				int count = DatabaseHelper.getCount(this,
						MyContentProvider.HOLELOG_CONTENT_URI, HoleLog.TYPE
								+ " = ?",
						new String[] { Integer.toString(HoleLog.TYPE_BINGO) });
				// 2個以上BINGOだったらリセットする
				if (count > 1) {
					// reset
					getContentResolver().delete(
							MyContentProvider.HOLE_CONTENT_URI, null, null);
					getContentResolver().delete(
							MyContentProvider.HOLELOG_CONTENT_URI, null, null);
					// 再読み込み
					if (mBingoFragment != null) {
						mBingoFragment.mHandler
								.sendEmptyMessage(Consts.MSG_RELOAD_LIST);
					}
				}

				List<HoleItem> holes = DatabaseHelper.selectHoles(this,
						DatabaseHelper.SELECT_HOLE_UNPUNCH);
				if (holes.size() > 0) {
					// 穴の開いていないホールを取得
					Random rand = new Random(System.currentTimeMillis());
					int n = rand.nextInt(holes.size());
					HoleItem item = holes.get(n);

					ServerUtilities.Pool(this, IDm, item.getNo());

					// POOL数を保存
					mPreference.setAdmitCount(mPreference.getAdmitCount() + 1);
					mPreference.commit();

					if (mBingoFragment != null) {
						mBingoFragment.startEntryAnimation();
					}
				}
			} else {
				DialogHelper.NoticeDialog(this, R.drawable.ic_launcher,
						getString(R.string.common_name_notice),
						getString(R.string.error_nfc_interval_short));
			}
		}

		// 後始末
		setIntent(null);
	}
}
