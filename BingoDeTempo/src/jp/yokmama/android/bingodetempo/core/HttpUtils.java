package jp.yokmama.android.bingodetempo.core;

import static jp.yokmama.android.bingodetempo.core.CommonUtilities.TAG;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class HttpUtils {

	public JSONObject getJSON(String path) throws JSONException, IOException {

		InputStream in = null;
		HttpURLConnection conn = null;
		try {
			URL url = new URL(path.toString());
			conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(20000);
			conn.setReadTimeout(20000);
			int responseCode = conn.getResponseCode();
			if (responseCode >= 200 && responseCode <= 206) {
				in = conn.getInputStream();

				String jsonstr = new String(getByteArrayFromStream(in));
				Log.d("KobekonClient", jsonstr);
				return new JSONObject(jsonstr);
			} else {
				throw new IOException("URL Connection Error :" + responseCode);
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	public JSONArray getJSONArray(String path) throws JSONException,
			IOException {

		InputStream in = null;
		HttpURLConnection conn = null;
		try {
			URL url = new URL(path.toString());
			conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(20000);
			conn.setReadTimeout(20000);
			int responseCode = conn.getResponseCode();
			if (responseCode >= 200 && responseCode <= 206) {
				in = conn.getInputStream();

				String jsonstr = new String(getByteArrayFromStream(in));
				return new JSONArray(jsonstr);
			} else {
				throw new IOException("URL Connection Error :" + responseCode);
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	public byte[] getByteArrayFromStream(InputStream in) {
		byte[] line = new byte[512];
		byte[] result = null;
		ByteArrayOutputStream out = null;
		int size = 0;
		try {
			out = new ByteArrayOutputStream();
			BufferedInputStream bis = new BufferedInputStream(in);
			while (true) {
				size = bis.read(line);
				if (size < 0) {
					break;
				}
				out.write(line, 0, size);
			}
			result = out.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null)
					out.close();
			} catch (Exception e) {
			}
		}
		return result;
	}

	public static void post(String endpoint, Map<String, String> params)
			throws IOException {
		URL url;
		try {
			url = new URL(endpoint);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + endpoint);
		}
		StringBuilder bodyBuilder = new StringBuilder();
		if(params !=null){
			Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
			// constructs the POST body using the parameters
			while (iterator.hasNext()) {
				Entry<String, String> param = iterator.next();
				bodyBuilder.append(param.getKey()).append('=')
						.append(param.getValue());
				if (iterator.hasNext()) {
					bodyBuilder.append('&');
				}
			}
		}
		String body = bodyBuilder.toString();
		Log.v(TAG, "Posting '" + body + "' to " + url);
		byte[] bytes = body.getBytes();
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(bytes.length);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded;charset=UTF-8");
			// post the request
			OutputStream out = conn.getOutputStream();
			out.write(bytes);
			out.close();
			// handle the response
			int status = conn.getResponseCode();
			if (status != 200) {
				throw new IOException("Post failed with error code " + status);
			}
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}
}
