package jp.yokmama.android.bingodetempo.core;

public interface MessageCode {

    public static final int MSG_ITEM_ADD = 1000;
    public static final int MSG_CHECKIN = 1001;
    public static final int MSG_USER_ADD = 1002;
    public static final int MSG_PROFILE_GET = 1010;
    public static final int MSG_PROFILE_SET = 1011;
    public static final int MSG_PROFILE_ICON_GET = 1012;
    public static final int MSG_PROFILE_ICON_SET = 1013;
    public static final int MSG_PROFILE_ICON_DELETE = 1014;
    public static final int MSG_CONTACT_ADD = 1015;

}
