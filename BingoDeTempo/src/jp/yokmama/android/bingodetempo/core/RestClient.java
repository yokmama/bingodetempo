
package jp.yokmama.android.bingodetempo.core;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * HttpURLConnection一本にしたい
 */
public class RestClient {
    private static final String TAG = RestClient.class.getSimpleName();
    private static final String BASE_URL = CommonUtilities.HOST;

    private static AsyncHttpClient mClient = new AsyncHttpClient();

    public static void get(String url, RequestParams params,
            AsyncHttpResponseHandler responseHandler) {
        String absUrl = getAbsoluteUrl(url);
        Log.d(TAG, "url=" + url);
        mClient.get(absUrl, params, responseHandler);
    }

    public static void getFull(String url, RequestParams params,
            AsyncHttpResponseHandler responseHandler) {
        Log.d(TAG, "url=" + url);
        mClient.get(url, params, responseHandler);
    }

    public static void post(String url, RequestParams params,
            AsyncHttpResponseHandler responseHandler) {
        Log.d(TAG, "url=" + url);
        mClient.post(getAbsoluteUrl(url), params, responseHandler);
    }
    
    public static void postFull(String url, RequestParams params,
            AsyncHttpResponseHandler responseHandler) {
        Log.d(TAG, "url=" + url);
        mClient.post(url, params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
