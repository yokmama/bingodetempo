
package jp.yokmama.android.bingodetempo.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

public class ImageUtil {
    private static final String TAG = ImageUtil.class.getSimpleName();

    /**
     * ダウンサンプリングしてBitmapを取得する
     * 
     * @param uri
     * @param width
     * @param height
     * @return
     * @throws IOException
     */
    public static Bitmap getDownsamplingBitmap(Context context, Uri uri, int width, int height)
            throws IOException {
        InputStream openInputStream = context.getContentResolver().openInputStream(uri);

        Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeStream(openInputStream, null, options);
        openInputStream.close();

        int scaleW = options.outWidth / width + 1;
        int scaleH = options.outHeight / height + 1;

        // 縮尺は整数値で、2なら画像の縦横のピクセル数を1/2にしたサイズ。
        // 3なら1/3にしたサイズで読み込まれます。
        int scale = Math.max(scaleW, scaleH);

        options.inJustDecodeBounds = false;

        // 先程計算した縮尺値を指定
        options.inSampleSize = scale;
        openInputStream = context.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(openInputStream, null, options);

        return bitmap;
    }

    /**
     * ダウンサンプリングしてBitmapを取得する
     * 
     * @param uri
     * @param width
     * @param height
     * @return
     * @throws IOException
     */
    public static Bitmap getDownsamplingBitmap(Context context, byte[] data, int width, int height)
            throws IOException {
        InputStream openInputStream = new ByteArrayInputStream(data);

        Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeStream(openInputStream, null, options);
        openInputStream.close();

        int scaleW = options.outWidth / width + 1;
        int scaleH = options.outHeight / height + 1;

        // 縮尺は整数値で、2なら画像の縦横のピクセル数を1/2にしたサイズ。
        // 3なら1/3にしたサイズで読み込まれます。
        int scale = Math.max(scaleW, scaleH);

        options.inJustDecodeBounds = false;

        // 先程計算した縮尺値を指定
        options.inSampleSize = scale;
        openInputStream = new ByteArrayInputStream(data);
        Bitmap bitmap = BitmapFactory.decodeStream(openInputStream, null, options);

        return bitmap;
    }
    
    /**
     * 画像を圧縮してInputStreamとして取得します。
     * @param bitmap
     * @return
     */
    public static InputStream getCompressedImageInputStream(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 100, bos);
        InputStream bitmapInputStream = new ByteArrayInputStream(bos.toByteArray());
        
        return bitmapInputStream;
    }

    /**
     * URIをfile:///sdcard/等のUriに変換します。
     * 
     * @param uri
     */
    public static Uri resolveToLocalUri(Context context, Uri uri) {
        if (uri == null)
            return null;

        if ("content".equals(uri.getScheme())) {
            String convertContentUriToLocal = convertContentUriToLocal(context, uri);
            return Uri.parse("file://" + convertContentUriToLocal);
        } else if ("file".equals(uri.getScheme())) {
            return uri;
        } else {
            return Uri.parse("file://" + uri);
        }
    }

    /**
     * content:// -> /sdcard/* 等に変換します。
     * @return 成功したらそのパスを、失敗したらnullを返します。
     */
    public static String convertContentUriToLocal(Context context, Uri uri) {
        if (uri == null)
            return null;

        String filename = null;
        if ("content".equals(uri.getScheme())) {
            // content:// 等のUri
            Cursor c = null;
            try {
                c = context.getContentResolver().query(uri, new String[] {
                        MediaStore.MediaColumns.DATA
                }, null, null, null);
                if (c.moveToFirst()) {
                    int columnIndex = c.getColumnIndex(MediaStore.MediaColumns.DATA);
                    if (columnIndex != -1) {
                        filename = c.getString(columnIndex);
                    }
                }
            } finally {
                if (c != null) {
                    c.close();
                }
            }
            Log.d(TAG, "fullpath=" + filename);
        } else if ("file".equals(uri.getScheme())) {
            return uri.getPath();
        } else {
            // /sdcard/* 等のUri
            filename = uri.toString();
        }
        return filename;
    }
}
