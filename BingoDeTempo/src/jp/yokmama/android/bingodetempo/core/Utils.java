
package jp.yokmama.android.bingodetempo.core;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.provider.Settings;

public class Utils {
    private static final String KEY = "Z5aRKrXU";
    private static final SimpleDateFormat _sdf1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.JAPAN);
    public static String dateToString(long milliseconds){
        if(milliseconds>0){
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(milliseconds);
            return _sdf1.format(cal.getTime());
        }else{
            return "";
        }
    }
    
    public static String getBingoMessage(Context context, int no){
    	if(no < 5){
    		return (no+1)+ "の列がビンゴ!";
    	} else if(no < 10){
    		return (no-4)+ "の行がビンゴ!";
    	} else if(no == 10){
    		return "左斜めがビンゴ!";
    	}else if(no == 11){
    		return "右斜めがビンゴ!";
    	}
    	return "";
    }

    public static String getReachMessage(Context context, int no){
    	if(no < 5){
    		return (no+1)+ "の列がリーチ!";
    	} else if(no < 10){
    		return (no-4)+ "の行がリーチ!";
    	} else if(no == 10){
    		return "左斜めがリーチ!";
    	}else if(no == 11){
    		return "右斜めがリーチ!";
    	}
    	return "";
    }
    
    public static String getJsonString(JSONObject obj, String name) throws JSONException,
            UnsupportedEncodingException {
        if (!obj.isNull(name))
            return URLDecoder.decode(obj.getString(name), "UTF-8");
        return null;
    }

    public static long getJsonLong(JSONObject obj, String name) throws JSONException {
        if (!obj.isNull(name))
            return obj.getLong(name);
        return 0L;
    }
    
    public static int getJsonInt(JSONObject obj, String name) throws JSONException {
        if (!obj.isNull(name))
            return obj.getInt(name);
        return 0;
    }
    
    public static String encodePassdigiest(String password) {
        byte[] enclyptedHash = null;
        // MD5で暗号化したByte型配列を取得する
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
            md5.update(password.getBytes());
            enclyptedHash = md5.digest();

            // 暗号化されたByte型配列を、16進数表記文字列に変換する
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return bytesToHexString(enclyptedHash);
    }

    private static String bytesToHexString(byte[] fromByte) {

        StringBuilder hexStrBuilder = new StringBuilder();
        for (int i = 0; i < fromByte.length; i++) {

            // 16進数表記で1桁数値だった場合、2桁目を0で埋める
            if ((fromByte[i] & 0xff) < 0x10) {
                hexStrBuilder.append("0");
            }
            hexStrBuilder.append(Integer.toHexString(0xff & fromByte[i]));
        }

        return hexStrBuilder.toString();
    }
    
    public static String getMyId(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String ret = pref.getString("key.pref.userid", "");
        if(ret.length()>0){
            return ret;
        }else{
            String devid = getDeviceKey(context);
            Editor editor = pref.edit();
            editor.putString("key.pref.userid", devid);
            editor.commit();
            
            return devid;
        }
    }

    public static String getDeviceKey(Context context) {
        String id = null;
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            String name = account.name;
            if (name.indexOf("@") != -1) {
                id = name;
                break;
            }
            id = name;
        }
        String deviceId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return encodePassdigiest(id + "+" + deviceId);
    }
}
