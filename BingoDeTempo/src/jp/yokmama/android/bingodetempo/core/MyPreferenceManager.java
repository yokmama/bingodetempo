
package jp.yokmama.android.bingodetempo.core;

import jp.yokmama.android.bingodetempo.R;
import jp.yokmama.android.bingodetempo.utils.PreferenceManagerWrapper;
import android.content.Context;

public class MyPreferenceManager extends PreferenceManagerWrapper {

    public MyPreferenceManager(Context context) {
        super(context);
    }

    public boolean isInitialized() {
        return getDefaultPref().getBoolean("key.initialized", false);
    }

    public void setInitialized(boolean b) {
        getDeafultEditor().putBoolean("key.initialized", b);
    }

    public String getAccountName() {
        return getDefaultPref().getString("key.username",
                getContext().getString(R.string.regist_text_default_username));
    }

    public void setAccountName(String value) {
        getDeafultEditor().putString("key.username", value);
    }

    public long getLastDisplayedModifiedDate() {
        return getDefaultPref().getLong("key.lastDisplayModifiedDate", 0);
    }

    public void setLastDisplayedModifiedDate(long value) {
        getDeafultEditor().putLong("key.lastDisplayModifiedDate", value);
    }
    
    public int getAdmitCount(){
        return getDefaultPref().getInt("key.admitcount", 0);
    }
    
    public void setAdmitCount(int value){
        getDeafultEditor().putInt("key.admitcount", value);
    }
}
