package jp.yokmama.android.bingodetempo.core;

public interface Consts {
    public static final long NFC_CHECK_TIME_DIFF = 1 * 1000;
    
    public static final String KEY_NFCCHECK_TIME = "key.nfccheck.time";

    public static final int MSG_LOAD_LIST = 0x1;
    public static final int MSG_RELOAD_LIST = 0x2;
    public static final int MSG_ADMITCOUNT = 0x3;
}
