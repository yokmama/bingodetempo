
package jp.yokmama.android.bingodetempo.core;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;

/**
 * Fragmentを使ったダイアログを作成する時に便利なフラグメントです。
 * 
 * @author s_kohno
 */
@TargetApi(11)
public class CustomizableDialogFragment extends DialogFragment {
    /** フラグメントのタグ */
    private static final String KEY_TAG = "key-tag";

    /**
     * 新しくFragmentを生成します。tagには、フラグメントのタグを指定します。
     * 
     * @param tag
     * @return
     */
    public static CustomizableDialogFragment newInstance(String tag) {
        CustomizableDialogFragment frg = new CustomizableDialogFragment();
        Bundle args = new Bundle();
        args.putString(KEY_TAG, tag);
        frg.setArguments(args);

        return frg;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getActivity() instanceof OnCreateCustomDialog) {
            OnCreateCustomDialog l = (OnCreateCustomDialog) getActivity();
            Dialog onCreateDialog = l.onCreateDialog(this, getArguments().getString(KEY_TAG));

            return onCreateDialog;
        }
        return super.onCreateDialog(savedInstanceState);
    }

    /**
     * アクティビティよりフラグメントを探して、ダイアログを非表示にします。
     */
    public static CustomizableDialogFragment addMyself(FragmentManager manager, String tag) {
        CustomizableDialogFragment dlg = (CustomizableDialogFragment) manager
                .findFragmentByTag(tag);
        if (dlg == null) {
            dlg = newInstance(tag);
            manager.beginTransaction().add(dlg, tag).commit();
        }

        return dlg;
    }

    /**
     * アクティビティよりフラグメントを探して、ダイアログを非表示にします。
     */
    public static CustomizableDialogFragment hideMyself(FragmentManager manager, String tag) {
        CustomizableDialogFragment dlg = (CustomizableDialogFragment) manager
                .findFragmentByTag(tag);
        if (dlg != null) {
            dlg.dismiss();
        }
        
        return dlg;
    }

    public void show(FragmentManager manager) {
        show(manager, getArguments().getString(KEY_TAG));
    }

    public interface OnCreateCustomDialog {
        public Dialog onCreateDialog(CustomizableDialogFragment frg, String tag);
    }
}
