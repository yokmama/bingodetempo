
package jp.yokmama.android.bingodetempo.db;

import jp.yokmama.android.bingodetempo.db.TableColumns.BaseColumns;
import jp.yokmama.android.bingodetempo.db.TableColumns.Bingo;
import jp.yokmama.android.bingodetempo.db.TableColumns.Hole;
import jp.yokmama.android.bingodetempo.db.TableColumns.HoleLog;
import jp.yokmama.android.bingodetempo.db.TableColumns.Store;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

class MyDatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "mydatabase.db";

    private static final int DATABASE_VERSION = 2;

    public MyDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            if (!findTable(db, Hole.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(Hole.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(Hole.INDEX).append(" INTEGER");
                sql.append(",").append(Hole.NO).append(" INTEGER");
                sql.append(",").append(Hole.NFCID).append(" TEXT");
                sql.append(",").append(Hole.PUNCH).append(" INTEGER DEFAULT 0");
                sql.append(",").append(Hole.MODIFIED_DATE).append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, HoleLog.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(HoleLog.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(HoleLog.NO).append(" INTEGER");
                sql.append(",").append(HoleLog.TYPE).append(" INTEGER");
                sql.append(",").append(HoleLog.CREATED_DATE).append(" LONG");
                sql.append(",").append(HoleLog.CONSUMED).append(" INTEGER DEFAULT 0");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, Bingo.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(Bingo.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(Bingo.PUNCH).append(" INTEGER");
                sql.append(",").append(Bingo.MESSAGE).append(" TEXT");
                sql.append(",").append(Bingo.CREATED_DATE).append(" LONG");
                sql.append(",").append(Bingo.MODIFIED_DATE).append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, Store.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(Store.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(Store.NAME).append(" TEXT");
                sql.append(",").append(Store.NFCID).append(" TEXT");
                sql.append(",").append(Store.CREATED_DATE).append(" LONG");
                sql.append(",").append(Bingo.MODIFIED_DATE).append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVewsion) {
        onCreate(db);
        db.beginTransaction();
        try {
            if (oldVersion == 1) {
                StringBuilder sql = new StringBuilder();
                sql.append("ALTER TABLE ").append(HoleLog.TBNAME).append(" ADD COLUMN ")
                        .append(HoleLog.CONSUMED).append(" INTEGER");
                db.execSQL(sql.toString());
                db.setTransactionSuccessful();
            }
        } finally {
            db.endTransaction();
        }
    }

    public boolean findTable(SQLiteDatabase db, String tbl) {

        StringBuilder where = new StringBuilder();
        where.append("type='table' and name='");
        where.append(tbl);
        where.append("'");

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables("sqlite_master"); // テーブル名
        Cursor cur = null;
        try {
            cur = qb.query(db, null, where.toString(), null, null, null, null, null);
            if (cur != null) {
                if (cur.moveToFirst()) {
                    return true;
                }
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public int delete(String table, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            int ret = db.delete(table, selection, selectionArgs);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }

    public long insert(String table, ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            long id = db.insert(table, null, values);
            db.setTransactionSuccessful();
            return id;
        } finally {
            db.endTransaction();
        }
    }

    public Cursor query(String table, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getReadableDatabase();
        try {
            return db.query(table, projection, selection, selectionArgs, null, null, sortOrder);
        } finally {
        }
    }

    public int update(String table, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            int ret = db.update(table, values, selection, selectionArgs);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }
}
