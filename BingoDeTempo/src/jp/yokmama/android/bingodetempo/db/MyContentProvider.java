package jp.yokmama.android.bingodetempo.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

public class MyContentProvider extends ContentProvider implements TableColumns {
	public static final String AUTHORITY = "jp.yokmama.android.bingodetempo";
	public static final String CONTENT_AUTHORITY = "content://" + AUTHORITY
			+ "/";

	public static final Uri HOLE_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY
			+ Hole.TBNAME);
	public static final Uri HOLELOG_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY
			+ HoleLog.TBNAME);
	public static final Uri BINGO_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY
			+ Bingo.TBNAME);
	public static final Uri STORE_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY
			+ Store.TBNAME);

	public static final int CODE_HOLE = 1;
	public static final int CODE_HOLE_ID = 2;
	public static final int CODE_HOLELOG = 3;
	public static final int CODE_HOLELOG_ID = 4;
	public static final int CODE_BINGO = 5;
	public static final int CODE_BINGO_ID = 6;
	public static final int CODE_STORE = 7;
	public static final int CODE_STORE_ID = 8;

	private MyDatabaseHelper mDatabaseHelper = null;
	private UriMatcher mUriMatcher = null;

	@Override
	public boolean onCreate() {
		mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		mUriMatcher.addURI(AUTHORITY, Hole.TBNAME, CODE_HOLE);
		mUriMatcher.addURI(AUTHORITY, Hole.TBNAME + "/#", CODE_HOLE_ID);
		mUriMatcher.addURI(AUTHORITY, HoleLog.TBNAME, CODE_HOLELOG);
		mUriMatcher.addURI(AUTHORITY, HoleLog.TBNAME + "/#", CODE_HOLELOG_ID);
		mUriMatcher.addURI(AUTHORITY, Bingo.TBNAME, CODE_BINGO);
		mUriMatcher.addURI(AUTHORITY, Bingo.TBNAME + "/#", CODE_BINGO_ID);
		mUriMatcher.addURI(AUTHORITY, Store.TBNAME, CODE_STORE);
		mUriMatcher.addURI(AUTHORITY, Store.TBNAME + "/#", CODE_STORE_ID);

		mDatabaseHelper = new MyDatabaseHelper(getContext());

		return true;
	}

	@Override
	public String getType(Uri arg0) {
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int match = mUriMatcher.match(uri);
		switch (match) {
		case CODE_HOLE: {
			return mDatabaseHelper
					.delete(Hole.TBNAME, selection, selectionArgs);
		}
		case CODE_HOLE_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.delete(Hole.TBNAME,
					BaseColumns._ID + " = ?",
					new String[] { Long.toString(id) });
		}
		case CODE_HOLELOG: {
			return mDatabaseHelper.delete(HoleLog.TBNAME, selection,
					selectionArgs);
		}
		case CODE_HOLELOG_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.delete(HoleLog.TBNAME, BaseColumns._ID
					+ " = ?", new String[] { Long.toString(id) });
		}
		case CODE_BINGO: {
			return mDatabaseHelper.delete(Bingo.TBNAME, selection,
					selectionArgs);
		}
		case CODE_BINGO_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.delete(Bingo.TBNAME, BaseColumns._ID
					+ " = ?", new String[] { Long.toString(id) });
		}
		case CODE_STORE: {
			return mDatabaseHelper.delete(Store.TBNAME, selection,
					selectionArgs);
		}
		case CODE_STORE_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.delete(Store.TBNAME, BaseColumns._ID
					+ " = ?", new String[] { Long.toString(id) });
		}
		}
		return 0;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int match = mUriMatcher.match(uri);
		switch (match) {
		case CODE_HOLE: {
			return ContentUris.withAppendedId(HOLE_CONTENT_URI,
					mDatabaseHelper.insert(Hole.TBNAME, values));
		}
		case CODE_HOLELOG: {
			return ContentUris.withAppendedId(HOLELOG_CONTENT_URI,
					mDatabaseHelper.insert(HoleLog.TBNAME, values));
		}
		case CODE_BINGO: {
			return ContentUris.withAppendedId(BINGO_CONTENT_URI,
					mDatabaseHelper.insert(Bingo.TBNAME, values));
		}
		case CODE_STORE: {
			return ContentUris.withAppendedId(STORE_CONTENT_URI,
					mDatabaseHelper.insert(Store.TBNAME, values));
		}
		}
		return null;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		int match = mUriMatcher.match(uri);
		switch (match) {
		case CODE_HOLE: {
			return mDatabaseHelper.query(Hole.TBNAME, projection, selection,
					selectionArgs, sortOrder);
		}
		case CODE_HOLE_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.query(Hole.TBNAME, projection,
					BaseColumns._ID + " = ?",
					new String[] { Long.toString(id) }, sortOrder);
		}
		case CODE_HOLELOG: {
			return mDatabaseHelper.query(HoleLog.TBNAME, projection, selection,
					selectionArgs, sortOrder);
		}
		case CODE_HOLELOG_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.query(HoleLog.TBNAME, projection,
					BaseColumns._ID + " = ?",
					new String[] { Long.toString(id) }, sortOrder);
		}
		case CODE_BINGO: {
			return mDatabaseHelper.query(Bingo.TBNAME, projection, selection,
					selectionArgs, sortOrder);
		}
		case CODE_BINGO_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.query(Bingo.TBNAME, projection,
					BaseColumns._ID + " = ?",
					new String[] { Long.toString(id) }, sortOrder);
		}
		case CODE_STORE: {
			return mDatabaseHelper.query(Store.TBNAME, projection, selection,
					selectionArgs, sortOrder);
		}
		case CODE_STORE_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.query(Store.TBNAME, projection,
					BaseColumns._ID + " = ?",
					new String[] { Long.toString(id) }, sortOrder);
		}
		}
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int match = mUriMatcher.match(uri);
		switch (match) {
		case CODE_HOLE: {
			return mDatabaseHelper.update(Hole.TBNAME, values, selection,
					selectionArgs);
		}
		case CODE_HOLE_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.update(Hole.TBNAME, values, BaseColumns._ID
					+ " = ?", new String[] { Long.toString(id) });
		}
		case CODE_HOLELOG: {
			return mDatabaseHelper.update(HoleLog.TBNAME, values, selection,
					selectionArgs);
		}
		case CODE_HOLELOG_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.update(HoleLog.TBNAME, values,
					BaseColumns._ID + " = ?",
					new String[] { Long.toString(id) });
		}
		case CODE_BINGO: {
			return mDatabaseHelper.update(Bingo.TBNAME, values, selection,
					selectionArgs);
		}
		case CODE_BINGO_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.update(Bingo.TBNAME, values, BaseColumns._ID
					+ " = ?", new String[] { Long.toString(id) });
		}
		case CODE_STORE: {
			return mDatabaseHelper.update(Store.TBNAME, values, selection,
					selectionArgs);
		}
		case CODE_STORE_ID: {
			long id = ContentUris.parseId(uri);
			return mDatabaseHelper.update(Store.TBNAME, values, BaseColumns._ID
					+ " = ?", new String[] { Long.toString(id) });
		}
		}
		return 0;
	}
}
