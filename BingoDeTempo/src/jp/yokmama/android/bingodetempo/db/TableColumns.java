
package jp.yokmama.android.bingodetempo.db;

public interface TableColumns {

    public static interface BaseColumns {
        public static String _ID = "_id";
    }

    public static interface Hole extends BaseColumns {
        public static String TBNAME = "t_hole";
        public static String INDEX = "f_index";
        public static String NO = "f_no";
        public static String PUNCH = "f_punch";
        public static String NFCID = "f_nfcid";
        public static String MODIFIED_DATE = "f_modifieddate";
    }

    public static interface HoleLog extends BaseColumns {
    	public static int TYPE_HOLE = 0;
    	public static int TYPE_REACH = 1;
    	public static int TYPE_BINGO = 2;
    	
        public static String TBNAME = "t_bingolog";
        public static String NO = "f_no";
        public static String TYPE = "f_type";
        public static String CREATED_DATE = "f_createddate";
        public static String CONSUMED = "f_consume"; // 画面OFF状態で受信した後、表示したか否か
    }

    public static interface Bingo extends BaseColumns {
        public static String TBNAME = "t_bingo";
        public static String PUNCH = "f_punch";
        public static String MESSAGE = "f_message";
        public static String CREATED_DATE = "f_createddate";
        public static String MODIFIED_DATE = "f_modifieddate";
    }
    
    public static interface Store extends BaseColumns {
        public static String TBNAME = "t_store";
        public static String NAME = "f_name";
        public static String NFCID = "f_nfcid";
        public static String CREATED_DATE = "f_createddate";
        public static String MODIFIED_DATE = "f_modifieddate";
    }
}
