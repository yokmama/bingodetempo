package jp.yokmama.android.bingodetempo.db;

import jp.yokmama.android.bingodetempo.db.TableColumns.BaseColumns;
import android.database.Cursor;


public class BaseItem {
    private long mId;

    public BaseItem() {

    }

    public BaseItem(Cursor cursor) {
        mId = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));

    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

}
