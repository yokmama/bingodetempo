package jp.yokmama.android.bingodetempo.bingohelp;

import static jp.yokmama.android.bingodetempo.core.CommonUtilities.HOST;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import jp.yokmama.android.bingodetempo.R;
import jp.yokmama.android.bingodetempo.core.HttpUtils;
import jp.yokmama.android.bingodetempo.core.Utils;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class HelpFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bingo_help, container, false);
        view.findViewById(R.id.button1).setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                Thread t = new Thread(new Runnable() {
                    
                    @Override
                    public void run() {
                        try {
                            HttpUtils.post(HOST+"/json/Lot", null);
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                });
                t.start();
            }
        });
        
        return view;
    }

}
