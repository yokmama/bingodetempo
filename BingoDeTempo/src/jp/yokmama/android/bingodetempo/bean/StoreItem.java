package jp.yokmama.android.bingodetempo.bean;

import jp.yokmama.android.bingodetempo.db.BaseItem;
import jp.yokmama.android.bingodetempo.db.TableColumns.Store;
import android.database.Cursor;

public class StoreItem extends BaseItem {
    private String mName;
    private String mIDm;
    private long mCreatedDate;
    private long mModifiedDate;

    public StoreItem() {

    }

    public StoreItem(Cursor cursor) {
        super(cursor);
        mName = cursor.getString(cursor.getColumnIndex(Store.NAME));
        mIDm = cursor.getString(cursor.getColumnIndex(Store.NFCID));
        mCreatedDate = cursor.getLong(cursor
                .getColumnIndex(Store.CREATED_DATE));
        mModifiedDate= cursor.getLong(cursor
                .getColumnIndex(Store.MODIFIED_DATE));
    }

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public String getIDm() {
		return mIDm;
	}

	public void setIDm(String iDm) {
		mIDm = iDm;
	}

	public long getCreatedDate() {
		return mCreatedDate;
	}

	public void setCreatedDate(long createdDate) {
		mCreatedDate = createdDate;
	}

	public long getModifiedDate() {
		return mModifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		mModifiedDate = modifiedDate;
	}
    
    
}
