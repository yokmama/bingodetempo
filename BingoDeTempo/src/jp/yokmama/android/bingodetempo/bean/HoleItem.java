package jp.yokmama.android.bingodetempo.bean;

import jp.yokmama.android.bingodetempo.db.BaseItem;
import jp.yokmama.android.bingodetempo.db.TableColumns.Hole;
import android.database.Cursor;

public class HoleItem extends BaseItem {
    private int mIndex;
    private int mNo;
    private int mPunch;
    private long mModifiedDate;

    public HoleItem() {

    }

    public HoleItem(Cursor cursor) {
        super(cursor);
        mIndex = cursor.getInt(cursor.getColumnIndex(Hole.INDEX));
        mPunch = cursor.getInt(cursor.getColumnIndex(Hole.PUNCH));
        mNo = cursor.getInt(cursor.getColumnIndex(Hole.NO));
        mModifiedDate = cursor.getLong(cursor
                .getColumnIndex(Hole.MODIFIED_DATE));
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public int getNo() {
        return mNo;
    }

    public void setNo(int no) {
        mNo = no;
    }

    public int getPunch() {
        return mPunch;
    }

    public void setPunch(int punch) {
        mPunch = punch;
    }

    public long getModifiedDate() {
        return mModifiedDate;
    }

    public void setModifiedDate(long modifiedDate) {
        mModifiedDate = modifiedDate;
    }

}
