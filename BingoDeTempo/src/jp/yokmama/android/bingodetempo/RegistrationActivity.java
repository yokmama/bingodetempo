package jp.yokmama.android.bingodetempo;

import jp.yokmama.android.bingodetempo.core.CommonUtilities;
import jp.yokmama.android.bingodetempo.core.MyPreferenceManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.google.android.gcm.GCMRegistrar;

public class RegistrationActivity extends FragmentActivity implements OnClickListener {
    private EditText editUserName;
    private MyPreferenceManager mPreference;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.regist_main);
        
        mPreference = new MyPreferenceManager(this);
        
        editUserName = (EditText)findViewById(R.id.editUserName);
        findViewById(R.id.buttonOk).setOnClickListener(this);
        findViewById(R.id.buttonCancel).setOnClickListener(this);
        
        editUserName.setText(mPreference.getAccountName());
        
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.buttonOk){
            mPreference.setAccountName(editUserName.getText().toString());
            //GCM登録
            GCMRegistrar.register(this, CommonUtilities.SENDER_ID);
            if(GCMRegistrar.isRegistered(this)){
                mPreference.setInitialized(true);
                mPreference.commit();
                Intent intent = new Intent();
                intent.setClass(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }else{
                //Progressで登録中をだす
            }
        }else if(view.getId() == R.id.buttonCancel){
            finish();
        }
    }

}
