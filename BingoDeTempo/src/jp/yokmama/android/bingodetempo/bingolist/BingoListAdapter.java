package jp.yokmama.android.bingodetempo.bingolist;

import jp.yokmama.android.bingodetempo.R;
import jp.yokmama.android.bingodetempo.core.Utils;
import jp.yokmama.android.bingodetempo.db.TableColumns.Bingo;
import jp.yokmama.android.bingodetempo.db.TableColumns.HoleLog;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class BingoListAdapter extends CursorAdapter {
	private LayoutInflater mInfalator;

	public BingoListAdapter(Context context, Cursor cursor) {
		super(context, cursor, true);
		mInfalator = LayoutInflater.from(context);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();

		String message = cursor.getString(cursor.getColumnIndex(Bingo.MESSAGE));
		int punch = cursor.getInt(cursor.getColumnIndex(Bingo.PUNCH));
		holder.textMessage.setText(message);
		holder.imageStamp.setVisibility(punch==1?View.VISIBLE:View.INVISIBLE);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View v = mInfalator.inflate(R.layout.bingo_list_item, parent,
				false);
		ViewHolder holder = new ViewHolder();
		holder.textMessage = (TextView) v.findViewById(R.id.textMessage);
		holder.imageStamp = (ImageView)v.findViewById(R.id.imageStamp);

		v.setTag(holder);
		return v;
	}

	private class ViewHolder {
		TextView textMessage;
		ImageView imageStamp;
	}
}
