package jp.yokmama.android.bingodetempo.bingolist;

import jp.yokmama.android.bingodetempo.R;
import jp.yokmama.android.bingodetempo.db.MyContentProvider;
import jp.yokmama.android.bingodetempo.db.TableColumns.Bingo;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class BingoListFragment extends Fragment implements
		LoaderCallbacks<Cursor>, OnItemClickListener {
	private ListView mListView;
	private BingoListAdapter mAdapter;

	private Handler mHandler;
	private ContentObserver mObserver;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.bingo_list, container, false);
		mListView = (ListView) view.findViewById(R.id.listView1);
		mListView.setOnItemClickListener(this);
		mListView.setEmptyView(view.findViewById(android.R.id.empty));

		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		mHandler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				getLoaderManager().restartLoader(R.layout.bingo_list, null,
						BingoListFragment.this);
				return false;
			}
		});
		mObserver = new ContentObserver(mHandler) {
			@Override
			public void onChange(boolean selfChange) {
				super.onChange(selfChange);
				mHandler.sendEmptyMessage(0);
			}
		};
		getActivity().getContentResolver().registerContentObserver(
				MyContentProvider.BINGO_CONTENT_URI, false, mObserver);
	}

	@Override
	public void onResume() {
		super.onResume();
		getLoaderManager().restartLoader(R.layout.bingo_list, null, this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mObserver != null) {
			getActivity().getContentResolver().unregisterContentObserver(
					mObserver);
			mObserver = null;
		}

		getLoaderManager().destroyLoader(R.layout.bingo_list);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		return new CursorLoader(getActivity(),
				MyContentProvider.BINGO_CONTENT_URI, null, Bingo.PUNCH + " = 0", null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
		mAdapter = new BingoListAdapter(getActivity(), cursor);
		mListView.setAdapter(mAdapter);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		mAdapter = new BingoListAdapter(getActivity(), null);
		mListView.setAdapter(mAdapter);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		Cursor cursor = (Cursor) mListView.getItemAtPosition(position);
		int punch = cursor.getInt(cursor.getColumnIndex(Bingo.PUNCH));
		if (punch == 0) {
			final long id = cursor.getLong(cursor.getColumnIndex(Bingo._ID));
			
			startActivity(BingoShowTicketActivity.createIntent(getActivity(), id));
			
			/*new AlertDialog.Builder(getActivity())
					.setMessage("割引券を使用しますか？")
					.setCancelable(true)
					.setPositiveButton("はい",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int n) {
									ContentValues values = new ContentValues();
									values.put(Bingo.PUNCH, 1);
									getActivity()
											.getContentResolver()
											.update(ContentUris
													.withAppendedId(
															MyContentProvider.BINGO_CONTENT_URI,
															id), values, null,
													null);

									getLoaderManager().restartLoader(
											R.layout.bingo_list, null,
											BingoListFragment.this);
								}
							})
					.setNegativeButton("いいえ",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							}).show();*/
		}
	}

}
