package jp.yokmama.android.bingodetempo.bingolist;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import jp.yokmama.android.bingodetempo.R;
import jp.yokmama.android.bingodetempo.db.MyContentProvider;
import jp.yokmama.android.bingodetempo.db.TableColumns.Bingo;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class BingoShowTicketActivity extends Activity implements
		OnClickListener {
	private static final String KEY_ID = "key.bingoshowticket.id";
	private static final SimpleDateFormat _sdf1 = new SimpleDateFormat(
			"yyyy年 MM日 dd日", Locale.JAPAN);

	private long mSelectedId;
	private long mDate;

	public BingoShowTicketActivity() {

	}

	public static Intent createIntent(Context context, long id) {
		Intent i = new Intent(context, BingoShowTicketActivity.class);
		i.putExtra(KEY_ID, id);
		return i;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bingo_show_ticket);

		if (getIntent() != null) {
			mSelectedId = getIntent().getLongExtra(KEY_ID, 0);
		}

		TextView textDate = (TextView) findViewById(R.id.textDate);
		TextView textDetail = (TextView) findViewById(R.id.textDetail);
		findViewById(R.id.buttonUse).setOnClickListener(this);

		Cursor cursor = null;
		try {
			cursor = getContentResolver().query(
					ContentUris.withAppendedId(
							MyContentProvider.BINGO_CONTENT_URI, mSelectedId),
					null, null, null, null);
			if (cursor != null && cursor.moveToFirst()) {
				mDate = cursor.getLong(cursor
						.getColumnIndex(Bingo.CREATED_DATE));
				String message = cursor.getString(cursor
						.getColumnIndex(Bingo.MESSAGE));

				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(mDate);
				cal.add(Calendar.DAY_OF_MONTH, 1);
				long a = cal.getTimeInMillis();

				StringBuilder buf = new StringBuilder();
				buf.append("使用期限：");
				buf.append(cal.get(Calendar.YEAR)).append("年");
				buf.append(cal.get(Calendar.MONTH) + 1).append("月");
				buf.append(cal.get(Calendar.DAY_OF_MONTH)).append("日 〜");
				cal.add(Calendar.DAY_OF_MONTH, 30);
				long b = cal.getTimeInMillis();
				
				buf.append(cal.get(Calendar.YEAR)).append("年");
				buf.append(cal.get(Calendar.MONTH) + 1).append("月");
				buf.append(cal.get(Calendar.DAY_OF_MONTH)).append("日まで");

				textDate.setText(buf.toString());
				textDetail.setText(message);
				
				long now = System.currentTimeMillis();
				if(now>=a && now<b){
				}
				else{
					findViewById(R.id.buttonUse).setEnabled(false);					
				}
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	@Override
	public void onClick(View arg0) {
		if (arg0.getId() == R.id.buttonUse) {
			ContentValues values = new ContentValues();
			values.put(Bingo.PUNCH, 1);
			getContentResolver().update(
					ContentUris.withAppendedId(
							MyContentProvider.BINGO_CONTENT_URI, mSelectedId), values,
					null, null);
			finish();
		}
	}

}
