package jp.yokmama.android.bingodetempo;

import jp.yokmama.android.bingodetempo.core.Consts;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class NFCRecieveActivity extends FragmentActivity {
    private static final String TAG = NFCRecieveActivity.class.getSimpleName();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Intent intent = getIntent();
        if (intent != null) {
            intent.putExtra(Consts.KEY_NFCCHECK_TIME, System.currentTimeMillis());
            
            // Intentを加工してMainActivityへ渡す
            intent.setClass(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            
            finish();
        } else {
            Log.d(TAG, "Intentがない、想定外");
        }
    }
}
