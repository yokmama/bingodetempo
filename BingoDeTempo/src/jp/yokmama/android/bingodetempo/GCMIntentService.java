/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.yokmama.android.bingodetempo;

import static jp.yokmama.android.bingodetempo.core.CommonUtilities.SENDER_ID;
import static jp.yokmama.android.bingodetempo.core.CommonUtilities.displayMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import jp.yokmama.android.bingodetempo.bean.HoleItem;
import jp.yokmama.android.bingodetempo.bean.StoreItem;
import jp.yokmama.android.bingodetempo.core.MyPreferenceManager;
import jp.yokmama.android.bingodetempo.core.ServerUtilities;
import jp.yokmama.android.bingodetempo.db.MyContentProvider;
import jp.yokmama.android.bingodetempo.db.TableColumns.Bingo;
import jp.yokmama.android.bingodetempo.db.TableColumns.Hole;
import jp.yokmama.android.bingodetempo.db.TableColumns.HoleLog;
import jp.yokmama.android.bingodetempo.utils.DatabaseHelper;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

	@SuppressWarnings("hiding")
	private static final String TAG = "GCMIntentService";

	public GCMIntentService() {
		super(SENDER_ID);
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.i(TAG, "Device registered: regId = " + registrationId);
		displayMessage(context, getString(R.string.gcm_registered));
		ServerUtilities.register(context, registrationId);

		MyPreferenceManager pref = new MyPreferenceManager(context);
		pref.setInitialized(true);
		pref.commit();

		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		callIntent.setClass(this, MainActivity.class);
		startActivity(callIntent);
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG, "Device unregistered");
		displayMessage(context, getString(R.string.gcm_unregistered));
		if (GCMRegistrar.isRegisteredOnServer(context)) {
			ServerUtilities.unregister(context, registrationId);
		} else {
			// This callback results from the call to unregister made on
			// ServerUtilities when the registration to the server failed.
			Log.i(TAG, "Ignoring unregister callback");
		}
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.i(TAG, "Received message");
		// String message = getString(R.string.gcm_message);
		// displayMessage(context, message);
		// notifies user
		String action = intent.getStringExtra("action");
		if ("call".equals(action)) {
			String status = intent.getStringExtra("status");
			String nickanem = intent.getStringExtra("nickname");
			StringBuilder buf = new StringBuilder();
			buf.append(nickanem);
			if ("0".equals(status)) {
				buf.append("さんがリーチです！");
			} else {
				buf.append("さんがBINGOです！");
			}

			Toast.makeText(this, buf.toString(), Toast.LENGTH_SHORT).show();
		} else if ("lot".equals(action)) {
			int number = Integer.parseInt(intent.getStringExtra("number"));
			
			//HoleLog
			ContentValues log_values = new ContentValues();
			log_values.put(HoleLog.TYPE, HoleLog.TYPE_HOLE);
			log_values.put(HoleLog.NO, number);
			log_values.put(HoleLog.CREATED_DATE,
					System.currentTimeMillis());
			getContentResolver().insert(
					MyContentProvider.HOLELOG_CONTENT_URI, log_values);
			//更新通知
			getContentResolver().notifyChange(
					MyContentProvider.HOLELOG_CONTENT_URI, null);
			
            //POOL数を保存
			MyPreferenceManager pref = new MyPreferenceManager(this);
			int n = pref.getAdmitCount()-1;
			if(n<0){
				n = 0;
			}
			pref.setAdmitCount(n);
			pref.commit();
			
			//bingo or reach log
			List<HoleItem> holes = DatabaseHelper.selectHoles(this,
					DatabaseHelper.SELECT_HOLE_UNPUNCH);
			for (int i = 0; i < holes.size(); i++) {
				HoleItem item = holes.get(i);
				if (item.getNo() == number) {
					ContentValues values = new ContentValues();
					values.put(Hole.PUNCH, 1);
					values.put(Hole.MODIFIED_DATE, System.currentTimeMillis());
					getContentResolver().update(
							ContentUris.withAppendedId(
									MyContentProvider.HOLE_CONTENT_URI,
									item.getId()), values, null, null);
					
					updateBingo();
					
					int count = DatabaseHelper.getCount(this,
							MyContentProvider.HOLELOG_CONTENT_URI, HoleLog.TYPE + " = ?",
							new String[] { Integer.toString(HoleLog.TYPE_BINGO) });
					//2個以上BINGOだったらリセットする
					if (count > 1) {
					    reset(pref);
					}
					
					//更新通知
					getContentResolver().notifyChange(
							MyContentProvider.HOLE_CONTENT_URI, null);
					break;
				}
			}
        } else if ("reset".equals(action)) {
            MyPreferenceManager pref = new MyPreferenceManager(this);
            reset(pref);
            
            //更新通知
            getContentResolver().notifyChange(
                    MyContentProvider.HOLE_CONTENT_URI, null);
		}
	}
	
	private void reset(MyPreferenceManager pref) {
        // reset
        getContentResolver().delete(
                MyContentProvider.HOLE_CONTENT_URI, null, null);
        getContentResolver().delete(
                MyContentProvider.HOLELOG_CONTENT_URI, null, null);
        
        //reset
        pref.setAdmitCount(0);
        pref.commit();
	}

	@Override
	protected void onDeletedMessages(Context context, int total) {
		Log.i(TAG, "Received deleted messages notification");
		String message = getString(R.string.gcm_deleted, total);
		displayMessage(context, message);
		// notifies user
		generateNotification(context, "delete message", message);
	}

	@Override
	public void onError(Context context, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
		displayMessage(context, getString(R.string.gcm_error, errorId));
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Log.i(TAG, "Received recoverable error: " + errorId);
		displayMessage(context,
				getString(R.string.gcm_recoverable_error, errorId));
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(Context context, String title,
			String message) {
		int icon = R.drawable.ic_stat_gcm;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		Intent notificationIntent = new Intent(context, MainActivity.class);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, notification);
	}

	private void updateBingo() {
		List<HoleItem> holes = DatabaseHelper.selectHoles(this,
				DatabaseHelper.SELECT_HOLE_ALL);
		HoleItem[][] matrix = new HoleItem[5][5];
		for (int i = 0; i < holes.size(); i++) {
			HoleItem item = holes.get(i);
			int pos = item.getIndex() - 1;
			int x = pos % 5;
			int y = (int) (pos / 5);
			matrix[y][x] = item;
		}
		List<Integer> bingolist = new ArrayList<Integer>();
		List<Integer> reachlist = new ArrayList<Integer>();
		int counter = 0;
		// X方向チェック
		for (int x = 0; x < 5; x++) {
			counter = 0;
			for (int y = 0; y < 5; y++) {
				if (matrix[y][x] == null || matrix[y][x].getPunch() == 1) {
					counter++;
				}
			}
			if (counter == 5) {
				bingolist.add(x);
			} else if (counter == 4) {
				reachlist.add(x);
			}
		}
		// Y方向チェック
		for (int y = 0; y < 5; y++) {
			counter = 0;
			for (int x = 0; x < 5; x++) {
				if (matrix[y][x] == null || matrix[y][x].getPunch() == 1) {
					counter++;
				}
			}
			if (counter == 5) {
				bingolist.add(y+5);
			} else if (counter == 4) {
				reachlist.add(y+5);
			}
		}

		// 斜めチェック1
		counter = 0;
		for (int i = 0; i < 5; i++) {
			if (matrix[i][i] == null || matrix[i][i].getPunch() == 1) {
				counter++;
			}
		}
		if (counter == 5) {
			bingolist.add(10);
		} else if (counter == 4) {
			reachlist.add(10);
		}
		// 斜めチェック2
		counter = 0;
		for (int i = 0; i < 5; i++) {
			if (matrix[i][4 - i] == null || matrix[i][4 - i].getPunch() == 1) {
				counter++;
			}
		}
		if (counter == 5) {
			bingolist.add(11);
		} else if (counter == 4) {
			reachlist.add(11);
		}

		ContentProviderClient client = null;
		try {
			client = getContentResolver().acquireContentProviderClient(
					MyContentProvider.HOLELOG_CONTENT_URI);
			// bingo
			for (int no : bingolist) {
				if (DatabaseHelper.getCount(client,
						MyContentProvider.HOLELOG_CONTENT_URI, HoleLog.TYPE
								+ " = ? AND " + HoleLog.NO + " = ?",
						new String[] { Integer.toString(HoleLog.TYPE_BINGO),
								Integer.toString(no) }) < 1) {
					// 新規追加
					ContentValues values = new ContentValues();
					values.put(HoleLog.TYPE, HoleLog.TYPE_BINGO);
					values.put(HoleLog.NO, no);
					values.put(HoleLog.CREATED_DATE, System.currentTimeMillis());
					client.insert(MyContentProvider.HOLELOG_CONTENT_URI, values);
					
					// BINGO登録
					ContentValues initialValues = new ContentValues();
					initialValues.put(Bingo.MESSAGE, makeRandamMessage(no));
					initialValues.put(Bingo.PUNCH, 0);
					initialValues.put(Bingo.CREATED_DATE, System.currentTimeMillis());
					client.insert(MyContentProvider.BINGO_CONTENT_URI, initialValues);

					//更新通知
					getContentResolver().notifyChange(
							MyContentProvider.BINGO_CONTENT_URI, null);
				}
			}

			// reach
			for (int no : reachlist) {
				client = getContentResolver().acquireContentProviderClient(
						MyContentProvider.HOLELOG_CONTENT_URI);
				if (DatabaseHelper.getCount(client,
						MyContentProvider.HOLELOG_CONTENT_URI, HoleLog.TYPE
								+ " != ? AND " + HoleLog.NO + " = ?",
						new String[] { Integer.toString(HoleLog.TYPE_REACH),
								Integer.toString(no) }) < 1) {
					// 新規追加
					ContentValues values = new ContentValues();
					values.put(HoleLog.TYPE, HoleLog.TYPE_REACH);
					values.put(HoleLog.NO, no);
					values.put(HoleLog.CREATED_DATE, System.currentTimeMillis());
					client.insert(MyContentProvider.HOLELOG_CONTENT_URI, values);
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				client.release();
			}
		}
	}

    private static final Random _rand = new Random(System.currentTimeMillis());
	private String makeRandamMessage(int bingo) {
		ArrayList<StoreItem> items = new ArrayList<StoreItem>();
		Cursor cursor = null;
		try{
			cursor = getContentResolver().query(MyContentProvider.STORE_CONTENT_URI, null, null, null, null);
			if(cursor != null && cursor.moveToFirst()){
				do{
					items.add(new StoreItem(cursor));
				}while(cursor.moveToNext());
			}
		}finally{
			if(cursor!=null){
				cursor.close();
			}
		}
		
		String name = null;
		if(items.size()>3){
			int num = _rand.nextInt(items.size());
			name = items.get(num).getName();
		}
		
		StringBuilder buf = new StringBuilder();
		if(name != null){
			buf.append("ビンゴ！　３割引券！「").append(name).append("」");
			buf.append("で使用できます。");
		}else{
			buf.append("ビンゴ！　2割引券！全店で使用できます。");
		}
		
		return buf.toString();
	}

}
