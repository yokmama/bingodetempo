package jp.yokmama.android.bingodetempo.bingomgr;

import jp.yokmama.android.bingodetempo.R;
import jp.yokmama.android.bingodetempo.core.CommonUtilities;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MgrFragment extends Fragment {
	private WebView mWebView;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bingo_mgr, container, false);
        
        mWebView = (WebView)view.findViewById(R.id.webView1);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        
        mWebView.loadUrl(CommonUtilities.HOST + "/Lot?" + System.currentTimeMillis());
        
        return view;
    }

	@Override
	public void onDestroy() {
		mWebView.destroy();
		super.onDestroy();
	}

    
}
