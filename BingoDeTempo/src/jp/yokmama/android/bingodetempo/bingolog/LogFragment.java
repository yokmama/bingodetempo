package jp.yokmama.android.bingodetempo.bingolog;

import jp.yokmama.android.bingodetempo.R;
import jp.yokmama.android.bingodetempo.bingomain.BingoFragment;
import jp.yokmama.android.bingodetempo.db.MyContentProvider;
import jp.yokmama.android.bingodetempo.db.TableColumns.HoleLog;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class LogFragment extends Fragment implements LoaderCallbacks<Cursor>, OnItemClickListener{
	private ListView mListView;
	private HoleLogAdapter mAdapter;
	
	private Handler mHandler;
	private ContentObserver mObserver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bingo_log, container, false);
        mListView = (ListView) view.findViewById(R.id.listView1);
		mListView.setOnItemClickListener(this);

        return view;
    }
    
    


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getLoaderManager().initLoader(R.layout.bingo_log, null, this);
		
		mHandler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				getLoaderManager().restartLoader(R.layout.bingo_log, null,
						LogFragment.this);
				return false;
			}
		});
		mObserver = new ContentObserver(mHandler) {
			@Override
			public void onChange(boolean selfChange) {
				super.onChange(selfChange);
				mHandler.sendEmptyMessage(0);
			}
		};
		getActivity().getContentResolver().registerContentObserver(
				MyContentProvider.HOLELOG_CONTENT_URI, false, mObserver);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mObserver != null) {
			getActivity().getContentResolver().unregisterContentObserver(
					mObserver);
			mObserver = null;
		}
		
		getLoaderManager().destroyLoader(R.layout.bingo_log);
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		return new CursorLoader(getActivity(),
				MyContentProvider.HOLELOG_CONTENT_URI, null, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
		mAdapter = new HoleLogAdapter(getActivity(), cursor);
		mListView.setAdapter(mAdapter);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		mAdapter = new HoleLogAdapter(getActivity(), null);
		mListView.setAdapter(mAdapter);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

}
