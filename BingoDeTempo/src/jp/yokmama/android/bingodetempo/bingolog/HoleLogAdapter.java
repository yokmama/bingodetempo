package jp.yokmama.android.bingodetempo.bingolog;

import jp.yokmama.android.bingodetempo.R;
import jp.yokmama.android.bingodetempo.core.Utils;
import jp.yokmama.android.bingodetempo.db.TableColumns.HoleLog;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class HoleLogAdapter extends CursorAdapter {
	private LayoutInflater mInfalator;

	public HoleLogAdapter(Context context, Cursor cursor) {
		super(context, cursor, true);
		mInfalator = LayoutInflater.from(context);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();

		int type = cursor.getInt(cursor.getColumnIndex(HoleLog.TYPE));
		int no = cursor.getInt(cursor.getColumnIndex(HoleLog.NO));
		long date = cursor.getLong(cursor.getColumnIndex(HoleLog.CREATED_DATE));
		
		holder.textDate.setText(Utils.dateToString(date));
		if(type == HoleLog.TYPE_HOLE){
			holder.imageBackground.setImageResource(R.drawable.bingolog_background1);
			holder.textMessage.setText("「"+no+"」が抽選されました。");
		}else if(type == HoleLog.TYPE_REACH){
			holder.imageBackground.setImageResource(R.drawable.bingolog_background2);
			holder.textMessage.setText(Utils.getReachMessage(mContext, no));
		}else if(type == HoleLog.TYPE_BINGO){
			holder.imageBackground.setImageResource(R.drawable.bingolog_background3);
			holder.textMessage.setText(Utils.getBingoMessage(mContext, no));
		}
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View v = mInfalator.inflate(R.layout.bingo_log_item, parent,
				false);
		ViewHolder holder = new ViewHolder();
		holder.imageBackground = (ImageView)v.findViewById(R.id.imageBackground);
		holder.textDate = (TextView) v.findViewById(R.id.textDate);
		holder.textMessage = (TextView) v.findViewById(R.id.textMessage);

		v.setTag(holder);
		return v;
	}

	private class ViewHolder {
		ImageView imageBackground;
		TextView textDate;
		TextView textMessage;
	}
}
