package jp.yokmama.android.bingodetempo.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import jp.yokmama.android.bingodetempo.bean.HoleItem;
import jp.yokmama.android.bingodetempo.bean.StoreItem;
import jp.yokmama.android.bingodetempo.db.MyContentProvider;
import jp.yokmama.android.bingodetempo.db.TableColumns.Hole;
import jp.yokmama.android.bingodetempo.db.TableColumns.Store;
import android.content.ContentProviderClient;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

public class DatabaseHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private static final Random _rand = new Random(System.currentTimeMillis());

    public static final int SELECT_HOLE_ALL = 0;
    public static final int SELECT_HOLE_PUNCH = 1;
    public static final int SELECT_HOLE_UNPUNCH = 2;

    public static int getCount(Context context, Uri uri, String where,
            String[] whereArgs) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri,
                    new String[] { "count(*)" }, where, whereArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                return cursor.getInt(0);
            }
            return 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    
    public static int getCount(ContentProviderClient client, Uri uri, String where,
            String[] whereArgs) throws RemoteException {
        Cursor cursor = null;
        try {
            cursor = client.query(uri,
                    new String[] { "count(*)" }, where, whereArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                return cursor.getInt(0);
            }
            return 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static List<HoleItem> selectHoles(Context context, int selectType) {

        long selectTime = getFirstTime();

        Log.d(TAG, "selecttime = " + selectTime);

        // 最新のGameを取得する
        int count = DatabaseHelper.getCount(context,
                MyContentProvider.HOLE_CONTENT_URI, null, null);
        if (count == 0) {
            return createHole(context);
        } else {
            return selectHole(context, selectType);
        }
    }

    public static List<HoleItem> selectHole(Context context,int selectType) {
        List<HoleItem> result = new ArrayList<HoleItem>();

        String where = null;
        String[] whereArgs = null;
        if (selectType == SELECT_HOLE_PUNCH) {
            where = Hole.PUNCH + " = ?";
            whereArgs = new String[] { "1" };
        } else if (selectType == SELECT_HOLE_UNPUNCH) {
                where = Hole.PUNCH + " = ?";
                whereArgs = new String[] { "0" };
        } else {
            where = null;
            whereArgs = null;
        }

        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(
                    MyContentProvider.HOLE_CONTENT_URI, null, where, whereArgs,
                    Hole.INDEX);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    HoleItem item = new HoleItem(cursor);
                    result.add(item);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return result;
    }

    public static List<HoleItem> createHole(Context context) {
        List<HoleItem> result = new ArrayList<HoleItem>();

        // 100の配列を作成
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        for (int i = 0; i < 100; i++) {
            numbers.add(i + 1);
        }

        long now = System.currentTimeMillis();
        // 25個の数値を無作為に抽出
        for (int i = 0; i < 25; i++) {
            int number = i == 12 ? 0 : selectNumber(numbers);
            int punch = i==12?1:0;
            HoleItem item = new HoleItem();
            item.setIndex(i + 1);
            item.setNo(number);
            item.setPunch(punch);
            item.setModifiedDate(now);

            ContentValues values = new ContentValues();
            values.put(Hole.INDEX, i + 1);
            values.put(Hole.PUNCH, punch);
            values.put(Hole.NO, number);
            values.put(Hole.MODIFIED_DATE, now);

            Uri uri = context.getContentResolver().insert(
                    MyContentProvider.HOLE_CONTENT_URI, values);
            item.setId(ContentUris.parseId(uri));

            result.add(item);
        }

        return result;
    }

    public static int selectNumber(ArrayList<Integer> numbers) {
        int n = _rand.nextInt(numbers.size());
        int select = numbers.remove(n);
        return select;
    }

    private static long getFirstTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis();
    }

	public static StoreItem getStore(Context context, String iDm) {
		Cursor cursor = null;
		try{
			cursor = context.getContentResolver().query(MyContentProvider.STORE_CONTENT_URI, null, Store.NFCID + " = ?", new String[]{iDm}, null);
			if(cursor != null && cursor.moveToFirst()){
				return new StoreItem(cursor);
			}
		}
		finally{
			if(cursor !=null){
				cursor.close();
			}
		}
		int count = getCount(context, MyContentProvider.STORE_CONTENT_URI, null, null);
		
		String name = "名無し";
		long now = System.currentTimeMillis();
		if(count < stores.length){
			name = stores[count];
		}
		
		StoreItem item = new StoreItem();
		item.setIDm(iDm);
		item.setName(name);
		item.setCreatedDate(now);
		item.setModifiedDate(0);
		
		ContentValues values = new ContentValues();
		values.put(Store.NAME, name);
		values.put(Store.NFCID, iDm);
		values.put(Store.CREATED_DATE, now);
		values.put(Store.MODIFIED_DATE, 0);
		
		Uri uri = context.getContentResolver().insert(MyContentProvider.STORE_CONTENT_URI, values);
		item.setId(ContentUris.parseId(uri));
		
		return item;
	}
	
	static String[] stores = new String[]{
		"オリエンタルカフェ", 
		"クルーズカフェ", 
		"だいぶっちゃん", 
		"華のしずく",
		"カレー屋けんちゃん",
		"菓子処どらごん屋",
		"カラオケうたひめ",
		"原フルーツ",
		"肉屋のてっちゃん",
		"あらき鮮魚店",
		"おみやげのあかし屋",
		"たこやき金八",
		"あかちゃんどう",
		"喫茶　噂のカフェ",
		};
	
}
