package jp.yokmama.android.bingodetempo.utils;

import jp.yokmama.android.bingodetempo.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class DialogHelper {
	
	public static void NoticeDialog(Context context, int icon, String title,
            String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setIcon(icon);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.create().show();
    }

    public static void YesNoDialog(Context context, int icon, String title,
            String message, String yesBtnText, String noBtnText,
            DialogInterface.OnClickListener yesBtnListner,
            DialogInterface.OnClickListener noBtnListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setIcon(icon);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(yesBtnText, yesBtnListner);
        builder.setNegativeButton(noBtnText, noBtnListener);
        builder.create().show();
    }
    
    public static void ErrorDialog(Context context, int icon,
            String message, String closeBtnText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setIcon(icon);
        builder.setTitle(context.getString(R.string.common_name_error));
        builder.setMessage(message);
        builder.setPositiveButton(closeBtnText, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                
            }});
        builder.create().show();
    }
}
