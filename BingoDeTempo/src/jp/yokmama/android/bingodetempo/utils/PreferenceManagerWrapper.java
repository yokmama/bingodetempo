package jp.yokmama.android.bingodetempo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

abstract public class PreferenceManagerWrapper {
	static private final String XML_FILE = "mypreference.xml";
	static private final int FILE_MODE = Context.MODE_WORLD_READABLE
			| Context.MODE_WORLD_WRITEABLE;

	private Context context;
	private SharedPreferences defaultPref;
	private SharedPreferences sharedPref;
	private Editor defaultEditor;
	private Editor sharedEditor;

	public PreferenceManagerWrapper(Context context) {
		this.context = context;
	}
	
	public Context getContext(){
	    return context;
	}

	protected SharedPreferences getDefaultPref() {
		if (defaultPref == null) {
			defaultPref = PreferenceManager
					.getDefaultSharedPreferences(context);
		}
		return defaultPref;
	}

	protected SharedPreferences getSharedPref() {
		if (sharedPref == null) {
			sharedPref = context.getSharedPreferences(XML_FILE, FILE_MODE);
		}
		return sharedPref;
	}

	protected Editor getDeafultEditor() {
		if (defaultEditor == null) {
			defaultEditor = getDefaultPref().edit();
		}
		return defaultEditor;
	}

	protected Editor getSharedEditor() {
		if (sharedEditor == null) {
			sharedEditor = getSharedPref().edit();
		}
		return sharedEditor;
	}

	public void commit() {
		if (defaultEditor != null) {
			defaultEditor.commit();
			defaultEditor = null;
		}
		if (sharedEditor != null) {
			sharedEditor.commit();
			sharedEditor = null;
		}
	}
	
	protected int parseInt(String s){
		if(s!=null && s.length()>0){
			try{
				return Integer.parseInt(s);
			}catch(Exception e){
			}
		}
		return 0;
	}
	
	protected long parseLong(String s){
		if(s!=null && s.length()>0){
			try{
				return Long.parseLong(s);
			}catch(Exception e){
			}
		}
		return 0;
	}
	
	protected boolean parseBoolean(String s){
		if(s!=null && s.length()>0){
			try{
				return Boolean.parseBoolean(s);
			}catch(Exception e){
			}
		}
		return false;
	}
	
	public int getIntValue(int res){
		return parseInt(context.getResources().getString(res));
	}
	
	public long getLongValue(int res){
		return parseLong(context.getResources().getString(res));
	}
	
	public boolean getBooleanValue(int res){
		return parseBoolean(context.getResources().getString(res));
	}
	
	public String getStringValue(int res){
		return context.getResources().getString(res);
	}
	

	public int getInt(String key, int value) {
		return getDefaultPref().getInt(key, value);
	}

	public void putInt(String key, int value) {
		getDeafultEditor().putInt(key, value);
	}

	public long getLong(String key, long value) {
		return getDefaultPref().getLong(key, value);
	}

	public void putLong(String key, long value) {
		getDeafultEditor().putLong(key, value);
	}

	public boolean getBoolean(String key, boolean value) {
		return getDefaultPref().getBoolean(key, value);
	}

	public void putBoolean(String key, boolean value) {
		getDeafultEditor().putBoolean(key, value);
	}

	public String getString(String key, String value) {
		return getDefaultPref().getString(key, value);
	}

	public void putString(String key, String value) {
		getDeafultEditor().putString(key, value);
	}
}
