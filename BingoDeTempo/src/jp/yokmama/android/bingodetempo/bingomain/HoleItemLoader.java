package jp.yokmama.android.bingodetempo.bingomain;

import java.util.List;

import jp.yokmama.android.bingodetempo.bean.HoleItem;
import jp.yokmama.android.bingodetempo.core.Consts;
import jp.yokmama.android.bingodetempo.db.MyContentProvider;
import jp.yokmama.android.bingodetempo.utils.DatabaseHelper;
import android.content.Context;
import android.os.Handler;
import android.util.Log;


public class HoleItemLoader implements Runnable {
    private static final String TAG = HoleItemLoader.class.getSimpleName();
    private Context mContext;
    private Handler mHandler;

    public HoleItemLoader(Context context, Handler handler) {
    	mContext = context;
    	mHandler = handler;
    }

	@Override
	public void run() {
    	Log.d(TAG, "HoleItemLoader.run");
        int count = DatabaseHelper.getCount(mContext,
                MyContentProvider.HOLE_CONTENT_URI, null, null);
        if (count == 0) {
        	List<HoleItem> items =  DatabaseHelper.createHole(mContext);
        	mHandler.sendMessage(mHandler.obtainMessage(Consts.MSG_LOAD_LIST, items));
        } else {
        	List<HoleItem> items =  DatabaseHelper.selectHole(mContext, DatabaseHelper.SELECT_HOLE_ALL);
        	mHandler.sendMessage(mHandler.obtainMessage(Consts.MSG_LOAD_LIST, items));
        }
	}
}
