package jp.yokmama.android.bingodetempo.bingomain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.yokmama.android.bingodetempo.R;
import jp.yokmama.android.bingodetempo.bean.HoleItem;
import jp.yokmama.android.bingodetempo.core.Consts;
import jp.yokmama.android.bingodetempo.core.MyPreferenceManager;
import jp.yokmama.android.bingodetempo.db.MyContentProvider;
import jp.yokmama.android.bingodetempo.db.TableColumns.HoleLog;
import jp.yokmama.android.bingodetempo.utils.DatabaseHelper;
import android.content.ContentValues;
import android.database.ContentObserver;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class BingoFragment extends Fragment {
    private static final String TAG = BingoFragment.class.getSimpleName();
	
	private static final int[] _labelIndex = new int[] { R.id.labelB,
			R.id.labelI, R.id.labelN, R.id.labelG, R.id.labelO };

	private static final int[] _holeindex = new int[] { R.id.button1,
			R.id.button2, R.id.button3, R.id.button4, R.id.button5,
			R.id.button6, R.id.button7, R.id.button8, R.id.button9,
			R.id.button10, R.id.button11, R.id.button12, R.id.button13,
			R.id.button14, R.id.button15, R.id.button16, R.id.button17,
			R.id.button18, R.id.button19, R.id.button20, R.id.button21,
			R.id.button22, R.id.button23, R.id.button24, R.id.button25, };

	private List<HoleItem> mHoles;

	private Map<View, ImageView> mParticles = new HashMap<View, ImageView>();

	private Map<View, View> mBingLabels = new HashMap<View, View>();

	private View mImgBingo;

	private View mImgReach;

	private View mImgEntry;

	public Handler mHandler;
	private ContentObserver mObserver;
    private MyPreferenceManager mPreference;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        mPreference = new MyPreferenceManager(getActivity());

        View view = inflater.inflate(R.layout.bingo_main, container, false);
		ViewGroup particleLayout = (ViewGroup) view
				.findViewById(R.id.particleLayout);

		for (int i = 0; i < _holeindex.length; i++) {
			Button btn = (Button) view.findViewById(_holeindex[i]);
			if (i != 12) {
				btn.setText("");
			} else {
				btn.setSelected(true);
			}
			{ // パーティクル用のビューを作成する
				ImageView particle;
				particle = new ImageView(getActivity());
				particle.setImageResource(R.drawable.particle_flush02_n02p4);
				particleLayout.addView(particle);
				particle.setVisibility(View.INVISIBLE);
				mParticles.put(btn, particle);
			}
			{
				View label = view.findViewById(_labelIndex[i
						% _labelIndex.length]);
				mBingLabels.put(btn, label);
			}
		}

		mImgBingo = view.findViewById(R.id.img_bingo);
		mImgReach = view.findViewById(R.id.img_reach);
		mImgEntry = view.findViewById(R.id.img_entry);
		
		setAdmitCount(view);

		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");

		Log.d(TAG, "initLoader:"+isVisible());
		
		mHandler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				Log.d(TAG, "restartLoader:"+isVisible());
				if(msg.what == Consts.MSG_LOAD_LIST){
					mHoles = (List<HoleItem>)msg.obj;
					refleshView();
				}else if(msg.what == Consts.MSG_RELOAD_LIST){
					Thread t = new Thread(new HoleItemLoader(getActivity(), mHandler));
					t.start();
				}
				return false;
			}
		});
		mObserver = new ContentObserver(mHandler) {
			@Override
			public void onChange(boolean selfChange) {
				super.onChange(selfChange);
				Log.d(TAG, "onChange:"+isVisible());
				mHandler.sendEmptyMessage(Consts.MSG_RELOAD_LIST);
			}
		};
		getActivity().getContentResolver().registerContentObserver(
				MyContentProvider.HOLE_CONTENT_URI, false, mObserver);

	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "onResume");
		mHandler.sendEmptyMessage(Consts.MSG_RELOAD_LIST);
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mObserver != null) {
			getActivity().getContentResolver().unregisterContentObserver(
					mObserver);
			mObserver = null;
		}

		getLoaderManager().destroyLoader(R.layout.bingo_main);
	}
	
	private void setAdmitCount(View view){
		LayoutInflater inflater = getActivity().getLayoutInflater();
		LinearLayout layout = (LinearLayout)view.findViewById(R.id.layoutAdmit);
		int currentCount = layout.getChildCount();
		int admintCount = mPreference.getAdmitCount();
		if(currentCount != admintCount){
			if(currentCount>admintCount){
				//減らす処理
				while(layout.getChildCount()>admintCount){
					View v = layout.getChildAt(layout.getChildCount()-1);
					layout.removeView(v);
				}
			}else{
				//増やす処理
				int n = admintCount - currentCount;
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
				for(int i=0; i<n; i++){
					View v = inflater.inflate(R.layout.bingo_pool_item, layout, false);
					layout.addView(v, params);
				}
			}
		}
	}

	private boolean refleshView() {
		Log.d(TAG, "hole size="+mHoles.size());
		if (mHoles != null && mHoles.size() >= _holeindex.length && getView() != null) {
			Log.d("uga!!!", "refleshView　IN:"+isVisible());
			setAdmitCount(getView());
			
			long lastDisplayModifiedDate = mPreference.getLastDisplayedModifiedDate();
			long nextLastDisplayModifiedDate = lastDisplayModifiedDate;

			for (int i = 0; i < _holeindex.length; i++) {
				Button btn = (Button) getView().findViewById(_holeindex[i]);
				HoleItem item = mHoles.get(i);
				if (item.getNo() > 0) {
					btn.setText(Integer.toString(item.getNo()));
				}
				btn.setSelected(item.getPunch() == 1);
				if (lastDisplayModifiedDate < item.getModifiedDate()) {
					nextLastDisplayModifiedDate = Math.max(
							lastDisplayModifiedDate, item.getModifiedDate());
					// アニメーションを行う
					startGooutAnimation(btn, true);
				}
			}
			if (lastDisplayModifiedDate != nextLastDisplayModifiedDate) {
				mPreference.setLastDisplayedModifiedDate(nextLastDisplayModifiedDate);
				mPreference.commit();
			}
			
			bingoEventCheck();
			return true;
		}else{
			return false;
		}
	}

	/**
	 * ボタンの更新時のアニメーションを開始します。
	 * 
	 * @param view
	 *            対象となるView
	 */
	private void startGooutAnimation(final View view, boolean withFlash) {
		{ // ボタン自体のアニメーションを設定する
			Animation anim = AnimationUtils.loadAnimation(getActivity(),
					R.anim.button_goout);
			view.startAnimation(anim);
		}
		{
			View label = mBingLabels.get(view);
			if (label != null) {
				startKnockAnimation(label);
			}
		}

		if (withFlash) { // パーティクルを設定する
			ViewGroup particleLayout = (ViewGroup) getView().findViewById(
					R.id.particleLayout);
			final View particle = mParticles.get(view);
			Animation anim = AnimationUtils.loadAnimation(getActivity(),
					R.anim.flash);
			anim.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					particle.setVisibility(View.INVISIBLE);
				}
			});

			Point viewGp = new Point();
			Rect viewGr = new Rect();
			view.getGlobalVisibleRect(viewGr, viewGp);
			Point layoutGp = new Point();
			Rect layoutGr = new Rect();
			particleLayout.getGlobalVisibleRect(layoutGr, layoutGp);
			particle.layout(//
					viewGr.left - layoutGp.x,//
					viewGr.top - layoutGp.y, //
					viewGr.right - layoutGp.x,//
					viewGr.bottom - layoutGp.y);
			particle.setVisibility(View.VISIBLE);
			particle.setAnimation(anim);
		}
	}

	private void startKnockAnimation(View view) {
		Animation anim = AnimationUtils.loadAnimation(getActivity(),
				R.anim.anim_knock);
		view.startAnimation(anim);
	}

	public void startReachAnimation() {
		Animation anim = AnimationUtils.loadAnimation(getActivity(),
				R.anim.anim_reach);
		mImgReach.setVisibility(View.VISIBLE);
		mImgReach.startAnimation(anim);
	}

	public void startBingoAnimation() {
		Animation anim = AnimationUtils.loadAnimation(getActivity(),
				R.anim.anim_bingo);
		mImgBingo.setVisibility(View.VISIBLE);
		mImgBingo.startAnimation(anim);
	}

	public void startEntryAnimation() {
		Animation anim = AnimationUtils.loadAnimation(getActivity(),
				R.anim.anim_entry);
		mImgEntry.setVisibility(View.VISIBLE);
		mImgEntry.startAnimation(anim);
	}

	private void bingoEventCheck() {
		// BingoCheck
		int new_bingo_count = DatabaseHelper.getCount(getActivity(),
				MyContentProvider.HOLELOG_CONTENT_URI, HoleLog.TYPE + " = ? AND "+HoleLog.CONSUMED + " = 0",
				new String[] { Integer.toString(HoleLog.TYPE_BINGO) });
		if(new_bingo_count>0){
			startBingoAnimation();
			ContentValues values = new ContentValues();
			values.put(HoleLog.CONSUMED, 1);
			getActivity().getContentResolver().update(MyContentProvider.HOLELOG_CONTENT_URI, values, null, null);
		}
		else{
			int new_reach_count = DatabaseHelper.getCount(getActivity(),
					MyContentProvider.HOLELOG_CONTENT_URI, HoleLog.TYPE + " = ? AND "+HoleLog.CONSUMED + " = 0",
					new String[] { Integer.toString(HoleLog.TYPE_REACH) });
			if(new_reach_count>0){
				startReachAnimation();
				ContentValues values = new ContentValues();
				values.put(HoleLog.CONSUMED, 1);
				getActivity().getContentResolver().update(MyContentProvider.HOLELOG_CONTENT_URI, values, null, null);
			}
		}
		
	}

}
