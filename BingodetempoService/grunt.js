module.exports = function(grunt) {

    "use strict";

    grunt.initConfig({
        lint: {
            all: [
                'js_src/**/*.js'
            ]
        },
        concat: {
            'war/js/lot.js': [
//            'js_build/lot.concat.js': [
                'js_src/Utils.js',
                'js_src/Preload.js',
                'js_src/BingoEvents.js',
                'js_src/Lot.js',
            ]
        },
//        min: {
//            'war/js/lot.min.js': 'js_build/lot.concat.js'
//        },
        watch: {
            files: [
                'grunt.js',
                'js_src/**/*.js'
            ],
            tasks: 'default'
        }
    });

    grunt.registerTask('default', [
                       'lint',
                       'concat',
//                       'min'
    ]);
};
