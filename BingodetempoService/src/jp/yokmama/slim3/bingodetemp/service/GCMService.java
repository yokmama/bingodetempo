package jp.yokmama.slim3.bingodetemp.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Transaction;

public class GCMService {
    private static final int MULTICAST_SIZE = 1000;
    private static final String DEVICE_TYPE = "Device";
    private static final String DEVICE_REG_ID_PROPERTY = "regId";

    public static final String MULTICAST_TYPE = "Multicast";
    public static final String MULTICAST_REG_IDS_PROPERTY = "regIds";

    private static final FetchOptions DEFAULT_FETCH_OPTIONS =
        FetchOptions.Builder
            .withPrefetchSize(MULTICAST_SIZE).chunkSize(MULTICAST_SIZE);

    private final DatastoreService datastore =
        DatastoreServiceFactory.getDatastoreService();

    private final Logger logger = Logger.getLogger(getClass().getName());

    private Entity findDeviceByRegId(String regId) {
        Query query = new Query(DEVICE_TYPE)
            .addFilter(DEVICE_REG_ID_PROPERTY, FilterOperator.EQUAL, regId);
        PreparedQuery preparedQuery = datastore.prepare(query);
        List<Entity> entities = preparedQuery.asList(DEFAULT_FETCH_OPTIONS);
        Entity entity = null;
        if (!entities.isEmpty()) {
            entity = entities.get(0);
        }
        int size = entities.size();
        if (size > 0) {
            logger.severe(
                "Found "
                    + size
                    + " entities for regId "
                    + regId
                    + ": "
                    + entities);
        }
        return entity;
    }

    private void retryTask(HttpServletResponse resp) {
        resp.setStatus(500);
    }

    public void taskDone(HttpServletResponse resp) {
        resp.setStatus(200);
    }

    private void updateRegistration(String oldId, String newId) {
        logger.info("Updating " + oldId + " to " + newId);
        Transaction txn = datastore.beginTransaction();
        try {
            Entity entity = findDeviceByRegId(oldId);
            if (entity == null) {
                logger.warning("No device for registration id " + oldId);
                return;
            }
            entity.setProperty(DEVICE_REG_ID_PROPERTY, newId);
            datastore.put(entity);
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    private void unregister(String regId) {
        logger.info("Unregistering " + regId);
        Transaction txn = datastore.beginTransaction();
        try {
            Entity entity = findDeviceByRegId(regId);
            if (entity == null) {
                logger.warning("Device " + regId + " already unregistered");
            } else {
                Key key = entity.getKey();
                datastore.delete(key);
            }
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    private void deleteMulticast(String encodedKey) {
        Transaction txn = datastore.beginTransaction();
        try {
            Key key = KeyFactory.stringToKey(encodedKey);
            datastore.delete(key);
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    private void multicastDone(HttpServletResponse resp, String encodedKey) {
        deleteMulticast(encodedKey);
        taskDone(resp);
    }

    private void updateMulticast(String encodedKey, List<String> devices) {
        Key key = KeyFactory.stringToKey(encodedKey);
        Entity entity;
        Transaction txn = datastore.beginTransaction();
        try {
            try {
                entity = datastore.get(key);
            } catch (EntityNotFoundException e) {
                logger.severe("No entity for key " + key);
                return;
            }
            entity.setProperty(MULTICAST_REG_IDS_PROPERTY, devices);
            datastore.put(entity);
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    public List<String> getMulticast(String encodedKey) {
        Key key = KeyFactory.stringToKey(encodedKey);
        Entity entity;
        Transaction txn = datastore.beginTransaction();
        try {
            entity = datastore.get(key);
            @SuppressWarnings("unchecked")
            List<String> devices =
                (List<String>) entity.getProperty(MULTICAST_REG_IDS_PROPERTY);
            txn.commit();
            return devices;
        } catch (EntityNotFoundException e) {
            logger.severe("No entity for key " + key);
            return Collections.emptyList();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    public void sendSingleMessage(Sender sender, String regId, Message message,
            HttpServletResponse resp) {
        if(regId == null){
            logger.info("can not send message. regId is null ");
            return;
        }
        logger.info("Sending message to device " + regId);
        Result result;
        try {
            result = sender.sendNoRetry(message, regId);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Exception posting " + message, e);
            taskDone(resp);
            return;
        }
        if (result == null) {
            retryTask(resp);
            return;
        }
        if (result.getMessageId() != null) {
            logger.info("Succesfully sent message to device " + regId);
            String canonicalRegId = result.getCanonicalRegistrationId();
            if (canonicalRegId != null) {
                logger.finest("canonicalRegId " + canonicalRegId);
                updateRegistration(regId, canonicalRegId);
            }
        } else {
            String error = result.getErrorCodeName();
            if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
                unregister(regId);
            } else {
                logger.severe("Error sending message to device " + regId
                    + ": " + error);
            }
        }
    }

    public void sendMulticastMessage(Sender sender, String multicastKey,
            Message message,
            HttpServletResponse resp) {
        List<String> regIds = getMulticast(multicastKey);
        MulticastResult multicastResult;
        try {
            multicastResult = sender.sendNoRetry(message, regIds);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Exception posting " + message, e);
            multicastDone(resp, multicastKey);
            return;
        }
        boolean allDone = true;
        if (multicastResult.getCanonicalIds() != 0) {
            List<Result> results = multicastResult.getResults();
            for (int i = 0; i < results.size(); i++) {
                String canonicalRegId =
                    results.get(i).getCanonicalRegistrationId();
                if (canonicalRegId != null) {
                    String regId = regIds.get(i);
                    updateRegistration(regId, canonicalRegId);
                }
            }
        }
        if (multicastResult.getFailure() != 0) {
            List<Result> results = multicastResult.getResults();
            List<String> retriableRegIds = new ArrayList<String>();
            for (int i = 0; i < results.size(); i++) {
                String error = results.get(i).getErrorCodeName();
                if (error != null) {
                    String regId = regIds.get(i);
                    logger.warning("Got error ("
                        + error
                        + ") for regId "
                        + regId);
                    if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
                        unregister(regId);
                    }
                    if (error.equals(Constants.ERROR_UNAVAILABLE)) {
                        retriableRegIds.add(regId);
                    }
                }
            }
            if (!retriableRegIds.isEmpty()) {
                updateMulticast(multicastKey, retriableRegIds);
                allDone = false;
                retryTask(resp);
            }
        }
        if (allDone) {
            multicastDone(resp, multicastKey);
        } else {
            retryTask(resp);
        }
    }

}
