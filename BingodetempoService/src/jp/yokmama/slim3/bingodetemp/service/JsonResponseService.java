package jp.yokmama.slim3.bingodetemp.service;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slim3.controller.Navigation;

public class JsonResponseService {
    private final static Logger LOG = Logger
        .getLogger(JsonResponseService.class.getName());

    /**
     * JSON形式でNGを返します。
     * 
     * @throws IOException
     */
    public Navigation returnNg(HttpServletResponse response, Object obj)
            throws IOException {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", "ng");
            jsonObject.put("results", obj);
            response.getWriter().append(jsonObject.toString());
            response.flushBuffer();
        } catch (JSONException e) {
            LOG.severe(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * JSON形式でOKを返します。
     * 
     * @throws IOException
     */
    public Navigation returnOk(HttpServletResponse response, Object obj)
            throws IOException {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ok");
            jsonObject.put("results", obj);
            response.getWriter().append(jsonObject.toString());
            response.flushBuffer();
        } catch (JSONException e) {
            LOG.severe(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
    
    public static String getJsonContentType() {
        return "application/json";
    }

    public void settings(HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
    }
}
