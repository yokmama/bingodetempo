package jp.yokmama.slim3.bingodetemp.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jp.yokmama.slim3.bingodetemp.meta.LotHistoryMeta;
import jp.yokmama.slim3.bingodetemp.meta.ParticipationMeta;
import jp.yokmama.slim3.bingodetemp.meta.PrimarilyUserMeta;
import jp.yokmama.slim3.bingodetemp.meta.StoreMeta;
import jp.yokmama.slim3.bingodetemp.model.LotHistory;
import jp.yokmama.slim3.bingodetemp.model.Participation;
import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.model.SecondaryUser;
import jp.yokmama.slim3.bingodetemp.model.Store;
import jp.yokmama.slim3.bingodetemp.utils.SSProperties;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.ModelQuery;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.data.spreadsheet.CustomElementCollection;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;

public class DatabaseService {
    private static final DatastoreService datastore = DatastoreServiceFactory
        .getDatastoreService();

    public void clearStorelist() {
        List<Key> keys = Datastore.query(StoreMeta.get()).asKeyList();
        if (keys != null && keys.size() > 0) {
            Datastore.delete(keys);
        }
    }

    public void clearPaticipationlist() {
        List<Key> keys = Datastore.query(ParticipationMeta.get()).asKeyList();
        if (keys != null && keys.size() > 0) {
            Datastore.delete(keys);
        }
    }
    
    public void clearPoolNumbers() {
        List<PrimarilyUser> asList = Datastore.query(PrimarilyUserMeta.get()).asList();
        if (asList != null) {
            for (Iterator<PrimarilyUser> ite = asList.iterator(); ite.hasNext();) {
                PrimarilyUser user = ite.next();
                user.setPoolnumbers(new ArrayList<Integer>());
                Datastore.put(user);
            }
        }
    }
    
    public void clearPoolNumbers(List<PrimarilyUser> asList) {
        if (asList != null) {
            for (Iterator<PrimarilyUser> ite = asList.iterator(); ite.hasNext();) {
                PrimarilyUser user = ite.next();
                user.setPoolnumbers(new ArrayList<Integer>());
                Datastore.put(user);
            }
        }
    }
    
    public void clearPoolNumbers(Key userKey) {
        PrimarilyUser user = Datastore.get(PrimarilyUser.class, userKey);
        if (user != null) {
            user.setPoolnumbers(new ArrayList<Integer>());
            Datastore.put(user);
        }
    }

    public PrimarilyUser getPrimarilyUser(Key key) {
        PrimarilyUser obj = null;
        try {
            obj = Datastore.get(PrimarilyUser.class, key);
        } catch (Exception e) {
            System.out.println("ERROR e:" + e.toString());
        }
        return obj;
    }
    
    public List<PrimarilyUser> getPrimarilyUsers() {
        List<PrimarilyUser> users = new ArrayList<PrimarilyUser>();
        List<Key> keys = Datastore.query(PrimarilyUserMeta.get()).asKeyList();
        if (keys != null && keys.size() > 0) {
            for (Key key : keys) {
                PrimarilyUser user = getPrimarilyUser(key);
                if (user != null) {
                    users.add(user);
                }
            }
        }
        return users;
    }

    public SecondaryUser getSecondaryUser(Key key) {
        SecondaryUser obj = null;
        try {
            obj = Datastore.get(SecondaryUser.class, key);
        } catch (Exception e) {
            System.out.println("ERROR e:" + e.toString());
        }
        return obj;
    }

    public Store getStore(Key key) {
        Store obj = null;
        try {
            obj = Datastore.get(Store.class, key);
        } catch (Exception e) {
            System.out.println("ERROR e:" + e.toString());
        }
        return obj;

    }

    public static String createMulticast(List<String> devices) {
        String encodedKey;
        Transaction txn = datastore.beginTransaction();
        try {
            Entity entity = new Entity(GCMService.MULTICAST_TYPE);
            entity.setProperty(GCMService.MULTICAST_REG_IDS_PROPERTY, devices);
            datastore.put(entity);
            Key key = entity.getKey();
            encodedKey = KeyFactory.keyToString(key);
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
        return encodedKey;
    }

    public Participation getParticipation(Key key) {
        Participation obj = null;
        try {
            obj = Datastore.get(Participation.class, key);
        } catch (Exception e) {
            System.out.println("ERROR e:" + e.toString());
        }
        return obj;

    }

    public Set<Key> getFriends(Key key) {
        PrimarilyUser obj = null;
        try {
            obj = Datastore.get(PrimarilyUser.class, key);
            if (obj != null) {
                obj.getFriends();
            }
        } catch (Exception e) {
            System.out.println("ERROR e:" + e.toString());
        }
        return null;
    }

    public void importStorelist() throws IOException, ServiceException,
            JSONException {

        SSProperties prop = SSProperties.getInstance();

        SpreadsheetEntry spreadsheetEntry = prop.loadSpreadSheetEntry("店舗一覧");
        WorksheetEntry worksheetEntry = spreadsheetEntry.getDefaultWorksheet();

        ListQuery listQuery = new ListQuery(worksheetEntry.getListFeedUrl());
        ListFeed listFeed = prop.getService().query(listQuery, ListFeed.class);
        int size = listFeed.getEntries().size();
        for (int i = 0; i < size; i++) {
            ListEntry listEntry = listFeed.getEntries().get(i);
            CustomElementCollection elements = listEntry.getCustomElements();

            String title = elements.getValue("店名");
            String addr = elements.getValue("住所");
            Key key = Store.createKey(title, addr);
            Store shop = getStore(key);
            if (shop != null) {
                Transaction tx = Datastore.beginTransaction();
                setStoreData(elements, shop, listEntry);
                Datastore.put(shop);
                tx.commit();
            } else {
                Transaction tx = Datastore.beginTransaction();
                shop = new Store();
                shop.setKey(key);
                shop.setTitle(title);
                setStoreData(elements, shop, listEntry);
                Datastore.put(shop);
                tx.commit();
            }
        }
    }

    public void setStoreData(CustomElementCollection elements, Store shop,
            ListEntry listEntry) throws JSONException, IOException,
            ServiceException {
        String yomigana = elements.getValue("ヨミガナ");
        String type = elements.getValue("種別");
        String desc = elements.getValue("備考");
        String phone = elements.getValue("電話番号");
        String opening = elements.getValue("営業時間");
        String address = elements.getValue("住所");
        String holiday = elements.getValue("定休日");
        String traffic = elements.getValue("交通手段");
        String card = elements.getValue("カード決済");
        String budget = elements.getValue("料金");
        String web = elements.getValue("WEB");
        String slng = elements.getValue("経度");
        String slat = elements.getValue("緯度");

        if (!Utils.isNull(yomigana)) {
            shop.setYomigana(yomigana);
        }
        if (!Utils.isNull(type)) {
            shop.setType(type);
        }
        if (!Utils.isNull(desc)) {
            shop.setDesc(desc);
        }
        if (!Utils.isNull(phone)) {
            shop.setPhone(phone);
        }
        if (!Utils.isNull(opening)) {
            shop.setOpening(opening);
        }
        if (!Utils.isNull(address)) {
            shop.setAddress(address);

            StringBuilder path = new StringBuilder();
            path.append("http://maps.googleapis.com/maps/api/geocode/json?");
            path.append("address=").append(URLEncoder.encode(address, "UTF-8"));
            path.append("&language=ja&sensor=false");
            String text = Utils.getHttpResponse(path.toString());

            JSONObject json = new JSONObject(text);
            JSONArray results = json.getJSONArray("results");
            if (results != null && results.length() > 0) {
                JSONObject item = results.getJSONObject(0);
                JSONObject geom = item.getJSONObject("geometry");
                if (geom != null) {
                    JSONObject loc = geom.getJSONObject("location");
                    long lat = (long) (loc.getDouble("lat") * 1E6);
                    long lng = (long) (loc.getDouble("lng") * 1E6);
                    shop.setLnglat(lng + "," + lat);

                    StringBuilder mapurl = new StringBuilder();
                    mapurl.append("http://maps.google.co.jp/maps?");
                    mapurl
                        .append(loc.getDouble("lat"))
                        .append(",")
                        .append(loc.getDouble("lng"))
                        .append("(")
                        .append(shop.getTitle())
                        .append(")&hl=ja");

                    listEntry.getCustomElements().setValueLocal(
                        "MAP",
                        mapurl.toString());
                    listEntry.getCustomElements().setValueLocal(
                        "緯度",
                        Long.toString(lat));
                    listEntry.getCustomElements().setValueLocal(
                        "経度",
                        Long.toString(lng));

                    // 電話番号
                    // Webサイト
                    if (Utils.isNull(shop.getPhone())
                        || Utils.isNull(shop.getWeb())) {
                        setPlaceInfo(
                            listEntry,
                            shop,
                            loc.getDouble("lat"),
                            loc.getDouble("lng"));
                    }

                    listEntry.update();
                }
            }
        }
        if (!Utils.isNull(holiday)) {
            shop.setHoliday(holiday);
        }
        if (!Utils.isNull(traffic)) {
            shop.setTraffic(traffic);
        }
        if (!Utils.isNull(card)) {
            shop.setCard(card);
        }
        if (!Utils.isNull(web)) {
            shop.setWeb(web);
        }
        if (!Utils.isNull(budget)) {
            shop.setBudget(budget);
        }
        if (!Utils.isNull(slat) && !Utils.isNull(slng)) {
            shop.setLnglat(slng + "," + slat);
        }
    }

    private void setPlaceInfo(ListEntry listEntry, Store shop, double lat,
            double lng) throws UnsupportedEncodingException {
        StringBuilder buf = new StringBuilder();
        buf.append("https://maps.googleapis.com/maps/api/place/search/json");
        buf.append("?location=").append(lat).append(",").append(lng);
        buf.append(URLEncoder.encode(
            "&language=ja&radius=10&types=bar|food&sensor=false",
            "UTF-8"));
        buf.append("&key=AIzaSyCaR3csaTYCWXJH6YvmhLf5y2NN9z4sNXE");
        String text = Utils.getHttpResponse(buf.toString());
        if (text != null && text.length() > 0 && text.indexOf("OK") != -1) {
            try {
                JSONObject json = new JSONObject(text);
                JSONArray results = json.getJSONArray("results");
                if (results != null && results.length() > 0) {
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject obj = results.getJSONObject(i);
                        String name = obj.getString("name");
                        String reference = obj.getString("reference");
                        System.out.println(name + " in " + shop.getTitle());

                        if (name.indexOf(shop.getTitle()) != -1
                            || shop.getTitle().indexOf(name) != -1) {

                            listEntry.getCustomElements().setValueLocal(
                                "reference",
                                reference);

                            buf = new StringBuilder();
                            buf
                                .append("https://maps.googleapis.com/maps/api/place/details/json");
                            buf
                                .append("?reference=")
                                .append(reference)
                                .append("&language=ja&sensor=false");
                            ;
                            buf
                                .append("&key=AIzaSyCaR3csaTYCWXJH6YvmhLf5y2NN9z4sNXE");

                            String detailText =
                                Utils.getHttpResponse(buf.toString());
                            JSONObject detail_json = new JSONObject(detailText);
                            JSONObject detail_result =
                                detail_json.getJSONObject("result");
                            if (detail_result != null) {
                                String website =
                                    detail_result.isNull("website")
                                        ? null
                                        : detail_result.getString("website");
                                if (website == null) {
                                    website =
                                        detail_result.isNull("url")
                                            ? null
                                            : detail_result.getString("url");
                                }
                                String phone =
                                    detail_result
                                        .isNull("formatted_phone_number")
                                        ? null
                                        : detail_result
                                            .getString("formatted_phone_number");
                                if (!Utils.isNull(website)) {
                                    listEntry
                                        .getCustomElements()
                                        .setValueLocal("WEB", website);
                                }
                                if (shop.getPhone() == null
                                    || shop.getPhone().trim().length() == 0) {
                                    listEntry
                                        .getCustomElements()
                                        .setValueLocal("電話番号", phone);
                                }
                            }
                            break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * プール中のビンゴの数を返します。
     * @param userId
     * @return
     */
    public int getPoolCount(String userId) {
        Key userKey = Datastore.createKey(PrimarilyUser.class, userId);
        PrimarilyUser priuser = getPrimarilyUser(userKey);
        if (priuser != null) {
            List<Integer> poolnumbers = priuser.getPoolnumbers();
            if (poolnumbers != null) {
                return poolnumbers.size();
            }
            return 0;
        } else {
            throw new RuntimeException("ユーザが存在しません。");
        }
    }

    /**
     * プール中のビンゴの数を返します。
     * @param userId
     * @return
     */
    public int getPoolCount() {
        int count = 0;
        
        PrimarilyUserMeta meta = PrimarilyUserMeta.get();
        ModelQuery<PrimarilyUser> query = Datastore.query(meta);
        List<PrimarilyUser> entities = query.asList();
        for (Iterator<PrimarilyUser> ite = entities.iterator(); ite.hasNext();) {
            PrimarilyUser user = ite.next();
            
            List<Integer> poolnumbers = user.getPoolnumbers();
            if (poolnumbers != null) {
                count += poolnumbers.size();
            }
        }
        return count;
    }
    
    /**
     * 一番最後に引いたビンゴの情報を返します。
     * @return
     */
    public LotHistory getLastLot() {
        LotHistoryMeta m = LotHistoryMeta.get();
        LotHistory lotHistory = Datastore.query(m)
                .sort(m.created.desc)
                .limit(1)
                .asSingle();
        return lotHistory;
    }

}
