package jp.yokmama.slim3.bingodetemp.controller.mnt;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import com.google.appengine.api.backends.BackendServiceFactory;
import com.google.appengine.api.taskqueue.*;
import com.google.appengine.api.taskqueue.TaskOptions.Builder;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

public class RunbackendController extends Controller {

    @Override
    public Navigation run() throws Exception {
        //String backend = request.getParameter("backend");

        Queue queue = QueueFactory.getDefaultQueue();
        queue.add(Builder.withUrl("/StoreImport").method(Method.GET).header("Host", BackendServiceFactory.getBackendService().getBackendAddress("small")));

        response.setContentType("text/plain");
        response.getWriter().println("called");
        
        return null;
    }
}
