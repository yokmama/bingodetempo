package jp.yokmama.slim3.bingodetemp.controller.mnt;

import java.util.List;

import jp.yokmama.slim3.bingodetemp.meta.StoreMeta;
import jp.yokmama.slim3.bingodetemp.model.Store;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;

public class CheckDatabaseController extends Controller {

    @Override
    public Navigation run() throws Exception {
        List<Key> keys = Datastore.query(StoreMeta.get()).asKeyList();

        List<Store> shops = Datastore.get(Store.class, keys);
        
        for(int i=0; i<shops.size(); i++){
            Store shop = shops.get(i);
            System.out.println(shop.getTitle()+":"+shop.getAddress()+":"+shop.getLnglat());
        }
        
        return null;
    }
}
