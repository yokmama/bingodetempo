package jp.yokmama.slim3.bingodetemp.controller;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;

public class RegisterController extends Controller {
    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        String userId = request.getParameter("userId");
        String accountName = request.getParameter("accountName");
        String regId = request.getParameter("regId");
        if(regId != null && userId != null){
            Key key = Datastore.createKey(PrimarilyUser.class, userId);
            
            PrimarilyUser user = service.getPrimarilyUser(key);
            if(user!=null){
                //update
                if(!regId.equals(user.getRegId())){
                    user.setRegId(regId);
                    user.setNickname(accountName);
                    Datastore.put(user);
                }
            }
            else{
                //create
                user = new PrimarilyUser();
                user.setKey(key);
                user.setNickname(accountName);
                user.setRegId(regId);
                Datastore.put(user);
            }
            
            /*
            Key regkey = Datastore.createKey(RegistrationId.class, regId);
            RegistrationId obj = null;
            try{
                obj = Datastore.get(RegistrationId.class, key);
            }
            catch(Exception e){
                System.out.println("ERROR e:" + e.toString());
            }
            if(obj == null){
                obj = new RegistrationId();
                user.setKey(regkey);
                Datastore.put(user);
            }*/
            
        }
        
        return null;
    }
}
