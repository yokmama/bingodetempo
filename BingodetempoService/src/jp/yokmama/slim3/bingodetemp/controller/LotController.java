package jp.yokmama.slim3.bingodetemp.controller;

import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

public class LotController extends Controller {

    @Override
    public Navigation run() throws Exception {
        request.setAttribute("serverName", getServerName());
        request.setAttribute("debug", Utils.DEBUG ? "true" : "false");
        return forward("Lot.jsp");
    }
    
    private String getServerName() {
        StringBuilder sb = new StringBuilder();
        sb.append("http://");
        sb.append(request.getServerName());
        int serverPort = request.getServerPort();
        if (serverPort != 80) {
            sb.append(":").append(serverPort);
        }
        sb.append("/");
        
        return sb.toString();
    }
}
