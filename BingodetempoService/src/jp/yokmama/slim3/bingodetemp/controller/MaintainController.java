package jp.yokmama.slim3.bingodetemp.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

public class MaintainController extends Controller {

    @Override
    public Navigation run() throws Exception {
        return forward("Maintain.jsp");
    }
}
