package jp.yokmama.slim3.bingodetemp.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import jp.yokmama.slim3.bingodetemp.meta.StoreMeta;
import jp.yokmama.slim3.bingodetemp.model.Participation;
import jp.yokmama.slim3.bingodetemp.model.Store;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Transaction;

public class StoreImportController extends Controller {
    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        service.clearStorelist();
        service.importStorelist();
        
        // ダミーデータとして、１０月５日のデータを作成する
        service.clearPaticipationlist();
        List<Entity> entityis = Datastore.query(StoreMeta.get()).asEntityList();
        if (entityis != null && entityis.size() > 0) {
            StoreMeta storeMt = StoreMeta.get();
            Calendar cal = Calendar.getInstance(Utils.jpnTz);
            cal.set(Calendar.MONTH, 9);
            cal.set(Calendar.DAY_OF_MONTH, 5);
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            Date date = cal.getTime();
            String section = "KYKK";
            String sectiondate = section + Utils.getfmtYYYMMDD().format(date);
            Date updateDate = Calendar.getInstance(Utils.jpnTz).getTime();
            for (Iterator<Entity> ite = entityis.iterator(); ite.hasNext();) {
                Store store = storeMt.entityToModel(ite.next());
                Transaction tx = Datastore.beginTransaction();
                Key key = Participation.createKey("KYKK", store.getTitle(), date);
                Participation participation = new Participation();
                participation.setKey(key);
                participation.setSection(section);
                participation.setSectiondate(sectiondate);
                participation.setDate(date);
                participation.setUpdateDate(updateDate);
                participation.setService(store.getBudget());
                participation.setDesc(store.getDesc());
                participation.getStoreRef().setModel(store);
                Datastore.put(participation);
                tx.commit();
            }
        }

        return null;
    }
}
