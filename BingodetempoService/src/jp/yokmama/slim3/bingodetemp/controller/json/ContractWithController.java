package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.HashMap;
import java.util.Map;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

public class ContractWithController extends Controller {
    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");

        String parentId = request.getParameter("parentId");
        String childId = request.getParameter("childId");
        if (!Utils.isNull(parentId) && !Utils.isNull(childId)) {

            PrimarilyUser parent =
                service.getPrimarilyUser(Datastore.createKey(
                    PrimarilyUser.class,
                    Long.parseLong(parentId)));
            PrimarilyUser child =
                service.getPrimarilyUser(Datastore.createKey(
                    PrimarilyUser.class,
                    Long.parseLong(childId)));

            if (parent != null && child != null) {
                if(child.getParentRef() == null){
                    child.getParentRef().setModel(parent);
                    Datastore.put(child);
    
                    Map<String, Object> resultmap = new HashMap<String, Object>();
                    resultmap.put("satus", "ok");
                    new JSONObject(resultmap).write(response.getWriter());
                }
                else{
                    Map<String, Object> resultmap = new HashMap<String, Object>();
                    resultmap.put("satus", "ng");
                    resultmap.put("code", 100); // すでに親がいる
                    new JSONObject(resultmap).write(response.getWriter());
                }
                return null;
            }
        }

        Map<String, Object> resultmap = new HashMap<String, Object>();
        resultmap.put("satus", "ng");
        resultmap.put("code", 0);
        new JSONObject(resultmap).write(response.getWriter());
        return null;
    }
}
