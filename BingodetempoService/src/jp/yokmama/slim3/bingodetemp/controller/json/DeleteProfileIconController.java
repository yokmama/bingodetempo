package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.logging.Logger;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.service.JsonResponseService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.Key;

public class DeleteProfileIconController extends Controller {
    private final static Logger LOG = Logger
            .getLogger(DeleteProfileIconController.class.getName());

    JsonResponseService jsonService = new JsonResponseService();
    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        String reqFromKey = param("fromKey");
        if (reqFromKey != null) {
            Key userKey = Datastore.createKey(PrimarilyUser.class, reqFromKey);
            PrimarilyUser fromUser = service.getPrimarilyUser(userKey);
            if (fromUser != null) {
                if (fromUser.getBlobKey() != null) {
                    String blobKeyName = fromUser.getBlobKey().getName();
                    BlobKey blobKey = new BlobKey(blobKeyName);
                    BlobstoreService bs = BlobstoreServiceFactory.getBlobstoreService();
                    bs.delete(blobKey);

                    fromUser.setBlobKey(null);
                    Datastore.put(fromUser);
                } else {
                    // アイコン未登録
                    LOG.info("This user's icon is not set yet.");
                }

                return jsonService.returnOk(response, true);
            } else {
                return jsonService.returnNg(response, "User not found.");
            }
        } else {
            return jsonService.returnNg(response, "fromKey is empty.");
        }
    }
}
