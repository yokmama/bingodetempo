package jp.yokmama.slim3.bingodetemp.controller.json;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import jp.yokmama.slim3.bingodetemp.meta.StoreMeta;
import jp.yokmama.slim3.bingodetemp.model.Participation;
import jp.yokmama.slim3.bingodetemp.model.Store;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;

public class GetStoreController extends Controller {
    private final static Logger LOG = Logger.getLogger(GetStoreController.class
        .getName());

    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        Map<String, Object> resultmap = new HashMap<String, Object>();

        String storeId = request.getParameter("storeId");
        if (!Utils.isNull(storeId)) {
            Key key = Datastore.createKey(Store.class, Long.parseLong(storeId));
            Store store = service.getStore(key);
            if (store != null) {
                resultmap.put("result", modelToJson(store));
            }
        } else {
            StoreMeta storeMt = StoreMeta.get();
            List<Key> keys = Datastore.query(storeMt).asKeyList();

            List<Store> stores = Datastore.get(Store.class, keys);
            if (stores != null && stores.size() > 0) {
                List<JSONObject> results = new ArrayList<JSONObject>();
                for (int i = 0; i < stores.size(); i++) {
                    Store store = stores.get(i);
                    results.add(modelToJson(store));
                }
                resultmap.put("results", results);
            }
        }
        resultmap.put("status", "ok");

        JSONObject result = new JSONObject(resultmap);

        result.write(response.getWriter());
        response.flushBuffer();

        return null;
    }

    private JSONObject modelToJson(Store store)
            throws UnsupportedEncodingException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("key", store.getKey().getId());
        map.put("title", Utils.encodUTF8(store.getTitle()));
        map.put("type", Utils.encodUTF8(store.getType()));
        map.put("desc", Utils.encodUTF8(store.getDesc()));
        map.put("phone", Utils.encodUTF8(store.getPhone()));
        map.put("opening", Utils.encodUTF8(store.getOpening()));
        map.put("address", Utils.encodUTF8(store.getAddress()));
        map.put("holiday", buildHoliday(store.getHoliday()));

        map.put("holiday_note", Utils.encodUTF8(store.getHolidayNote()));
        map.put("part", getParticipationList(store));
        map.put("traffic", Utils.encodUTF8(store.getTraffic()));
        map.put("card", Utils.encodUTF8(store.getCard()));
        map.put("budget", Utils.encodUTF8(store.getBudget()));
        map.put("web", Utils.encodUTF8(store.getWeb()));
        map.put("yomigana", Utils.encodUTF8(store.getYomigana()));
        // TODO 画像のURL
        map.put("url", Utils.encodUTF8(store.getWeb()));
        map.put("lnglat", store.getLnglat());

        return new JSONObject(map);
    }

    /**
     * カンマ区切りで格納されている曜日を分解し、配列にします。
     * @param holidays
     * @return
     */
    private int[] buildHoliday(String holidays) {
        if (holidays != null) {
            try {
                String[] split = holidays.split(",");
                int[] holidayAry = new int[split.length];
                for (int i = 0; i < split.length; i++) {
                    holidayAry[i] = Integer.parseInt(split[i]);
                }
                
                return holidayAry;
            } catch (NumberFormatException e) {
                LOG.severe("Illegal holiday. holidays=" + holidays);
            }
        }
        return new int[0];
    }

    /**
     * 参加日リストを表示します。
     * 
     * @param store
     * @return
     */
    private String[] getParticipationList(Store store) {
        List<Participation> modelList =
            store.getParticipationsRefs().getModelList();
        int size = modelList.size();
        String[] strPartList = new String[size];
        int i = 0;
        for (Iterator<Participation> ite = modelList.iterator(); ite.hasNext();) {
            Participation participation = ite.next();

            Date date = participation.getDate();
            String format = Utils.getfmtYYYMMDD().format(date);
            try {
                strPartList[i++] = Utils.encodUTF8(format);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        return strPartList;
    }
}
