package jp.yokmama.slim3.bingodetemp.controller.json;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.service.JsonResponseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.Key;

public class GetProfileController extends Controller {
    private final static Logger LOG = Logger
        .getLogger(GetProfileController.class.getName());

    JsonResponseService jsonService = new JsonResponseService();
    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        String fromKey = param("fromKey");
        LOG.info("fromKey=" + fromKey);
        if (Utils.isNull(fromKey)) {
            return jsonService.returnNg(response, "ユーザが存在しません。");
        }
        Key userKey = Datastore.createKey(PrimarilyUser.class, fromKey);
        PrimarilyUser user = service.getPrimarilyUser(userKey);
        if (user == null) {
            // ユーザがまだ作成されていない場合は作成する
            user = PrimarilyUser.createUser(fromKey);
        }
        return jsonService.returnOk(response, modelToJson(user));
    }

    private JSONObject modelToJson(PrimarilyUser user)
            throws UnsupportedEncodingException {
        String blobKey = null;
        if (user.getBlobKey() != null) {
            blobKey = user.getBlobKey().getName();
        }
        
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("nickname", user.getNickname());
        map.put("sex", user.getSex());
        map.put("blobKey", blobKey);
        map.put("bloodType", user.getBloodType());
        map.put("showRange", user.getShowRange());
        map.put("appeal", user.getAppeal());

        BlobstoreService bs = BlobstoreServiceFactory.getBlobstoreService();
        String uploadUrl = bs.createUploadUrl("/json/SetProfileIcon");
        map.put("regIconUrl", uploadUrl);

        return new JSONObject(map);
    }
}
