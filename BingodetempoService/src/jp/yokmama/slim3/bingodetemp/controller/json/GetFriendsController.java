package jp.yokmama.slim3.bingodetemp.controller.json;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;

public class GetFriendsController extends Controller {
    public static int REL_PARENT = 0;
    public static int REL_CHILED = 1;
    public static int REL_FRIEND = 2;
    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        Map<String, Object> resultmap = new HashMap<String, Object>();

        String userId = request.getParameter("userId");
        if (!Utils.isNull(userId)) {
            Key key = Datastore.createKey(PrimarilyUser.class, Long.parseLong(userId));
            PrimarilyUser me = service.getPrimarilyUser(key);
            if(me!=null){
                List<JSONObject> results = new ArrayList<JSONObject>();
                //親
                results.add(modelToJson(me.getParentRef().getModel(), REL_PARENT));
                
                //知人
                Set<Key> friends = me.getFriends();
                if (friends != null && friends.size() > 0) {
                    for(Iterator<Key> ite = friends.iterator(); ite.hasNext(); ){
                        Key userKey = ite.next();
                        PrimarilyUser friend = service.getPrimarilyUser(userKey);
                        if(friend.getParentRef().getKey().equals(key)){
                            results.add(modelToJson(friend, REL_CHILED));
                        }
                        else{
                            results.add(modelToJson(friend, REL_FRIEND));
                        }
                    }
                }
                
                resultmap.put("results", results);
            }
        }
        resultmap.put("status", "ok");

        JSONObject result = new JSONObject(resultmap);

        result.write(response.getWriter());
        response.flushBuffer();

        return null;
    }
    
    private JSONObject modelToJson(PrimarilyUser user, int rel)
            throws UnsupportedEncodingException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("key", user.getKey().getId());
        map.put("rel", rel);
        map.put("nickname", Utils.encodUTF8(user.getNickname()));
        if(user.getSecondaryRef()!=null){
            map.put("number", user.getSecondaryRef().getModel().getNumber());
            map.put("message", Utils.encodUTF8(user.getSecondaryRef().getModel().getMessage()));
        }
        map.put("iconKey", user.getBlobKey().getId());

        return new JSONObject(map);
    }
}
