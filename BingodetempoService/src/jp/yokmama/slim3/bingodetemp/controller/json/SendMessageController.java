package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.logging.Logger;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.model.SecondaryUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.service.GCMService;
import jp.yokmama.slim3.bingodetemp.utils.ApiKeyInitializer;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;

public class SendMessageController extends Controller {
    private final Logger logger = Logger.getLogger(getClass().getName());

    private DatabaseService service = new DatabaseService();
    private GCMService gcmservice = new GCMService();
    private Sender sender;

    private Sender getSender() {
        if (sender == null) {
            String key = (String) servletContext
                .getAttribute(ApiKeyInitializer.ATTRIBUTE_ACCESS_KEY);
            return new Sender(key);
        }
        return sender;
    }

    @Override
    public Navigation run() throws Exception {

        String toKey = request.getParameter("toKey");
        String fromKey = request.getParameter("fromKey");
        String text = request.getParameter("message");

        SecondaryUser fromUser = service.getSecondaryUser(Datastore.createKey(SecondaryUser.class, fromKey));
        if (fromUser != null) {
            SecondaryUser toUser = service.getSecondaryUser(Datastore.createKey(SecondaryUser.class, toKey));
            if (toUser != null) {
                Builder builder = new Message.Builder();
                builder.addData("message", text);
                builder.addData("id", fromUser.getKey().getName());
                builder.addData("number", Long.toString(fromUser.getNumber()));
                //本来はここで、公開設定にあわせて数字だけを返すか、ニックネームも返すか判断する必要がある
                builder.addData("nickname", fromUser.getPrimarilyRef().getModel().getNickname());
                
                gcmservice.sendSingleMessage(
                    getSender(),
                    toUser.getPrimarilyRef().getModel().getRegId(),
                    builder.build(),
                    response);
                return null;
            }
        }

        logger.severe("Invalid request!");
        gcmservice.taskDone(response);

        return null;
    }

}
