package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.yokmama.slim3.bingodetemp.model.Participation;
import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.model.SecondaryUser;
import jp.yokmama.slim3.bingodetemp.model.Store;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;

public class GetUserlistController extends Controller {
    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        String storeKey = request.getParameter("storeKey");
        if(!Utils.isNull(storeKey)){
            long id = Utils.valueLong(storeKey, -1);
            if(id!=-1){
                Key key = Datastore.createKey(Participation.class, id);
                Participation participation = service.getParticipation(key);
                if(participation!=null){
                    Map<String, Object> resultmap = new HashMap<String, Object>();
                    
                    Set<Key> secusers = participation.getUsers();
                    List<JSONObject> results = new ArrayList<JSONObject>();
                    for(Iterator<Key> ite = secusers.iterator(); ite.hasNext(); ){
                        Key secuserKey = ite.next();
                        SecondaryUser user = service.getSecondaryUser(secuserKey);
                        if(user!=null){
                            Map<String, Object> map = new HashMap<String, Object>();
                            
                            map.put("id", secuserKey.getName());
                            map.put("number", user.getNumber());
                            map.put("message", Utils.encodUTF8(user.getMessage()));
                            //本来はここで、公開設定にあわせて数字だけを返すか、ニックネームも返すか判断する必要がある
                            map.put("showRange", user.getPrimarilyRef().getModel().getShowRange());
                            map.put("nickname", Utils.encodUTF8(user.getPrimarilyRef().getModel().getNickname()));
                            
                            JSONObject jsonobj = new JSONObject(map);
                            results.add(jsonobj);
                        }
                    }
                    
                    resultmap.put("satus", "ok");
                    resultmap.put("results", results);
                    
                    new JSONObject(resultmap).write(response.getWriter());
                }
            }
            
        }
        return null;
    }
}
