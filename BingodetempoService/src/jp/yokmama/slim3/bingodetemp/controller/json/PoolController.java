package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;

public class PoolController extends Controller {
    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        String nfcId = request.getParameter("nfcId");
        String reqUserId = request.getParameter("userId");
        String poolNum = request.getParameter("poolNum");
        if(!Utils.isNull(nfcId) && !Utils.isNull(reqUserId) && !Utils.isNull(poolNum)){
            int number = Integer.parseInt(poolNum);
            String userId = reqUserId;
            
            Key userKey = Datastore.createKey(PrimarilyUser.class, userId);
            PrimarilyUser priuser = service.getPrimarilyUser(userKey);
            if(priuser!=null){
                List<Integer> list = priuser.getPoolnumbers();
                if(list == null){
                    list = new ArrayList<Integer>();
                }
                list.add(number);
                priuser.setPoolnumbers(list);
                Datastore.put(priuser);
                
                Map<String, Object> resultmap = new HashMap<String, Object>();
                resultmap.put("satus", "ok");
                resultmap.put("code", 0);
                new JSONObject(resultmap).write(response.getWriter());
                return null;
            }
            else{
                Map<String, Object> resultmap = new HashMap<String, Object>();
                resultmap.put("satus", "ng");
                resultmap.put("result", "no such user.");
                new JSONObject(resultmap).write(response.getWriter());
                return null;
            }
        }

        response.setStatus(200);
        return null;
    }
}
