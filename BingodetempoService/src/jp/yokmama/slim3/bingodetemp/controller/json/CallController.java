package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.ArrayList;
import java.util.List;

import jp.yokmama.slim3.bingodetemp.meta.PrimarilyUserMeta;
import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.service.GCMService;
import jp.yokmama.slim3.bingodetemp.utils.ApiKeyInitializer;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;
import com.google.appengine.api.datastore.Key;

public class CallController extends Controller {
    DatabaseService service = new DatabaseService();
    
    private GCMService gcmservice = new GCMService();
    private Sender sender;

    private Sender getSender() {
        if (sender == null) {
            String key = (String) servletContext
                .getAttribute(ApiKeyInitializer.ATTRIBUTE_ACCESS_KEY);
            return new Sender(key);
        }
        return sender;
    }
    

    @Override
    public Navigation run() throws Exception {
        String userId = request.getParameter("userId");
        String status = request.getParameter("status");
        if(!Utils.isNull(userId) && !Utils.isNull(status)){
            Key userKey = Datastore.createKey(PrimarilyUser.class, userId);
            PrimarilyUser priuser = service.getPrimarilyUser(userKey);
            if(priuser!=null){
                
                Builder builder = new Message.Builder();
                builder.addData("action", "call");
                builder.addData("status", status);
                builder.addData("id", priuser.getKey().getName());
                builder.addData("nickname", priuser.getNickname());

                List<String> ids = new ArrayList<String>();
                List<Key> keys = Datastore.query(PrimarilyUserMeta.get()).asKeyList();
                if (keys != null && keys.size() > 0) {
                    for(Key key : keys){
                        PrimarilyUser user = service.getPrimarilyUser(key);
                        ids.add(user.getRegId());
                        //gcmservice.sendSingleMessage(getSender(), user.getRegId(), builder.build(), response);
                    }
                }
                gcmservice.sendMulticastMessage(getSender(), service.createMulticast(ids), builder.build(), response);
                return null;
            }
        }
        
        return null;
    }
}
