package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

public class GetProfileIconController extends Controller {
    private final static Logger LOG = Logger
            .getLogger(GetProfileIconController.class.getName());

    @Override
    public Navigation run() throws Exception {
        response.setContentType("image/jpeg");
        String reqBlobKey = param("blobKey");
        if (reqBlobKey != null) {
            BlobKey blobKey = new BlobKey(reqBlobKey);
            BlobstoreService bs = BlobstoreServiceFactory.getBlobstoreService();
            bs.serve(blobKey, response);
        } else {
            LOG.severe("blobKey is empty.");
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return null;
    }
}
