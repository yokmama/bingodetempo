package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import jp.yokmama.slim3.bingodetemp.model.Counter;
import jp.yokmama.slim3.bingodetemp.model.Participation;
import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.model.SecondaryUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.ModelRef;

import com.google.appengine.api.datastore.Key;

public class CheckInController extends Controller {
    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        String reqStoreId = request.getParameter("storeId");
        String reqUserId = request.getParameter("userId");
        if(!Utils.isNull(reqStoreId) && !Utils.isNull(reqUserId)){
            long storeId = Long.parseLong(reqStoreId);
            int code = -1;
            String userId = reqUserId;
            
            Key storeKey = Datastore.createKey(Participation.class, storeId);
            Key userKey = Datastore.createKey(PrimarilyUser.class, userId);
            Participation participation = service.getParticipation(storeKey);
            Date updateDate = Calendar.getInstance(Utils.jpnTz).getTime();
            if(participation!=null){
                PrimarilyUser priuser = service.getPrimarilyUser(userKey);
                if(priuser!=null){
                    SecondaryUser secuser = priuser.getSecondaryRef().getModel();
                    boolean isCheckdin = false;
                    if(secuser!=null){
                        //チェックイン済み
                        Participation beforStore = secuser.getStoreRef().getModel();
                        if(beforStore!=null){
                            if(beforStore.getKey().equals(storeKey)){
                                //すでにチェックイン済
                                if(priuser.getRegId()==null){
                                    code = 1;
                                }
                                else{
                                    code = 0;
                                }
                                isCheckdin = true;
                            }
                            else{
                                //remove link
                                beforStore.getUsers().remove(userKey);
                                beforStore.setUpdateDate(updateDate);
                                Datastore.put(beforStore);
                                
                                secuser.getStoreRef().setModel(participation);
                                Datastore.put(secuser);
                                code = 2;
                            }
                        }else{
                            secuser.getStoreRef().setModel(participation);
                            Datastore.put(secuser);
                            code = 2;
                        }
                    }else{
                        //Create Secondary Account
                        long number = Counter.increment(Utils.getSwichingDate(Calendar.getInstance(Utils.jpnTz)));
                        Key key = Datastore.createKey(SecondaryUser.class, userId+number);
                        secuser = new SecondaryUser();
                        secuser.setKey(key);
                        secuser.setNumber(number);
                        secuser.getPrimarilyRef().setModel(priuser);
                        secuser.getStoreRef().setModel(participation);
                        Datastore.put(secuser);
                        code = 1;
                    }
                    if(!isCheckdin){
                        //new link
                        participation.getUsers().add(secuser.getKey());
                        participation.setUpdateDate(updateDate);
                        Datastore.put(participation);
                    }
                }
                else{
                    //create
                    priuser = new PrimarilyUser();
                    priuser.setKey(userKey);
                    Datastore.put(priuser);
                    
                    long number = Counter.increment(Utils.getSwichingDate(Calendar.getInstance(Utils.jpnTz)));
                    Key key = Datastore.createKey(SecondaryUser.class, userId+number);
                    SecondaryUser secuser = new SecondaryUser();
                    secuser.setKey(key);
                    secuser.setNumber(number);
                    secuser.getPrimarilyRef().setModel(priuser);
                    secuser.getStoreRef().setModel(participation);
                    Datastore.put(secuser);
                    
                    participation.getUsers().add(key);
                    participation.setUpdateDate(updateDate);
                    Datastore.put(participation);
                    code = 1;
                }
                
                Map<String, Object> resultmap = new HashMap<String, Object>();
                resultmap.put("satus", "ok");
                resultmap.put("code", code);
                resultmap.put("result", participation.getJSONObject());
                new JSONObject(resultmap).write(response.getWriter());
                return null;
            }
            Map<String, Object> resultmap = new HashMap<String, Object>();
            resultmap.put("satus", "ng");
            resultmap.put("result", "no such find.");
            new JSONObject(resultmap).write(response.getWriter());
            return null;
        }

        response.setStatus(200);
        return null;
    }

    /*private void writeCode(String status, int code) throws JSONException, IOException{
        System.out.println("status code = "+ code);
        Map<String, Object> resultmap = new HashMap<String, Object>();
        
        resultmap.put("satus", status);
        if("OK".equals(status)){
            JSONObject result = new JSONObject();
            result.put("code", code);
            resultmap.put("result", result);
        }
        
        new JSONObject(resultmap).write(response.getWriter());
        response.flushBuffer();
    }*/
    
    
}
