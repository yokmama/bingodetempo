package jp.yokmama.slim3.bingodetemp.controller.json;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.yokmama.slim3.bingodetemp.meta.ParticipationMeta;
import jp.yokmama.slim3.bingodetemp.model.Participation;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;

public class GetParticipationController extends Controller {

    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        String storeId = request.getParameter("storeId");
        if(!Utils.isNull(storeId)){
            Key storeKey = Datastore.createKey(Participation.class, storeId);
            Participation participation = service.getParticipation(storeKey);
            if(participation!=null){
                Map<String, Object> resultmap = new HashMap<String, Object>();
                resultmap.put("satus", "ok");
                resultmap.put("result", participation.getJSONObject());
                new JSONObject(resultmap).write(response.getWriter());
            }
        }
        else{
            String section = request.getParameter("section");
            String selectDate = request.getParameter("date");
            if (!Utils.isNull(section)) {
                Date date = Calendar.getInstance(Utils.jpnTz).getTime();
                if (!Utils.isNull(selectDate)) {
                    SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
                    fmt.setTimeZone(Utils.jpnTz);
                    date = fmt.parse(selectDate);
                }
    
                String sectiondate = section + Utils.getfmtYYYMMDD().format(date);
                Map<String, Object> resultmap = new HashMap<String, Object>();
    
                ParticipationMeta participationMt = ParticipationMeta.get();
                List<Key> keys =
                    Datastore
                        .query(participationMt)
                        .filter(participationMt.sectiondate.equal(sectiondate))
                        .asKeyList();
    
                List<Participation> participations =
                    Datastore.get(Participation.class, keys);
                List<JSONObject> results = new ArrayList<JSONObject>();
                Participation participation;
                for (Iterator<Participation> ite = participations.iterator(); ite
                    .hasNext();) {
                    participation = ite.next();
                    results.add(participation.getJSONObject());
                }
    
                resultmap.put("satus", "ok");
                resultmap.put("results", results);
    
                new JSONObject(resultmap).write(response.getWriter());
            }
        }

        return null;
    }
}
