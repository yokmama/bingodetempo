package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.HashMap;
import java.util.Map;

import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.service.JsonResponseService;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

public class PoolCountController extends Controller {
    DatabaseService service = new DatabaseService();
    JsonResponseService json = new JsonResponseService();

    @Override
    public Navigation run() throws Exception {
        json.settings(response);
        
        // String nfcId = request.getParameter("nfcId");
        String reqUserId = request.getParameter("userId");

        final String userId = reqUserId;
        String errorMessage = null;
        int poolCount = 0;
        try {
            if (userId != null && userId.length() > 0) {
                poolCount = service.getPoolCount(userId);
            } else {
                poolCount = service.getPoolCount();
            }
        } catch (RuntimeException e) {
            errorMessage = e.getMessage();
            if (errorMessage == null) {
                errorMessage = e.getClass().getSimpleName();
            }
        }
        
        response.setStatus(200);

        Map<String, Object> resultmap = new HashMap<String, Object>();
        final String status;
        final int code;
        if (errorMessage == null) {
            status = "ok";
            code = 0;
        } else {
            status = "ng";
            code = 0;
            resultmap.put("message", errorMessage);
        }
        resultmap.put("satus", status);
        resultmap.put("code", code);
        resultmap.put("count", poolCount);
        new JSONObject(resultmap).write(response.getWriter());
        return null;
    }
}
