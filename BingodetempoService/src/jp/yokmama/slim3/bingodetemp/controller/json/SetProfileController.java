package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.service.JsonResponseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Transaction;

public class SetProfileController extends Controller {
    private final static Logger LOG = Logger
        .getLogger(SetProfileController.class.getName());

    private JsonResponseService jsonService = new JsonResponseService();
    private DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrimarilyUser collectUser = null;
        try {
            collectUser = collectUser(request);
        } catch (NumberFormatException e) {
            LOG.warning("入力値チェックエラー。 message=" + e.getMessage());
            return jsonService.returnNg(response, e.getMessage());
        } catch (RuntimeException e) {
            LOG.warning("入力値チェックエラー。 message=" + e.getMessage());
            return jsonService.returnNg(response, e.getMessage());
        }

        Transaction beginTransaction = Datastore.beginTransaction();
        Datastore.put(collectUser);
        beginTransaction.commit();

        return jsonService.returnOk(response, true);
    }

    /**
     * ユーザ情報をクライアントのリクエストから集めます。
     * 
     * @param request
     * @return
     * @throws RuntimeException
     *             入力値のエラー
     */
    private PrimarilyUser collectUser(HttpServletRequest request)
            throws RuntimeException, NumberFormatException {
        String reqKey = param("fromKey");
        Key userKey = Datastore.createKey(PrimarilyUser.class, reqKey);
        PrimarilyUser user = service.getPrimarilyUser(userKey);
        if (user == null) {
            user = new PrimarilyUser();
            user.setKey(userKey);
        }
        user.setNickname(param("nickname"));
        user.setSex(Integer.parseInt(param("sex")));
        user.setBloodType(Integer.parseInt(param("bloodType")));
        user.setShowRange(Integer.parseInt(param("showRange")));
        user.setAppeal(param("appeal"));

        // TODO 入力値チェック
        if (Utils.isNull(user.getNickname())) {
            throw new RuntimeException("ニックネームが未記入です。");
        }

        return user;
    }
}
