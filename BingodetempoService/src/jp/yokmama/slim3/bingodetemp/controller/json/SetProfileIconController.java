package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.List;
import java.util.Map;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.service.JsonResponseService;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.Key;

public class SetProfileIconController extends Controller {

    JsonResponseService jsonService = new JsonResponseService();
    DatabaseService service = new DatabaseService();

    @Override
    public Navigation run() throws Exception {
        String fromKey = request.getParameter("fromKey");
        if (fromKey == null) {
            return jsonService.returnNg(response, "fromKey is empty.");
        }
        
        BlobstoreService bs = BlobstoreServiceFactory.getBlobstoreService();
        Map<String, List<BlobKey>> uploads = bs.getUploads(request);
        List<BlobKey> blobKeyList = uploads.get("icon");
        if (blobKeyList != null && blobKeyList.size() > 0) {
            String blobKeyString = blobKeyList.get(0).getKeyString();
            Key key = Datastore.createKey(PrimarilyUser.class, blobKeyString);

            // 関連するユーザを登録する
            Key userKey = Datastore.createKey(PrimarilyUser.class, fromKey);
            PrimarilyUser fromUser = service.getPrimarilyUser(userKey);
            fromUser.setBlobKey(key);
            Datastore.put(fromUser);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("blobKey", blobKeyString);
            return jsonService.returnOk(response, jsonObject);
        }
        
        // TODO その他のエラー
        return jsonService.returnNg(response, false);
    }
}
