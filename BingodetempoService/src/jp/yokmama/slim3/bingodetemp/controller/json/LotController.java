package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import jp.yokmama.slim3.bingodetemp.model.LotHistory;
import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.service.GCMService;
import jp.yokmama.slim3.bingodetemp.service.JsonResponseService;
import jp.yokmama.slim3.bingodetemp.utils.ApiKeyInitializer;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;
import org.slim3.memcache.Memcache;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;

public class LotController extends Controller {
    private static final String KEY_LAST_LOT_HISTORY = "last_lot_history";
    /** 次に引くまでの差分時間(ミリ秒) */
    private static final int TIME_NEXT_LOT;
    static {
        if (Utils.DEBUG) {
            // デバッグ時
            TIME_NEXT_LOT = 10 * 1000; // 10秒
        } else {
            TIME_NEXT_LOT = 15 * 60 * 1000; // 15分
        }
    }
    
    DatabaseService service = new DatabaseService();
    private GCMService gcmservice = new GCMService();
    private Sender sender;
    private Random rand = new Random(System.currentTimeMillis());
    private JsonResponseService json = new JsonResponseService();

    private Sender getSender() {
        if (sender == null) {
            String key =
                (String) servletContext
                    .getAttribute(ApiKeyInitializer.ATTRIBUTE_ACCESS_KEY);
            return new Sender(key);
        }
        return sender;
    }
    
    /**
     * 最後に回した時の玉データを取得する
     * @return
     */
    private LotHistory getLastLot() {
        final LotHistory lastLot;
        LotHistory lastLotCache = Memcache.get(KEY_LAST_LOT_HISTORY);
        if (lastLotCache != null) {
            // キャッシュがある場合はそれを返す。
            lastLot = lastLotCache;
        } else {
            lastLot = service.getLastLot();
        }
        
        return lastLot;
    }

    @Override
    public Navigation run() throws Exception {
        json.settings(response);
        
        // ユーザ一覧の取得
        List<PrimarilyUser> users = service.getPrimarilyUsers();
        
        final long nowMillis = System.currentTimeMillis();
        final long nextMillis;
        LotHistory lastLot = getLastLot();
        
        final long nextMillisTmp;
        if (lastLot != null) {
            nextMillisTmp = lastLot.getCreated().getTime() + TIME_NEXT_LOT;
        } else {
            // 初めて引いた時（履歴が無い場合）
            nextMillisTmp = nowMillis + TIME_NEXT_LOT;
        }

        final List<Integer> numbers = new ArrayList<Integer>();
        // 引く時間が来ているかどうかチェック
        if (nowMillis >= nextMillisTmp) {
            nextMillis = nextMillisTmp;
            
            // 時間が来たので引く
            List<String> ids = new ArrayList<String>();
            for (Iterator<PrimarilyUser> ite = users.iterator(); ite.hasNext();) {
                PrimarilyUser user = ite.next();

                List<Integer> list = user.getPoolnumbers();
                if(list != null && list.size()>0){
                    ids.add(user.getRegId());
                    int num = list.remove(0);
                    user.setPoolnumbers(list);
                    Datastore.put(user);
                    numbers.add(num);
                }
            }
            
            final int num;
            if (numbers.size() > 0) {
                int n = rand.nextInt(numbers.size());
                num = numbers.get(n);
                
                // 引いた玉と時間を保存
                lastLot = new LotHistory();
                lastLot.setNum(num);
                lastLot.setCreated(new Date(System.currentTimeMillis()));
                Datastore.put(lastLot);

                Builder builder = new Message.Builder();
                builder.addData("action", "lot");
                builder.addData("number", Integer.toString(num));
                
                gcmservice.sendMulticastMessage(getSender(), DatabaseService.createMulticast(ids), builder.build(), response);
            } else {
                // プールしている番号がない時
               if (lastLot != null) {
                   lastLot.setCreated(new Date(System.currentTimeMillis()));
               }
            }
        } else {
            // まだ引く時間ではない時
            nextMillis = nextMillisTmp;
            
            // プールされている番号の取得
            for (Iterator<PrimarilyUser> ite = users.iterator(); ite.hasNext();) {
                PrimarilyUser user = ite.next();

                List<Integer> list = user.getPoolnumbers();
                if(list != null && list.size()>0){
                    int num = list.remove(0);
                    numbers.add(num);
                }
            }
        }
        
        if (lastLot == null) {
            lastLot = new LotHistory();
            lastLot.setCreated(new Date(System.currentTimeMillis()));
            lastLot.setNum(-1);
        }
        Memcache.put(KEY_LAST_LOT_HISTORY, lastLot);
        
        final String status = "ok";
        final int code = 0;
        response.setStatus(200);

        Map<String, Object> resultmap = new HashMap<String, Object>();
        resultmap.put("satus", status); // "ok" or "ng"
        resultmap.put("code", code); // 0
        resultmap.put("num", lastLot.getNum()); // 前回引いた時の玉の番号。前回引いた玉がなければ-1になります。
        resultmap.put("now", nowMillis); // 現在時刻
        resultmap.put("next", nextMillis); // 次に回す時刻
        resultmap.put("numbers", numbers); // 現在プールされている番号
        new JSONObject(resultmap).write(response.getWriter());

        return null;
    }
}
