package jp.yokmama.slim3.bingodetemp.controller.json;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import jp.yokmama.slim3.bingodetemp.model.Participation;
import jp.yokmama.slim3.bingodetemp.model.Store;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.service.JsonResponseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Transaction;

public class SetStoreController extends Controller {
    private final static Logger LOG = Logger.getLogger(SetStoreController.class
        .getName());

    DatabaseService service = new DatabaseService();
    JsonResponseService jsonService = new JsonResponseService();

    @Override
    protected Navigation run() throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        if (!isPost()) {
            // POST以外はエラー
            return jsonService.returnNg(response, "Not Found");
        }

        // 入力値エラーチェック
        Store store = null;
        String[] parts = paramValues("part[]"); // 参加日
        try {
            store = collectShop(request);
        } catch (RuntimeException e) {
            // 入力値エラーを返す
            LOG.info("Validation Error: message=" + e.getMessage());
            response.getWriter().append(e.getMessage());
            response.flushBuffer();
            return null;
        }

        // 店舗情報をDBに格納する
        String reqStoreId = request.getParameter("storeId");
        LOG.info("reqStoreId=" + reqStoreId);
        Transaction tx = null;
        Key key;
        try {
            tx = Datastore.beginTransaction();
            if (reqStoreId != null) {
                // 登録情報変更の場合
                long storeId = Long.parseLong(reqStoreId);
                key = Datastore.createKey(Store.class, storeId);
                // Store existingStore = service.getStore(key);
                store.setKey(key);

                Datastore.put(store);

                LOG.info("storeId=" + storeId + ", store=" + store.getTitle());
            } else {
                // 新規登録の場合
                key = Store.createKey(store.getTitle(), store.getAddress());
                store.setKey(key);
                Datastore.put(store);
            }
            tx.commit();
        } catch (Exception e) {
            LOG.severe(e.getMessage());
            if (tx != null) {
                tx.rollback();
            }
            return jsonService.returnNg(response, "登録時にエラーが発生しました。再度お試しください。");
        }

        updateParticipation(store, parts);

        // 結果を返す
        Map<String, Object> resultmap = new HashMap<String, Object>();
        resultmap.put("status", "ok");
        resultmap.put("results", true);
        new JSONObject(resultmap).write(response.getWriter());

        return null;
    }

    /**
     * 参加日を編集します。
     * 
     * @param store
     *            対象の店舗
     * @param reqParts
     *            参加日リスト
     * 
     * @return
     */
    private void updateParticipation(Store store, String[] reqParts) {
        if (reqParts == null || reqParts.length <= 0) {
            LOG.warning("parts is null.");
            return;
        }

        // FIXME 現在は全て削除してから追加するようにしている。
        // これだとDatastoreに対するR/Wがとても増えてしまうので、
        // 差分をとって、変更のあったものだけ追加・削除するようにする。

        // 参加日データを一旦全て削除する
//        Transaction tx = Datastore.beginTransaction();
        List<Participation> modelList =
            store.getParticipationsRefs().getModelList();
        for (Iterator<Participation> ite = modelList.iterator(); ite.hasNext();) {
            Participation participation = ite.next();
            Datastore.delete(participation.getKey());
        }

        for (String reqPart : reqParts) {
            Date parsedDate = null;
            try {
                parsedDate = Utils.parseDate(reqPart);
            } catch (ParseException e) {
                // パースに失敗したら無視して次を処理する
                LOG.severe("parse failed. dateStr=" + reqParts);
                e.printStackTrace();
                continue;
            }

            // データを追加
            String section = "KYKK";
            String sectiondate =
                section + Utils.getfmtYYYMMDD().format(parsedDate);
            Date updateDate = Calendar.getInstance(Utils.jpnTz).getTime();

            // TODO 与える日付は、ユーザに与えられた日付で良いかどうか
            Key key =
                Participation.createKey("KYKK", store.getTitle(), parsedDate);

            Participation participation = new Participation();
            participation.setKey(key);
            participation.setSection(section);
            participation.setSectiondate(sectiondate);
            participation.setDate(parsedDate);
            participation.setUpdateDate(updateDate);
            participation.setService(store.getBudget());
            participation.setDesc(store.getDesc());
            participation.getStoreRef().setModel(store);
            Datastore.put(participation);
        }
    }

    /**
     * 店舗情報をクライアントのリクエストから集めます。
     * 
     * @param request
     * @return
     * @throws RuntimeException
     *             入力値のエラー
     */
    private Store collectShop(HttpServletRequest request)
            throws RuntimeException {
        Store store = new Store();
        store.setAddress(param("address"));
        store.setTitle(param("title"));
        store.setType(param("type"));
        store.setBlobKey(param("key")); // TODO ブロブキー
        store.setBudget(param("budget"));
        store.setCard(param("card"));
        store.setDesc(param("desc"));
        String[] holidays = paramValues("holiday[]");
        store.setHoliday(buildHolidays(holidays));
        store.setHolidayNote(param("holiday_note"));
        store.setLnglat(param("lnglat"));
        store.setOpening(param("opening"));
        store.setPhone(param("phone"));
        store.setTraffic(param("traffic"));
        store.setWeb(param("web"));
        store.setYomigana(param("yomigana"));

        // TODO 入力値チェック
        if (Utils.isNull(store.getTitle())) {
            throw new RuntimeException("タイトルが未入力です。");
        }
        if (Utils.isNull(store.getAddress())) {
            throw new RuntimeException("住所が未入力です。");
        }

        return store;
    }

    /**
     * 定休日のフィールドを作成します。
     * 
     * @param holidays
     * @return
     */
    private String buildHolidays(String[] holidays) {
        if (holidays == null)
            return null;

        StringBuilder bld = new StringBuilder();
        boolean isFirst = true;
        for (String holidayStr : holidays) {
            // 入力値チェック
            if (holidayStr.matches("^[0-6]$")) {
                if (isFirst) {
                    isFirst = false;
                } else {
                    bld.append(",");
                }
                bld.append(holidayStr);
            } else {
                LOG.warning("Unexpected value. holiday=" + holidayStr);
            }
        }
        return bld.toString();
    }

}
