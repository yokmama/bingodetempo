package jp.yokmama.slim3.bingodetemp.controller.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.service.GCMService;
import jp.yokmama.slim3.bingodetemp.service.JsonResponseService;
import jp.yokmama.slim3.bingodetemp.utils.ApiKeyInitializer;

import org.json.JSONObject;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;
import com.google.android.gcm.server.Message.Builder;
import com.google.appengine.api.datastore.Key;

/**
 * 現在プールしている番号を全てリセットする
 * @author esperia09
 *
 */
public class PoolResetController extends Controller {
    DatabaseService service = new DatabaseService();
    JsonResponseService json = new JsonResponseService();
    private GCMService gcmservice = new GCMService();
    private Sender sender;

    @Override
    public Navigation run() throws Exception {
        response.setCharacterEncoding("UTF-8");

        String reqUserId = request.getParameter("userId");

        final String userId = reqUserId;
        try {
            if (userId != null && userId.length() > 0) {
                Key userKey = Datastore.createKey(PrimarilyUser.class, userId);
                    service.clearPoolNumbers(userKey);
                    
                    new JSONObject(toJsonOK(0)).write(response.getWriter());
            } else {
                // オールリセット
                List<PrimarilyUser> users = service.getPrimarilyUsers();
                
                // IDの収集
                List<String> ids = new ArrayList<String>();
                for (Iterator<PrimarilyUser> ite = users.iterator(); ite.hasNext();) {
                    PrimarilyUser user = ite.next();

                    ids.add(user.getRegId());
                }
                
                service.clearPoolNumbers(users);

                Builder builder = new Message.Builder();
                builder.addData("action", "reset");
                gcmservice.sendMulticastMessage(getSender(), DatabaseService.createMulticast(ids), builder.build(), response);
                
                new JSONObject(toJsonOK(0)).write(response.getWriter());
            }
        } catch (RuntimeException e) {
            new JSONObject(toJsonNG(0, e.getMessage())).write(response
                .getWriter());
        }
        
        response.flushBuffer();
        return null;
    }

    private Sender getSender() {
        if (sender == null) {
            String key =
                (String) servletContext
                    .getAttribute(ApiKeyInitializer.ATTRIBUTE_ACCESS_KEY);
            return new Sender(key);
        }
        return sender;
    }
    
    private Map<String, Object> toJsonOK(int code) {
        Map<String, Object> resultmap = new HashMap<String, Object>();
        resultmap.put("satus", "ok");
        resultmap.put("code", code);
        return resultmap;
    }
    
    private Map<String, Object> toJsonNG(int code, String message) {
        Map<String, Object> resultmap = new HashMap<String, Object>();
        resultmap.put("satus", "ng");
        resultmap.put("code", code);
        if (message != null) {
            resultmap.put("message", message);
        }
        return resultmap;
    }
}
