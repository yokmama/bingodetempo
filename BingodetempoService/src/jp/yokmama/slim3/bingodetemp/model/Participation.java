package jp.yokmama.slim3.bingodetemp.model;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import jp.yokmama.slim3.bingodetemp.meta.ParticipationMeta;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.json.JSONObject;
import org.slim3.datastore.Attribute;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.Model;
import org.slim3.datastore.ModelRef;

import com.google.appengine.api.datastore.Key;

@Model(schemaVersion = 1)
public class Participation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;

    private String section;

    private String sectiondate;

    private Date updateDate;

    @Attribute(unindexed = true)
    private Date date;
    @Attribute(unindexed = true)
    private String service;
    @Attribute(unindexed = true)
    private String desc;
    @Attribute(persistent = true)
    private Set<Key> users = new HashSet<Key>();
    
//    @Attribute(unindexed = true)
    private ModelRef<Store> storeRef = new ModelRef<Store>(Store.class);

    /**
     * Returns the key.
     * 
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     * 
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     * 
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     * 
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Participation other = (Participation) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }

    public static Key createKey(String section, String title, Date date) {
        String datestr = Utils.getfmtYYYMMDD().format(date);
        return Datastore.createKey(
            ParticipationMeta.get(),
            (section + title + datestr).hashCode());
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ModelRef<Store> getStoreRef() {
        return storeRef;
    }

    public Set<Key> getUsers() {
        return users;
    }

    public void setUsers(Set<Key> users) {
        this.users = users;
    }

    public String getSectiondate() {
        return sectiondate;
    }

    public void setSectiondate(String sectiondate) {
        this.sectiondate = sectiondate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public JSONObject getJSONObject() throws UnsupportedEncodingException {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("key", getKey().getId());
        map.put("desc", Utils.encodUTF8(getDesc()));
        map.put("service", Utils.encodUTF8(getService()));
        map.put("numppl", getUsers().size());
        Store store = getStoreRef().getModel();
        if (store != null) {
            map.put("title", Utils.encodUTF8(store.getTitle()));
            map.put("lnglat", store.getLnglat());
        }

        return new JSONObject(map);
    }
}
