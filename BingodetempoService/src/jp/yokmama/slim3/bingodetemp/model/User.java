package jp.yokmama.slim3.bingodetemp.model;

import java.io.Serializable;
import java.util.Calendar;

import jp.yokmama.slim3.bingodetemp.utils.Utils;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Transaction;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.EntityNotFoundRuntimeException;
import org.slim3.datastore.Model;
import org.slim3.datastore.ModelRef;

@Model(schemaVersion = 1)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;

    @Attribute(unindexed = true)
    private ModelRef<Participation> storeRef = new ModelRef<Participation>(
        Participation.class);

    @Attribute(unindexed = true)
    private String regId;

    /** 一日一回、サーバから振られるユーザID */
    private long number;

    @Attribute(unindexed = true)
    private String nickname;

    @Attribute(unindexed = true)
    private String message;

    @Attribute(unindexed = true)
    private int sex;
    @Attribute(unindexed = true)
    private int bloodType;
    @Attribute(unindexed = true)
    private int showRange;
    @Attribute(unindexed = true)
    private String appeal;
    @Attribute(unindexed = true)
    private Key blobKey;

    public static User createUser(String fromKey) {
        User user = new User();
        Key key = Datastore.createKey(User.class, fromKey);
        user.setKey(key);
//        user.setRegId(regId);
        Datastore.put(user);
        return user;
    }

    /**
     * Returns the key.
     * 
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     * 
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     * 
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     * 
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        User other = (User) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }

    public ModelRef<Participation> getStoreRef() {
        return storeRef;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getBloodType() {
        return bloodType;
    }

    public void setBloodType(int bloodType) {
        this.bloodType = bloodType;
    }

    public int getShowRange() {
        return showRange;
    }

    public void setShowRange(int showRange) {
        this.showRange = showRange;
    }

    public String getAppeal() {
        return appeal;
    }

    public void setAppeal(String appeal) {
        this.appeal = appeal;
    }

    public Key getBlobKey() {
        return blobKey;
    }

    public void setBlobKey(Key blobKey) {
        this.blobKey = blobKey;
    }
}
