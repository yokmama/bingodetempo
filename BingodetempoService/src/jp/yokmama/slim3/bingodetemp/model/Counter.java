package jp.yokmama.slim3.bingodetemp.model;

import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Transaction;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.EntityNotFoundRuntimeException;
import org.slim3.datastore.Model;
import org.slim3.util.DateUtil;


@Model(schemaVersion = 1)
public class Counter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;

    private Long seq;
    
    /**
     * Returns the key.
     *
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     *
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Counter other = (Counter) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }
    
    public Long getSeq() {
        return seq;
    }
    public void setSeq(Long seq) {
        this.seq = seq;
    }

    private static Logger logger = Logger.getLogger(Counter.class.getName());
    private static final int RETRY_MAX = 10;

    private static Key createKey(Date date) {
        String keyName = DateUtil.toString(date, DateUtil.ISO_DATE_PATTERN);
        return Datastore.createKey(Counter.class, keyName);
    }

    public static Long increment() {
        return increment(new Date());
    }

    public static Long increment(Date date) {
        Key key = createKey(date);
        Transaction tx = Datastore.beginTransaction();
        try {
            return createOrUpdate(tx, key);
        } catch (ConcurrentModificationException e) {
            for (int i = 0; i < RETRY_MAX; i++) {
                try {
                    return createOrUpdate(tx, key);
                } catch (ConcurrentModificationException e2) {
                    logger.log(Level.WARNING, "Retry("
                        + i
                        + "): "
                        + e2.getMessage(), e2);
                }
            }
            throw e;
        }
    }

    private static Long createOrUpdate(Transaction tx, Key key) {
        Counter counter = null;
        try {
            counter = Datastore.get(tx, Counter.class, key);
            counter.setSeq(counter.getSeq() + 1);
            Datastore.put(tx, counter);
        } catch (EntityNotFoundRuntimeException e) {
            counter = new Counter();
            counter.setKey(key);
            counter.setSeq(new Long(1));
            Datastore.put(tx, counter);
        }
        Datastore.commit(tx);
        return counter.getSeq();
    }

    public static void clear() {
        clear(new Date());
    }

    public static void clear(Date date) {
        Key key = createKey(date);
        Counter counter = Datastore.get(Counter.class, key);
        counter.setSeq(new Long(0));
        Datastore.put(counter);
    }
}
