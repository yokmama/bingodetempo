package jp.yokmama.slim3.bingodetemp.model;

import java.io.Serializable;

import jp.yokmama.slim3.bingodetemp.meta.ParticipationMeta;
import jp.yokmama.slim3.bingodetemp.meta.StoreMeta;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.InverseModelListRef;
import org.slim3.datastore.Model;

import com.google.appengine.api.datastore.Key;

@Model(schemaVersion = 1)
public class Store implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;

    @Attribute(unindexed = true)
    private String title;
    @Attribute(unindexed = true)
    private String yomigana;
    @Attribute(unindexed = true)
    private String type;
    @Attribute(unindexed = true)
    private String desc;
    @Attribute(unindexed = true)
    private String phone;
    @Attribute(unindexed = true)
    private String opening;
    @Attribute(unindexed = true)
    private String address;
    @Attribute(unindexed = true)
    private String holiday;
    @Attribute(unindexed = true)
    private String holidayNote;
    @Attribute(unindexed = true)
    private String traffic;
    @Attribute(unindexed = true)
    private String card;
    @Attribute(unindexed = true)
    private String budget;
    @Attribute(unindexed = true)
    private String web;
    @Attribute(unindexed = true)
    private String blobKey;
    @Attribute(unindexed = true)
    private String lnglat;

    @Attribute(persistent = false)
    private InverseModelListRef<Participation, Store> participationsRefs =
        new InverseModelListRef<Participation, Store>(
                Participation.class,
            ParticipationMeta.get().storeRef.getName(),
            this);
    
    /**
     * Returns the key.
     * 
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     * 
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     * 
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     * 
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Store other = (Store) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }

    public static Key createKey(String title, String addr) {
        return Datastore.createKey(
            StoreMeta.get(),
            (title+addr).hashCode());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYomigana() {
        return yomigana;
    }

    public void setYomigana(String yomigana) {
        this.yomigana = yomigana;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOpening() {
        return opening;
    }

    public void setOpening(String opening) {
        this.opening = opening;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHoliday() {
        return holiday;
    }

    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }

    public String getTraffic() {
        return traffic;
    }

    public void setTraffic(String traffic) {
        this.traffic = traffic;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getBlobKey() {
        return blobKey;
    }

    public void setBlobKey(String blobKey) {
        this.blobKey = blobKey;
    }

    public String getLnglat() {
        return lnglat;
    }

    public void setLnglat(String lnglat) {
        this.lnglat = lnglat;
    }

    public String getHolidayNote() {
        return holidayNote;
    }

    public void setHolidayNote(String holidayNote) {
        this.holidayNote = holidayNote;
    }

    public InverseModelListRef<Participation, Store> getParticipationsRefs() {
        return participationsRefs;
    }
}
