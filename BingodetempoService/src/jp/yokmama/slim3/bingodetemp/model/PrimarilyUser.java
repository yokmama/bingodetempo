package jp.yokmama.slim3.bingodetemp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.InverseModelRef;
import org.slim3.datastore.Model;
import org.slim3.datastore.ModelRef;

import com.google.appengine.api.datastore.Key;

@Model(schemaVersion = 1)
public class PrimarilyUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;

    @Attribute(unindexed = true)
    private String regId;

    @Attribute(unindexed = true)
    private String nickname;

    @Attribute(unindexed = true)
    private int sex;
    @Attribute(unindexed = true)
    private int bloodType;
    @Attribute(unindexed = true)
    private int showRange;
    @Attribute(unindexed = true)
    private String appeal;
    @Attribute(unindexed = true)
    private Key blobKey;
    
    @Attribute(unindexed = true)
    private ModelRef<PrimarilyUser> parentRef = new ModelRef<PrimarilyUser>(
        PrimarilyUser.class);
    
    @Attribute(persistent = false)
    private InverseModelRef<PrimarilyUser, PrimarilyUser> childsRef = new InverseModelRef<PrimarilyUser, PrimarilyUser>(PrimarilyUser.class, "parentRef", this);
    
    @Attribute(persistent = true)
    private Set<Key> friends = new HashSet<Key>();
    
    @Attribute(persistent = true)
    private List<Integer> poolnumbers = new ArrayList<Integer>();
    
    
    @Attribute(persistent = false)
    private InverseModelRef<SecondaryUser, PrimarilyUser> secondaryRef = new InverseModelRef<SecondaryUser, PrimarilyUser>(SecondaryUser.class, "primarilyRef", this);

    public static PrimarilyUser createUser(String fromKey) {
        PrimarilyUser user = new PrimarilyUser();
        Key key = Datastore.createKey(PrimarilyUser.class, fromKey);
        user.setKey(key);
        Datastore.put(user);
        return user;
    }

    /**
     * Returns the key.
     * 
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     * 
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     * 
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     * 
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PrimarilyUser other = (PrimarilyUser) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getBloodType() {
        return bloodType;
    }

    public void setBloodType(int bloodType) {
        this.bloodType = bloodType;
    }

    public int getShowRange() {
        return showRange;
    }

    public void setShowRange(int showRange) {
        this.showRange = showRange;
    }

    public String getAppeal() {
        return appeal;
    }

    public void setAppeal(String appeal) {
        this.appeal = appeal;
    }

    public Key getBlobKey() {
        return blobKey;
    }

    public void setBlobKey(Key blobKey) {
        this.blobKey = blobKey;
    }

    public ModelRef<PrimarilyUser> getParentRef() {
        return parentRef;
    }
    
    public Set<Key> getFriends() {
        return friends;
    }

    public void setFriends(Set<Key> friends) {
        this.friends = friends;
    }

    public InverseModelRef<PrimarilyUser, PrimarilyUser> getChildsRef() {
        return childsRef;
    }

    public InverseModelRef<SecondaryUser, PrimarilyUser> getSecondaryRef() {
        return secondaryRef;
    }

    public List<Integer> getPoolnumbers() {
        return poolnumbers;
    }

    public void setPoolnumbers(List<Integer> poolnumbers) {
        this.poolnumbers = poolnumbers;
    }
 }
