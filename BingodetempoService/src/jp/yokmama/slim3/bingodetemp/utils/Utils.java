package jp.yokmama.slim3.bingodetemp.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Utils {
    public static final boolean DEBUG = false;
    
    private static SimpleDateFormat _sdfyyyymmdd = null;
    public static final TimeZone jpnTz = TimeZone.getTimeZone("Asia/Tokyo");

    public static SimpleDateFormat getfmtYYYMMDD() {
        if (_sdfyyyymmdd == null) {
            _sdfyyyymmdd = new SimpleDateFormat("yyyy/MM/dd");
            _sdfyyyymmdd.setTimeZone(jpnTz);
        }
        return _sdfyyyymmdd;
    }
    
    public static Date getSwichingDate(Calendar cal){
        //翌日7時までは前日に含める
        int hour = cal.get(Calendar.HOUR);
        if(hour < 7){
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    
    public static String encodUTF8(String text) throws UnsupportedEncodingException{
        if(text!=null){
            return URLEncoder.encode(text, "UTF-8");
        }
        return null;
    }

    public static boolean isNull(Object o) {
        if (o == null || o.toString().trim().length() == 0) {
            return true;
        }
        return false;
    }

    public static int valueInteger(String s, int defaultvalue) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
        }
        return defaultvalue;
    }
    
    public static long valueLong(String s, long defaultvalue) {
        try {
            return Long.parseLong(s);
        } catch (Exception e) {
        }
        return defaultvalue;
    }

    public static String valueString(String s) {
        if (s != null) {
            return s;
        }
        return "";
    }

    public static Date parseDate(String date) throws ParseException {
        return getfmtYYYMMDD().parse(date);
    }
    
    public static boolean equalsDate(Date a, Date b){
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeZone(jpnTz);
        calendar1.setTime(a);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTimeZone(jpnTz);
        calendar2.setTime(b);
        
        if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)){
            if(calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH)){
                if(calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                    return true;
                }
            }
        }
        
        return false;
    }

    public static String getHttpResponse(String url) {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
        try {
            URL urlPathtraq = new URL(url);
            reader =
                new BufferedReader(new InputStreamReader(
                    urlPathtraq.openStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(reader!=null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
