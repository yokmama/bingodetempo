package jp.yokmama.slim3.bingodetemp.utils;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.SpreadsheetQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class SSProperties {
    private SpreadsheetService service = null;
    private static SSProperties instance = null;
    private static String shopSheet;
    private static String applicationName = "SSProperties";

    public static synchronized SSProperties getInstance() throws IOException,
            AuthenticationException {
        if (instance == null) {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            Properties prop = new Properties();
            prop.load(loader.getResourceAsStream("ssproperties.properties"));
            String user = prop.getProperty("GoogleDocsUser", "rekayosystem.appengine@gmail.com");
            String pass = prop.getProperty("GoogleDocsPassword", "kayoitkayoit");
            shopSheet = prop.getProperty("ConfigSpreadSheet", "shopSheet");
            instance = new SSProperties(user, pass);
        }
        return instance;
    }
    
    private SSProperties(String user, String pass)
            throws AuthenticationException {
        service = loadService(user, pass);
    }
    
    private SpreadsheetService loadService(String user, String pass)
            throws AuthenticationException {
        SpreadsheetService svc = new SpreadsheetService(applicationName);
        svc.setUserCredentials(user, pass);
        return svc;
    }
    
    public SpreadsheetService getService(){
        return service;
    }
    
    public SpreadsheetEntry loadSpreadSheetEntry(String sheet)
            throws IOException, ServiceException {
        if (sheet == null) {
            return null;
        }
        FeedURLFactory factory = FeedURLFactory.getDefault();
        SpreadsheetQuery spreadsheetQuery =
            new SpreadsheetQuery(factory.getSpreadsheetsFeedUrl());
        spreadsheetQuery.setTitleQuery(sheet);
        SpreadsheetFeed spreadsheetFeed =
                service.query(spreadsheetQuery, SpreadsheetFeed.class);
        List<SpreadsheetEntry> entries = spreadsheetFeed.getEntries();
        
        Iterator<SpreadsheetEntry> iterator = entries.iterator();
        while (iterator.hasNext()) {
            SpreadsheetEntry next = iterator.next();
            if (sheet.equals(next.getTitle().getPlainText())) {
                return next;
            }
        }
        
        return null;
    }
}
