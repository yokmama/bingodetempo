var requestCache;
$(function() {
	//ストア情報を取得
	getStorelist();

	//ストア情報を要求
	function getStorelist() {
		var value = {};
		$.post("/json/GetStore", value, getStoreResult);
	}

	//ストア情報の取得結果を処理
	function getStoreResult(root) {
		if (typeof resp === 'string') {
			root = $.parseJSON(root);
		}
        console.log('root', root);
		if (root.status === 'ok') {
			requestCache = root;
			createStoreList(root.results);
		} else {
			requestCache = null;
			alert('データの取得に失敗しました');
		}
	}
	
	function reloadStoreResult(){
		if(requestCache==null){
			getStorelist();
		}else{
			createStoreList(requestCache.results);
		}
	}

    function decQuery(str) {
        if (str) {
            return decodeURIComponent(str);
        } else {
            // undefinedとかの時
            return '';
        }
    }

	/* ストアテーブルの生成 */
	function createStoreList(result) {
		//受け取ったデータからストア情報を取り出す
		var title = {};
		var yomigana = {};
		var type = {};
		var desc = {};
		var phone = {};
		var opening = {};
		var address = {};
		var holiday = {};
		var traffic = {};
		var card = {};
		var budget = {};
		var web = {};
		var key = {};
		var lnglat = {};

		var address = {};
		var index = result.length;
		for (var i = 0; i < index; i++) {
			key[i] = decQuery(result[i].key);
			title[i] = decQuery(result[i].title);
			type[i] = decQuery(result[i].type);
			desc[i] = decQuery(result[i].desc);
			phone[i] = decQuery(result[i].phone);
			opening[i] = decQuery(result[i].opening);
			address[i] = decQuery(result[i].address);
			holiday[i] = decQuery(result[i].holiday);
			traffic[i] = decQuery(result[i].traffic);
			card[i] = decQuery(result[i].card);
			budget[i] = decQuery(result[i].budget);
			web[i] = decQuery(result[i].web);
			lnglat[i] = result[i].lnglat;
			yomigana[i] = decQuery(result[i].yomigana);
		}
		var rows = "";

		//テーブルヘッダーを追加
		rows += "<tr>";
		//編集ボタン
		rows += "<th nowrap>詳細</th>";

		//店名
		if ($('#check-store').is(':checked')) {
			rows += "<th nowrap>店名</th>"
		}
		//よみがな
		if ($('#check-yomigana').is(':checked')) {
			rows += "<th nowrap>店名（ふりがな）</th>"
		}
		//ジャンル
		if ($('#check-type').is(':checked')) {
			rows += "<th nowrap>ジャンル</th>"
		}
		//お店の情報
		if ($('#check-desc').is(':checked')) {
			rows += "<th nowrap>お店の情報</th>"
		}
		//電話番号
		if ($('#check-phone').is(':checked')) {
			rows += "<th nowrap>電話番号</th>"
		}
		//住所
		if ($('#check-address').is(':checked')) {
			rows += "<th nowrap>住所</th>"
		}
		//定休日
		if ($('#check-holiday').is(':checked')) {
			rows += "<th nowrap>定休日</th>"
		}
		//交通・アクセス方法
		if ($('#check-traffic').is(':checked')) {
			rows += "<th nowrap>交通・アクセス情報</th>"
		}
		//クレジットカード
		if ($('#check-card').is(':checked')) {
			rows += "<th nowrap>カード利用</th>"
		}
		//予算
		if ($('#check-budget').is(':checked')) {
			rows += "<th nowrap>予算</th>"
		}
		//Webサイト
		if ($('#check-web').is(':checked')) {
			rows += "<th nowrap>Webサイト</th>"
		}
		//緯度・経度
		if ($('#check-lnglat').is(':checked')) {
			rows += "<th nowrap>緯度・経度</th>"
		}
		rows += "</tr>";

		//テーブルボディを追加
		for ( i = 0; i < index; i++) {
			var link = "shopdetail.html?key="+key[i];
			rows += "<tr>";
			//編集ボタン
			rows += "<td nowrap>";
			rows += "<a id=\"store-edit\" class=\"btn btn-primary\" href='" + link + "'>";
			rows += "編集" + "</a>";
			rows += "</td>";

			//店名
			if ($('#check-store').is(':checked')) {
				rows += "<td nowrap>";
				rows += title[i];
				rows += "</td>"
			}
			//よみがな
			if ($('#check-yomigana').is(':checked')) {
				rows += "<td nowrap>";
				rows += yomigana[i];
				rows += "</td>"
			}
			//ジャンル
			if ($('#check-type').is(':checked')) {
				rows += "<td nowrap>";
				rows += type[i];
				rows += "</td>"
			}
			//お店の情報
			if ($('#check-desc').is(':checked')) {
				rows += "<td nowrap>";
				rows += desc[i];
				rows += "</td>"
			}
			//電話番号
			if ($('#check-phone').is(':checked')) {
				rows += "<td nowrap>";
				rows += phone[i];
				rows += "</td>"
			}
			//住所
			if ($('#check-address').is(':checked')) {
				rows += "<td nowrap>";
				rows += address[i];
				rows += "</td>"
			}
			//定休日
			if ($('#check-holiday').is(':checked')) {
				rows += "<td nowrap>";
				rows += holiday[i];
				rows += "</td>"
			}
			//交通・アクセス方法
			if ($('#check-traffic').is(':checked')) {
				rows += "<td nowrap>";
				rows += traffic[i];
				rows += "</td>"
			}
			//クレジットカード
			if ($('#check-card').is(':checked')) {
				rows += "<td nowrap>";
				rows += card[i];
				rows += "</td>"
			}
			//予算
			if ($('#check-budget').is(':checked')) {
				rows += "<td nowrap>";
				rows += budget[i];
				rows += "</td>"
			}
			//Webサイト
			if ($('#check-web').is(':checked')) {
				rows += "<td nowrap>";
				rows += web[i];
				rows += "</td>"
			}
			//緯度・経度
			if ($('#check-lnglat').is(':checked')) {
				rows += "<td nowrap>";
				rows += lnglat[i];
				rows += "</td>"
			}
			rows += "</tr>"
		}

		//テーブルに作成したhtmlを追加する
		$('#store-table').append(rows);

        //編集ボタンをクリック
        $('#store-edit').click(function(e) {
            e.preventDefault();
        });
	}

	//店名チェックボックスをクリック
	$('#check-store').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//よみがなチェックボックスをクリック
	$('#check-yomigana').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//ジャンルチェックボックスをクリック
	$('#check-type').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//お店の情報チェックボックスをクリック
	$('#check-desc').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//電話番号チェックボックスをクリック
	$('#check-phone').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//住所チェックボックスをクリック
	$('#check-address').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//定休日ルチェックボックスをクリック
	$('#check-holiday').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//交通・アクセス方法チェックボックスをクリック
	$('#check-traffic').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//カードチェックボックスをクリック
	$('#check-card').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//予算チェックボックスをクリック
	$('#check-budget').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//Webサイトチェックボックスをクリック
	$('#check-web').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});
	//緯度・経度チェックボックスをクリック
	$('#check-lnglat').click(function() {
		$('#store-table tbody *').remove();
		$(reloadStoreResult);
	});

});
