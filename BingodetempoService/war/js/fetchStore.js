
// ユーティリティ
(function() {
	// IE系でconsoleオブジェクトがエラーになるのを防ぐ
	function enableConsoleObj() {
		var isExistConsole = true;
		try {
			if (!console) {
				isExistConsole  = false;
			}
		} catch (e){
			isExistConsole = false;
		}
		if (!isExistConsole) {
			window.console = {};
			window.console.log = function(){};
		}
	}
	enableConsoleObj();
})();


$(function() {
	// 店舗のIDをURLから取得
	var key = getUrlVars()["key"];

	// サーバに送るデータを持つ、各要素が入る変数
	var inputEls = getStoreEls();

	// サーバに送信する情報を保持している要素を集めます。
	function getStoreEls() {
		var inputEls = {};
		$('input, select, textarea').each(function(idx, el) {
			var name = $(el).attr('name');
			switch (name) {
				// input/select/textarea要素のnameを列挙する
				case 'holiday':
					if (!inputEls[name]) {
						inputEls[name] = [];
					}
					inputEls[name][$(el).val()] = el;
					break;
				default:
					inputEls[name] = el;
					break;
			}
		});
		console.log('inputEls=', inputEls);

		return inputEls;
	}

	//url param
	function getUrlVars() {
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i <hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}

	/* ストア情報をサイト内に配置します。 */
	function setStoreToHtml(result) {
		for (var k in result) {
			if (result.hasOwnProperty(k)) {
				console.log('result, k=', k);
				switch (k) {
					case 'holiday':
						setHoliday(result[k]);
						break;
					case 'part':
						setParticipations(result[k]);
						break;
					case 'card':
						inputEls[k].checked = result[k];
						break;
					default:
						// stringの場合のみ、文字列をデコードする
						if (typeof result[k] === 'string') {
							$(inputEls[k]).val(decodeURIComponent(result[k]));
						} else {
							$(inputEls[k]).val(result[k]);
						}
						break;
				}
			}
		}
	}

	/** 参加日をページに表示します。 */
	function setParticipations(parts) {
		$.each(parts, function(i, o) {
			var partEl = $('#part');

			// 同じ日付のものは追加しないようにする
			var opt = $('<option/>');
			opt.val(decodeURIComponent(o));
			opt.text(decodeURIComponent(o));
			opt.appendTo(partEl);
			console.log('part=', o);
		});
	}

	// 定休日をページ表示します。
	// @param {Array} holidayAry [0, 2, 6]等の0-6の値
	function setHoliday(holidayAry) {
		console.log('holidayAry=', holidayAry);
		// 一旦全てチェックボックスをリセットする
		$.each(inputEls.holiday, function(i, o) {
			o.checked = false;
		});
		$.each(holidayAry, function(i, o) {
			inputEls.holiday[o].checked = true;
		});
	}

	/* HTMLからストア情報の取得 */
	function getStoreFromHtml() {
		var value = {};
		for (var k in inputEls) {
			if (inputEls.hasOwnProperty(k)) {
				switch (k) {
					case 'holiday':
						// 曜日
						if (!value[k]) {
							value[k] = [];
						}
						$.each(inputEls[k], function(i, o) {
							if (o.checked) {
								value[k].push(Number($(o).val()));
							}
							console.log('isChecked=', o.checked, 'o=', o);
						});
						break;
					case 'card':
						value[k] = inputEls[k].checked;
						break;
					case 'part':
						// 参加日
						value[k] = (function(partEl) {
							var ary = [];
							var children = $(partEl).find('option').each(function(i, el) {
								ary.push($(el).val());
							});
							return ary;
						})(inputEls[k]);
						break;
					default:
						value[k] = $(inputEls[k]).val();
//						value[k] = encodeURIComponent($(inputEls[k]).val());
						break;
				}
				console.log('getStoreFromHtml, formKey=', k, 'value=', value[k]);
			}
		}

		return value;
	}

	//ストア情報を要求
	function getStore(id) {
		var value = {};
		value.storeId = id;

		//getStoreStub(getStoreResult); // TODO using stub
		$.post("/json/GetStore", value, getStoreResult);
	}

	// ストア情報の取得結果を処理
	function getStoreResult(resp) {
			console.log('resp=', resp);
		var root = jQuery.parseJSON(resp);
		if (root.status == 'ok') {
			setStoreToHtml(root.result);
		} else {
			alert('店舗情報の取得に失敗しました');
		}
	}

	// スタブです。指定数秒後にcallbackを呼びます。
	function getStoreStub(callback) {
		var result = {
			'status': 'ok',
			'result': {
				"title" : "test",
				"yomigana" : "add",
				"type" : "pub",
				// お店の情報
				"phone" : "078-234-5678",
				"part" : "お店の詳細です。",
				"desc" : [
					"2012%2F10%2F03",
					"2012%2F10%2F05"
				],
				"holiday" : [
					0, 5
				],
				"holiday_note" : "但し第2, 第4月曜日は定休日",
				"budget" : 2000,
				"address" : "兵庫県神戸市垂水区神田町ほげほげのほげ",
				"traffic" : "垂水駅より徒歩3分",
				"web" : "http://www.google.co.jp/",
				"card" : true,
				"lnglat" : "135189364,34694046"
			}
		};

		setTimeout(function() {
			callback(JSON.stringify(result));
		}, 500);
	}

	// 各コントロールにイベントを設定します。
	function setEvent() {
		$('#btn-save').click(function(e) {
			var btnSave = $(this);
			e.preventDefault();
			if (confirm('この内容で登録します。よろしいでしょうか？')) {
				var store = getStoreFromHtml();
				store.storeId = key;

				// 情報の登録
				console.log('store=', store);
				$.post("/json/SetStore", store, function(resp) {
					console.log('SetStore,resp=', resp);
					if (resp.status === 'ok') {
//						alert('登録に成功しました');
						location.href = btnSave.attr('href');
					} else {
						alert('データの取得に失敗しました');
					}
				});
			}
		});
		$('#btn-cancel').click(function(e) {
			location.href = $(this).attr('href');
		});
		$('#btn-part-add').click(function(e) {
			e.preventDefault();
			var partEl = $('#part');
			var datepickerEl = $('#datepicker');
			var value = $.trim(datepickerEl .val());
			if (!value) {
				// 日付が空
//				console.log('value is empty.');
				return;
			}

			var isExist = (function() {
				var is = false;
				// 既に同じ日付があるかどうかをチェック
				partEl.find('option').each(function(i, o) {
//					console.log('val=', $(o).val(), 'value=', value);
					if ($(o).val() === value) {
						is = true;
						return false;
					}
				});
				return is;
			})();
			if (isExist) {
				return;
			}

			// 同じ日付のものは追加しないようにする
			var opt = $('<option/>');
			opt.val(value);
			opt.text(value);
			opt.appendTo(partEl);
//			console.log('partEl=', partEl, 'option=', opt);
			datepickerEl.val('');
		});
		$('#btn-part-delete').click(function(e) {
			e.preventDefault();
			var partEl = $('#part');
			partEl.find('option:selected').remove();
		});
	}

	getStore(key);
	setEvent();
});
