
/**
 * requestAnimationFrame
 */
(function (w, r) {
	w['r'+r] = w['r'+r] || w['webkitR'+r] || w['mozR'+r] || w['msR'+r] || w['oR'+r] || function(c){ w.setTimeout(c, 1000 / 60); };
})(window, 'equestAnimationFrame');

var Utils = (function() {
    return {
        /**
         * H:i:s 形式で返す
         * ex.) 01:23:45
		 * @params {number} raw ミリ秒の数値データ
         */
        timeHis: function(raw) {
            var s = parseInt(raw / 1000, 10);
            var ms = raw - s * 1000;
            var h = parseInt(s / 60, 10) % 60;
            //console.log('raw', raw, 'ms', ms, 's', s, 'h', h);

            var time =
                ( '0' + parseInt(s / 3600, 10) ).slice(-2) +
                ':' +
                ( '0' + h ).slice(-2) +
                ':' +
                ( '0' + (s % 60) ).slice(-2);
            return time;
        },
		showCoords: function(targetEl) {
			var elm = $('<div/>').appendTo($('body'));
			var canvasElm = $(targetEl);
			elm.text("0, 0");
			canvasElm.mousemove(function(e) {
				elm.text(e.offsetX + ", " + e.offsetY);
			});

		}
    };
})();

var Net = (function() {
    var baseUrl = window.settings.server_url;
//    var baseUrl = 'http://bingodetempo.appspot.com/';
//    var baseUrl = 'http://localhost:8888/';

	function get(url, params, done, fail, always) {
		console.log('request=', url);
		var ajax = (params) ? $.get(url, params) : $.get(url);
		ajax.done(function(data, textStatus, jqXHR) {
			data = (typeof data === 'string') ? JSON.parse(data) : data;
			console.log('done, data', data);
			if (data.status === 'ng') {
				if (fail) {
					fail.apply(fail, [jqXHR, textStatus, new Error()]);
				}
			}
			else if (done) {
				done.apply(done, arguments);
			}
		}).fail(function(jqXHR, textStatus, errorThrown) {
			console.log('fail, data', textStatus);
			if (fail) {
				fail.apply(fail, arguments);
			}
		}).always(function() {
			if (always) {
				always.apply(always, arguments);
			}
		});
	}

    return {
        init: function(baseUrl_) {
            if (!baseUrl) {
                throw 'baseUrlが指定されていません。';
            }
            baseUrl = baseUrl_;
        },

        /**
         * ビンゴを回す
         */
        lot: function(done, fail) {
            var url = baseUrl + 'json/Lot';
			get(url, null, function(data) {
				console.log('lot');
				done(data);
			}, function() {
				fail();
			});
        }
    };
})();


/**
 * 画像のプリロードを行います。
 */
var Preload = (function() {
	var imagePathes = [];
	var images = [];
	var loaded = [];
	var ip = 0;
	var onSuccess;

	// クロスブラウザにaddEventListener
	function addEvent(target, type, listener) {
		//@cc_on
		target./*@if (@_jscript_version < 5.9) attachEvent('on' +
			@else@*/addEventListener(/*@end@*/type, listener, false);
	}

	function checkLoaded(elm) {
		loaded[ip++] = true;
		console.log(elm);

		var loadedFlg = true;
		for(var j=0, len = images.length; j<len; j++){
			if(!loaded[j]){
				loadedFlg = false;
				break;
			}
		}
		if (loadedFlg) {
			// 全部チェックできていればcallback実行
			onSuccess(images);
		}
	}

	return {
		images: function(imgs, callback) {
			imagePathes = imgs;
			onSuccess = callback;

            var loadedCallback = function() {
                checkLoaded(this);
            };

			var nowMillis = new Date().getTime();
			for(var i=0, len = imagePathes.length; i<len; i++) {
				loaded[i] = false;
				var img = new Image();
				img.src = imagePathes[i] + '?' + nowMillis;
				images[i] = img;

				addEvent(images[i], "load", loadedCallback);
			}
		}
	};
})();



/**
 * Bingo
 */
var BingoEvents = (function() {
	var SPEED = 5;
	var ctx;

	var f = function(context, imgs, endCallback) {
		console.log('constructor : BingoEvents');
		ctx = context;
		this.images = imgs;
		this.endCallback = endCallback;
		this.theta = 0.0;
		this.rotationCount = 0;
		this.num = 0;

		ctx.font = "bold 12px sans-serif";

		/** scene num */
		this.scene = 0;

		// mapping
		this.handleImg = this.images[0];
		this.ballImg = this.images[1];
		this.bodyImg = this.images[2];
		this.baseImg = this.images[3];
	};

	f.prototype.looper = function() {
		this.clear();
		if (this.scene === 0) {
			this.draw();
		} else if (this.scene === 1) {
			this.ballEvent();
			this.draw();
		} else if (this.scene === 2) {
			this.ballRollEvent();
			this.draw();
		}
	};

    f.prototype.clear = function() {
		ctx.save();

		ctx.beginPath();
		ctx.clearRect(0, 0, canvas.width, canvas.height);

		ctx.restore();
    };

	f.prototype.draw = function() {
		var handleImg = this.images[0];
		var ballImg = this.images[1];
		var bodyImg = this.images[2];
		var baseImg = this.images[3];

		ctx.save();

		drawCircle(this.theta, bodyImg);

		ctx.restore();
		ctx.save();

		ctx.drawImage(baseImg, 82, 220);

		ctx.restore();
		ctx.save();

		drawHandle(this.theta, handleImg);

		ctx.restore();

		if (this.scene === 0) {
			// 現在このシーンである場合
			this.speedUp();
			if (this.rotationCount >= 2) {
				console.log('start ball');
				this.scene++;
			}
		} else {
		}
	};
	f.prototype.speedUp = function() {
		this.theta += SPEED * Math.PI / 180;
		if (Math.PI * 2 < this.theta) {
			this.theta = 0;
			this.rotationCount++;
		}
//		console.log("theta" + this.theta);
	};

	/**
	 * after #draw
	 */
	f.prototype.ballEvent = function() {
		if (!this.ballBehavior) {
			var self = this;
			var ballImg = this.ballImg;
            var metrics = ctx.measureText(this.num);
            var numWidth = metrics.width;
			this.ballBehavior = new MoveStraight({
				sx: 421,
				sy: 325,
				dx: 333,
				dy: 474 - this.ballImg.height
			}, 0.5);
			this.ballBehavior.draw = function(c) {
                ctx.save();

				ctx.translate(c.sx + c.mx, c.sy + c.my);
				ctx.drawImage(ballImg, 0, 0);
                ctx.fillStyle = '#FFF';
//                console.log('ball', 'w', self.ballImg.width, 'h', self.ballImg.height,
//                           'tw', numWidth);
				ctx.fillText(self.num,
                             (self.ballImg.width/2) - (numWidth/2),
                             (self.ballImg.height/2) + 5);

                ctx.restore();
			};
			this.ballBehavior.callback = function() {
                self.scene++;
			};
		}
		this.ballBehavior.tick();
	};

    function speedUp(theta, speed) {
		theta += speed * Math.PI / 180;
		if (Math.PI * 2 < theta) {
			theta = 0;
		}
        return theta;
    }

	/**
	 * after #ballEvent
	 */
	f.prototype.ballRollEvent = function() {
		if (!this.ballRollBehavior) {
			var self = this;
            var theta = 0.0;

            var metrics = ctx.measureText(this.num);
            var numWidth = metrics.width;

			this.ballRollBehavior = new MoveStraight({
				sx: this.ballBehavior.coords.dx,
				sy: this.ballBehavior.coords.dy,
				dx: 179,
				dy: 474 - this.ballImg.height
			}, 0.5);
            this.ballRollBehavior.draw = function(c) {
                ctx.save();

                ctx.translate(c.sx + c.mx, c.sy + c.my);
                ctx.drawImage(self.ballImg, 0, 0);
                ctx.fillStyle = '#FFF';

                if (!this.isFinished) {
                    theta = speedUp(theta, -SPEED);
                }
                ctx.translate((self.ballImg.width/2), (self.ballImg.height/2));
                ctx.rotate(theta);
                ctx.translate(-(self.ballImg.width/2), -(self.ballImg.height/2));
                ctx.fillText(self.num,
                             (self.ballImg.width/2) - (numWidth/2),
                             (self.ballImg.height/2) + 5);

                ctx.restore();
			};
			this.ballRollBehavior.callback = function() {
				console.log('end ballEvent');

				// initまで少し待つ
				setTimeout(function() {
					self.endCallback();
				}, 2000);
			};
		}
		this.ballRollBehavior.tick();
	};

	var Scale = (function() {
        var f = function() {
        };
        f.prototype.draw = function() {
        };
        return f;
    })();

	/**
	 * まっすぐ進むオブジェクトを定義する
	 * @param {Image} 
	 * @param {object} 行き先を指定 { sx, sy, dx, dy }
	 * @param {number} スピードを指定
	 */
	var MoveStraight = (function() {
		var f = function(coords, speed) {
			this.coords = coords;
			this.speed = speed;

			this.callback = null;
			this.isFinished = false;

			// 現在の位置からの差分
			this.coords.mx = 0;
			this.coords.my = 0;

			// 増分
			var a = Math.abs(coords.dx - coords.sx);
			var b = Math.abs(coords.dy - coords.sy);
			var distance = Math.sqrt( a^2 + b^2 );
			console.log('distance=' + distance);
			var ratio = speed / distance;
			this.coords.px = (coords.dx - coords.sx) * ratio;
			this.coords.py = (coords.dy - coords.sy) * ratio;
			console.log('px=' + this.coords.px + ', py=' + this.coords.py);
		};
		f.prototype.tick = function() {
			var c = this.coords;

			//console.log('mx=' + c.mx + ', my=' + c.my);

			if (this.draw) {
				this.draw(c);
			}

			var isFinished = false;
			if (this.isFinished) {
//				console.log('already finished!');
				return;
			} else {
				// 値増加
				c.mx += c.px;
				c.my += c.py;

				// 終了処理
				isFinished = (function(c) {
					var isX = false;
					if (c.px > 0) {
						isX = (c.sx + c.mx) > c.dx;
					} else {
						isX = (c.sx + c.mx) <= c.dx;
					}
					var isY = false;
					if (c.py > 0) {
						isY = (c.sy + c.my) > c.dy;
					} else {
						isY = (c.sy + c.my) <= c.dy;
					}
					if (isX && isY) {
						return true;
					}
                    console.log('isX', isX, 'isY', isY);
					return false;
				})(c);
			}
			if (isFinished && !this.isFinished) {
				// 初めてFinishedになった時に一度だけ来る
				this.isFinished = true;
				if (this.callback) {
					this.callback();
				}
			}
		};
		return f;
	})();
	
	// http://tech.kayac.com/archive/canvas-tutorial.html
	function drawCircle(theta, img) {
		// 回転の中心は原点なので、
		// 一度図形の中心を原点に移してから回転させて
		// 元の場所に戻す
		ctx.translate(519, 243);
		ctx.rotate(theta);
		ctx.translate(-img.width / 2, -img.height / 2);
		ctx.drawImage(img, 0, 0);
	}

	function drawHandle(theta, img) {
		ctx.translate(519, 243);
		ctx.rotate(theta);
		ctx.translate(-17, -17);
		ctx.drawImage(img, 0, 0);
	}

	return f;
})();


/**
 * Main
 */
var Main = (function() {
	var ctx;
	var images;
	var canvas;
	var isLooping = false;
	var bingo;
	var timerId = -1;
	var lotCache = null;

	/**
	 * 前回値と比較し、回した状態になっているかどうかをチェックします。
	 * @return {boolean} 前回と比較し、回していればtrueを返します。
	 */
	function isLotted(lot) {
		if (lotCache === null || !lot) {
			console.log('それぞれのデータが空の場合', lotCache, lot);
			return false;
		}
		if (lotCache.num != lot.num) {
			return true;
		}
		return false;
	}

	/**
	 * サーバに対して試しにビンゴのチェックをしてみる。
	 */
	function tryLot() {
		var lotTimeEl = $('#lot-time');

		Net.lot(function(data) {
			var isFirstTime = (lotCache === null);

			// 番号リスト更新
			var numbersEl = $('#numbers').empty();
			$.each(data.numbers, function(i, o) {
				$('<li/>').text(o).appendTo(numbersEl);
			});
//			if (window.settings.debug) {
//				$('<li/>').text(11).appendTo($('#numbers'));
//				$('<li/>').text(22).appendTo($('#numbers'));
//				$('<li/>').text(33).appendTo($('#numbers'));
//			}

			if (!isLotted(data)) {
				console.log('前と同じ番号の場合');
				if (data.numbers.length <= 0) {
					console.log('ひくbingoがなかった時。 指定時間後にリトライする');
					var nextRetry = data.next - data.now;
					//					var nextRetry = 3 * 60 * 1000;
					var nextRetryId = setInterval(function() {
						nextRetry -= 1000;
						lotTimeEl.text('抽選券待ちです…。' + Utils.timeHis(nextRetry));
						//console.log(nextRetry);
						if (nextRetry <= 0) {
							clearInterval(nextRetryId);
							tryLot();
						}
					}, 1000);

					lotCache = data;
					return;
				} else {
					console.log('次の抽選待ち状態');
				}
			} else {
				console.log('抽選がきた！ num=', data.num);
			}

			lotCache = data;

			console.log('check timer');
			// 次に回す時刻をチェックしてタイマーをセット
			var nowMillis = data.now;
			var nextMillis = data.next - data.now;

			if (timerId !== -1) {
				clearInterval(timerId);
			}
			timerId = setInterval(function() {
				nextMillis -= 1000;
				var str = Utils.timeHis(nextMillis);
				// console.log('time=' + str);
				lotTimeEl.text('次の抽選まで、あと' + str);
				if (nextMillis <= 0) {
					console.log('次に回す時間', str);
					clearInterval(timerId);
					timerId = -1;
					tryLot();
				}
			}, 1000);

			if (isFirstTime) {
				// 最初のlot
				// 時間だけ回して、待ち状態にしている
				console.log('first lot');
			} else {
				if (!isLooping && bingo) {
					isLooping = true;
					bingo.num = data.num;
					looper();
				} else {
					console.log('ループ中、もしくは初期化が終わっていません。');
				}
			}
		}, function() {
			// 失敗したら５秒後にリトライ
			console.log('通信失敗');
			lotTimeEl.text('取得に失敗しました。リトライしています…。');
			setTimeout(function() {
				console.log('リトライ');
				tryLot();
			}, 5000);
		});
	}

	function setEvents() {
	}

	/**
	 * Bingoの表示を初期化
	 */
	function initBingo() {
		// object 初期化
		bingo = new BingoEvents(ctx, images, end);
		console.log('start draw');
		bingo.draw();
	}

	function looper() {
		if (isLooping) {
			window.requestAnimationFrame(looper);
		} else {
			return;
		}

		bingo.looper();
	}

	/**
	 * ビンゴ引き終わった時
	 */
	function end() {
		console.log('end callback');
		isLooping = false;
		initBingo();
//        startTimer();
	}

	return {
		init: function() {
			// provide canvas
			canvas = $('#canvas')[0];
			if ( ! canvas || ! canvas.getContext ) {
				return false;
			}
			ctx = canvas.getContext('2d');

			setEvents();

			if (window.settings.debug) {
				Utils.showCoords($('#canvas')[0]);
			}

			Preload.images([
				'/img/kuji-handle.png',
				'/img/kuji-ball.png',
				'/img/kuji-body.png',
				'/img/kuji-base.png'
			], function(imgs) {
				images = imgs;
				initBingo();

				tryLot();
			});
		}
	};
})();

$(function() {
	Main.init();
});
