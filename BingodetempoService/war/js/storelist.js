$(document).ready(function() {
	//ページ読み込み時の処理
	$(window).load(function() {
		//ストア情報を取得
		$(getStore(1989506607));
	});

	//ストア情報を要求
	function getStore() {
		var value = {};
		$.post("/json/GetStore", value, getStoreResult);
	}

	//ストア情報を要求
	function getStore(id) {
		var value = {};
		value["storeId"] = id;
		$.post("/json/GetStore", value, getStoreResult);
	}

	//ストア情報の取得結果を処理
	function getStoreResult(resp) {
		var root = {
			"results" : [{
				"title" : "federico+Garcia+Lorcaf",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E4%B8%8B%E5%B1%B1%E6%89%8B%E9%80%9A%EF%BC%92-13-22",
				"lnglat" : "135189122,34693465",
				"key" : -2138056527
			}, {
				"title" : "BAR-ZIN",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%B1%B1%E6%89%8B%E9%80%9A1-2-7+%E3%83%AA%E3%83%83%E3%83%81%E3%83%93%E3%83%ABB1F",
				"lnglat" : "135192061,34694138",
				"key" : -2137687155
			}, {
				"title" : "Di+Shiner",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E5%85%83%E7%94%BA%E9%80%9A1-13-11",
				"lnglat" : "135188311,34689505",
				"key" : -2079950529
			}, {
				"title" : "%EF%BC%A2%EF%BC%A1%EF%BC%B2%E3%80%80%E3%83%96%E3%83%AD%E3%82%B3%E3%83%BC%E3%83%88",
				"phone" : "078%EF%BC%8D322-3321",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E5%8C%97%E9%95%B7%E7%8B%AD%E9%80%9A%EF%BC%92%E4%B8%81%E7%9B%AE%E3%83%9F%E3%82%BA%E3%82%AD%E3%83%93%E3%83%AB%EF%BC%93%EF%BC%A6",
				"lnglat" : "135191267,34692404",
				"key" : -2058195209
			}, {
				"title" : "caldo",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E5%85%83%E7%94%BA%E9%80%9A1-7-2+%E3%83%8B%E3%83%A5%E3%83%BC%E3%82%82%E3%81%A8%E3%83%93%E3%83%AB5F",
				"lnglat" : "135189170,34689035",
				"key" : -2020438632
			}, {
				"title" : "%E4%B8%87%E8%8F%AF%E9%8F%A1",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E5%8A%A0%E7%B4%8D%E7%94%BA4-9-2+AOI%E3%83%93%E3%83%AB5F",
				"lnglat" : "135193211,34695671",
				"key" : -2017775098
			}, {
				"title" : "%E3%83%9E%E3%82%B6%E3%83%BC%E3%83%A0%E3%83%BC%E3%83%B3%E3%82%AB%E3%83%95%E3%82%A7%E5%85%AD%E7%94%B2",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E4%B8%AD%E5%B1%B1%E6%89%8B%E9%80%9A2-3-25",
				"lnglat" : "135189186,34694293",
				"key" : 1965308860
			}, {
				"title" : "%E7%82%AD%E7%81%AB%E7%84%BC%E8%82%89+%E6%9D%BE%E7%94%B0",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E6%9D%B1%E7%81%98%E5%8C%BA%E7%94%B0%E4%B8%AD%E7%94%BA3-19-16-106",
				"lnglat" : "135272918,34725016",
				"key" : 1989506607
			}, {
				"title" : "%E3%83%9E%E3%82%B6%E3%83%BC%E3%83%A0%E3%83%BC%E3%83%B3%E5%9B%BD%E9%9A%9B%E4%BC%9A%E9%A4%A8%E5%BA%97",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E5%BE%A1%E5%B9%B8%E9%80%9A8-1-6+%E5%9B%BD%E9%9A%9B%E4%BC%9A%E9%A4%A8B1F",
				"key" : 2007711286
			}, {
				"title" : "%E7%84%BC%E9%85%8E%E3%83%80%E3%82%A4%E3%83%8B%E3%83%B3%E3%82%B0%E5%AE%B4%E9%99%A3",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E5%8C%97%E9%95%B7%E7%8B%AD%E9%80%9A1-21-6%E3%82%B4%E3%83%BC%E3%83%AB%E3%83%87%E3%83%B3%E3%82%A6%E3%83%83%E3%82%BA%E3%82%B3%E3%83%9A%E3%83%B3%E3%83%93%E3%83%AB2F",
				"lnglat" : "-89877677,30685112",
				"key" : 2024244679
			}, {
				"title" : "%E5%92%8C%E9%A3%9F%EF%BD%93%EF%BD%94%EF%BD%99le%E7%99%BE%E5%90%88%E5%BA%B5",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E4%B8%AD%E5%B1%B1%E6%89%8B%E9%80%9A1-9-2MOZAN%E3%83%93%E3%83%AB203",
				"key" : 2024893929
			}, {
				"title" : "%E3%83%8A%E3%82%A4%E3%82%B9%E3%83%90%E3%83%BC",
				"address" : "%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E4%B8%8B%E5%B1%B1%E6%89%8B%E9%80%9A2-15-12+MK%E3%83%93%E3%83%AB3F",
				"lnglat" : "135189364,34694046",
				"key" : 2036376971
			}],
			"status" : "ok"
		};
		//var root = jQuery.parseJSON(resp2);
		if (root.status == 'ok') {
			if (root.type == 'one') {
			}else {
				createStoreList(root.results);
			}
		} else {
			//TODO 取得失敗処理
			alert('データの取得に失敗しました');
		}
	}

	/* ストアテーブルの生成 */
	function createStoreList(result) {
		//受け取ったデータからストア情報を取り出す
		var title = {};
		var yomigana = {};
		var type = {};
		var desc = {};
		var phone = {};
		var opening = {};
		var address = {};
		var holiday = {};
		var traffic = {};
		var card = {};
		var budget = {};
		var web = {};
		var key = {};
		var lnglat = {};

		var address = {};
		var index = result.length;
		for (var i = 0; i < index; i++) {
			key[i] = decodeURI(result[i].key);
			title[i] = decodeURI(result[i].title);
			type[i] = decodeURI(result[i].type);
			desc[i] = decodeURI(result[i].desc);
			phone[i] = decodeURI(result[i].phone);
			opening[i] = decodeURI(result[i].opening);
			address[i] = decodeURI(result[i].address);
			holiday[i] = decodeURI(result[i].holiday);
			traffic[i] = decodeURI(result[i].traffic);
			card[i] = decodeURI(result[i].card);
			budget[i] = decodeURI(result[i].budget);
			web[i] = decodeURI(result[i].web);
			lnglat[i] = result[i].lnglat;
			yomigana[i] = decodeURI(result[i].yomigana);
		}
		var rows = "";

		//テーブルヘッダーを追加
		rows += "<tr>";
		//編集ボタン
		rows += "<th nowrap>詳細</th>";

		//店名
		if ($('#check-store').is(':checked')) {
			rows += "<th nowrap>店名</th>"
		}
		//よみがな
		if ($('#check-yomigana').is(':checked')) {
			rows += "<th nowrap>店名（ふりがな）</th>"
		}
		//ジャンル
		if ($('#check-type').is(':checked')) {
			rows += "<th nowrap>ジャンル</th>"
		}
		//お店の情報
		if ($('#check-desc').is(':checked')) {
			rows += "<th nowrap>お店の情報</th>"
		}
		//電話番号
		if ($('#check-phone').is(':checked')) {
			rows += "<th nowrap>電話番号</th>"
		}
		//住所
		if ($('#check-address').is(':checked')) {
			rows += "<th nowrap>住所</th>"
		}
		//定休日
		if ($('#check-holiday').is(':checked')) {
			rows += "<th nowrap>定休日</th>"
		}
		//交通・アクセス方法
		if ($('#check-traffic').is(':checked')) {
			rows += "<th nowrap>交通・アクセス情報</th>"
		}
		//クレジットカード
		if ($('#check-card').is(':checked')) {
			rows += "<th nowrap>カード利用</th>"
		}
		//予算
		if ($('#check-budget').is(':checked')) {
			rows += "<th nowrap>予算</th>"
		}
		//Webサイト
		if ($('#check-web').is(':checked')) {
			rows += "<th nowrap>Webサイト</th>"
		}
		//緯度・経度
		if ($('#check-lnglat').is(':checked')) {
			rows += "<th nowrap>緯度・経度</th>"
		}
		rows += "</tr>";

		//テーブルボディを追加
		var link = "shopdetail.html";
		for ( i = 0; i < index; i++) {
			rows += "<tr>";
			//編集ボタン
			rows += "<td nowrap>";
			rows += "<a id=\"store-edit\" class=\"btn btn-primary\" href=" + link + '#' + key[i] + ">";
			rows += "編集" + "</a>";
			rows += "</td>";

			//店名
			if ($('#check-store').is(':checked')) {
				rows += "<td nowrap>";
				rows += title[i];
				rows += "</td>"
			}
			//よみがな
			if ($('#check-yomigana').is(':checked')) {
				rows += "<td nowrap>";
				rows += yomigana[i];
				rows += "</td>"
			}
			//ジャンル
			if ($('#check-type').is(':checked')) {
				rows += "<td nowrap>";
				rows += type[i];
				rows += "</td>"
			}
			//お店の情報
			if ($('#check-desc').is(':checked')) {
				rows += "<td nowrap>";
				rows += desc[i];
				rows += "</td>"
			}
			//電話番号
			if ($('#check-phone').is(':checked')) {
				rows += "<td nowrap>";
				rows += phone[i];
				rows += "</td>"
			}
			//住所
			if ($('#check-address').is(':checked')) {
				rows += "<td nowrap>";
				rows += address[i];
				rows += "</td>"
			}
			//定休日
			if ($('#check-holiday').is(':checked')) {
				rows += "<td nowrap>";
				rows += holiday[i];
				rows += "</td>"
			}
			//交通・アクセス方法
			if ($('#check-traffic').is(':checked')) {
				rows += "<td nowrap>";
				rows += traffic[i];
				rows += "</td>"
			}
			//クレジットカード
			if ($('#check-card').is(':checked')) {
				rows += "<td nowrap>";
				rows += card[i];
				rows += "</td>"
			}
			//予算
			if ($('#check-budget').is(':checked')) {
				rows += "<td nowrap>";
				rows += budget[i];
				rows += "</td>"
			}
			//Webサイト
			if ($('#check-web').is(':checked')) {
				rows += "<td nowrap>";
				rows += web[i];
				rows += "</td>"
			}
			//緯度・経度
			if ($('#check-lnglat').is(':checked')) {
				rows += "<td nowrap>";
				rows += lnglat[i];
				rows += "</td>"
			}
			rows += "</tr>"
		}

		//テーブルに作成したhtmlを追加する
		$('#store-table').append(rows);

		//編集ボタンをクリック
		$('#store-edit').click(function(e) {
//			e.preventDefault();
//			var href = $(this).attr('href');
//			alert('href=' + href);
		});
	}

	//店名チェックボックスをクリック
	$('#check-store').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//よみがなチェックボックスをクリック
	$('#check-yomigana').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//ジャンルチェックボックスをクリック
	$('#check-type').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//お店の情報チェックボックスをクリック
	$('#check-desc').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//電話番号チェックボックスをクリック
	$('#check-phone').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//住所チェックボックスをクリック
	$('#check-address').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//定休日ルチェックボックスをクリック
	$('#check-holiday').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//交通・アクセス方法チェックボックスをクリック
	$('#check-traffic').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//カードチェックボックスをクリック
	$('#check-card').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//予算チェックボックスをクリック
	$('#check-budget').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//Webサイトチェックボックスをクリック
	$('#check-web').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});
	//緯度・経度チェックボックスをクリック
	$('#check-lnglat').click(function() {
		$('#store-table tbody *').remove();
		$(getStore);
	});

});
