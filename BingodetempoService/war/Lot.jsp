<%@page pageEncoding="UTF-8" isELIgnored="false" session="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="f" uri="http://www.slim3.org/functions"%>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="UTF-8" />
		<title>ビンゴで店舗</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.6" />

		<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
		<link rel="stylesheet" href="/css/lot.css" />

		<!--[if lt IE 9]> 
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script> 
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<script>
			window.settings = {
				server_url: '${serverName}',
				debug: ${debug}
			};
		</script>
		<script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
		<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/Box2dWeb-2.1.a.3.min.js"></script>
		<script type="text/javascript" src="/js/lot.js"></script>
	</head>
	<body>
		<div class="header contents falicity">
			<div class="container">
				<h1>Bingo De 店舗</h1>
				<span id="lot-time">取得中…</span>
			</div>
		</div>
		<div class="contents">
			<canvas id="canvas" width="800" height="600"></canvas>
		</div>
		<div class="contents bluesky">
			<div class="container">
				<h2>抽選待ち番号</h2>
				<ul id="numbers"></ul>
			</div>
		</div>
	</body>

</html>
