<%@page pageEncoding="UTF-8" isELIgnored="false" session="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="f" uri="http://www.slim3.org/functions"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Maintain</title>
	</head>
	<body>
		<h1>メンテナンス</h1>
		<div class="container">
			<form action="/json/PoolReset" method="post">
				<h2>ビンゴをリセットする</h2>
				<p>ビンゴを一からやり直す場合は、以下のボタンをクリックして下さい。</p>
				<input type="submit" value="ビンゴをリセットする" />
			</form>
		</div>
	</body>
</html>
