
/**
 * requestAnimationFrame
 */
(function (w, r) {
	w['r'+r] = w['r'+r] || w['webkitR'+r] || w['mozR'+r] || w['msR'+r] || w['oR'+r] || function(c){ w.setTimeout(c, 1000 / 60); };
})(window, 'equestAnimationFrame');

var Utils = (function() {
    return {
        /**
         * H:i:s 形式で返す
         * ex.) 01:23:45
		 * @params {number} raw ミリ秒の数値データ
         */
        timeHis: function(raw) {
            var s = parseInt(raw / 1000, 10);
            var ms = raw - s * 1000;
            var h = parseInt(s / 60, 10) % 60;
            //console.log('raw', raw, 'ms', ms, 's', s, 'h', h);

            var time =
                ( '0' + parseInt(s / 3600, 10) ).slice(-2) +
                ':' +
                ( '0' + h ).slice(-2) +
                ':' +
                ( '0' + (s % 60) ).slice(-2);
            return time;
        },
		showCoords: function(targetEl) {
			var elm = $('<div/>').appendTo($('body'));
			var canvasElm = $(targetEl);
			elm.text("0, 0");
			canvasElm.mousemove(function(e) {
				elm.text(e.offsetX + ", " + e.offsetY);
			});

		}
    };
})();

var Net = (function() {
    var baseUrl = window.settings.server_url;
//    var baseUrl = 'http://bingodetempo.appspot.com/';
//    var baseUrl = 'http://localhost:8888/';

	function get(url, params, done, fail, always) {
		console.log('request=', url);
		var ajax = (params) ? $.get(url, params) : $.get(url);
		ajax.done(function(data, textStatus, jqXHR) {
			data = (typeof data === 'string') ? JSON.parse(data) : data;
			console.log('done, data', data);
			if (data.status === 'ng') {
				if (fail) {
					fail.apply(fail, [jqXHR, textStatus, new Error()]);
				}
			}
			else if (done) {
				done.apply(done, arguments);
			}
		}).fail(function(jqXHR, textStatus, errorThrown) {
			console.log('fail, data', textStatus);
			if (fail) {
				fail.apply(fail, arguments);
			}
		}).always(function() {
			if (always) {
				always.apply(always, arguments);
			}
		});
	}

    return {
        init: function(baseUrl_) {
            if (!baseUrl) {
                throw 'baseUrlが指定されていません。';
            }
            baseUrl = baseUrl_;
        },

        /**
         * ビンゴを回す
         */
        lot: function(done, fail) {
            var url = baseUrl + 'json/Lot';
			get(url, null, function(data) {
				console.log('lot');
				done(data);
			}, function() {
				fail();
			});
        }
    };
})();
