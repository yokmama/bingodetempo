
/**
 * 画像のプリロードを行います。
 */
var Preload = (function() {
	var imagePathes = [];
	var images = [];
	var loaded = [];
	var ip = 0;
	var onSuccess;

	// クロスブラウザにaddEventListener
	function addEvent(target, type, listener) {
		//@cc_on
		target./*@if (@_jscript_version < 5.9) attachEvent('on' +
			@else@*/addEventListener(/*@end@*/type, listener, false);
	}

	function checkLoaded(elm) {
		loaded[ip++] = true;
		console.log(elm);

		var loadedFlg = true;
		for(var j=0, len = images.length; j<len; j++){
			if(!loaded[j]){
				loadedFlg = false;
				break;
			}
		}
		if (loadedFlg) {
			// 全部チェックできていればcallback実行
			onSuccess(images);
		}
	}

	return {
		images: function(imgs, callback) {
			imagePathes = imgs;
			onSuccess = callback;

            var loadedCallback = function() {
                checkLoaded(this);
            };

			var nowMillis = new Date().getTime();
			for(var i=0, len = imagePathes.length; i<len; i++) {
				loaded[i] = false;
				var img = new Image();
				img.src = imagePathes[i] + '?' + nowMillis;
				images[i] = img;

				addEvent(images[i], "load", loadedCallback);
			}
		}
	};
})();
