

/**
 * Bingo
 */
var BingoEvents = (function() {
	var SPEED = 5;
	var ctx;

	var f = function(context, imgs, endCallback) {
		console.log('constructor : BingoEvents');
		ctx = context;
		this.images = imgs;
		this.endCallback = endCallback;
		this.theta = 0.0;
		this.rotationCount = 0;
		this.num = 0;

		ctx.font = "bold 12px sans-serif";

		/** scene num */
		this.scene = 0;

		// mapping
		this.handleImg = this.images[0];
		this.ballImg = this.images[1];
		this.bodyImg = this.images[2];
		this.baseImg = this.images[3];
	};

	f.prototype.looper = function() {
		this.clear();
		if (this.scene === 0) {
			this.draw();
		} else if (this.scene === 1) {
			this.ballEvent();
			this.draw();
		} else if (this.scene === 2) {
			this.ballRollEvent();
			this.draw();
		}
	};

    f.prototype.clear = function() {
		ctx.save();

		ctx.beginPath();
		ctx.clearRect(0, 0, canvas.width, canvas.height);

		ctx.restore();
    };

	f.prototype.draw = function() {
		var handleImg = this.images[0];
		var ballImg = this.images[1];
		var bodyImg = this.images[2];
		var baseImg = this.images[3];

		ctx.save();

		drawCircle(this.theta, bodyImg);

		ctx.restore();
		ctx.save();

		ctx.drawImage(baseImg, 82, 220);

		ctx.restore();
		ctx.save();

		drawHandle(this.theta, handleImg);

		ctx.restore();

		if (this.scene === 0) {
			// 現在このシーンである場合
			this.speedUp();
			if (this.rotationCount >= 2) {
				console.log('start ball');
				this.scene++;
			}
		} else {
		}
	};
	f.prototype.speedUp = function() {
		this.theta += SPEED * Math.PI / 180;
		if (Math.PI * 2 < this.theta) {
			this.theta = 0;
			this.rotationCount++;
		}
//		console.log("theta" + this.theta);
	};

	/**
	 * after #draw
	 */
	f.prototype.ballEvent = function() {
		if (!this.ballBehavior) {
			var self = this;
			var ballImg = this.ballImg;
            var metrics = ctx.measureText(this.num);
            var numWidth = metrics.width;
			this.ballBehavior = new MoveStraight({
				sx: 421,
				sy: 325,
				dx: 333,
				dy: 474 - this.ballImg.height
			}, 0.5);
			this.ballBehavior.draw = function(c) {
                ctx.save();

				ctx.translate(c.sx + c.mx, c.sy + c.my);
				ctx.drawImage(ballImg, 0, 0);
                ctx.fillStyle = '#FFF';
//                console.log('ball', 'w', self.ballImg.width, 'h', self.ballImg.height,
//                           'tw', numWidth);
				ctx.fillText(self.num,
                             (self.ballImg.width/2) - (numWidth/2),
                             (self.ballImg.height/2) + 5);

                ctx.restore();
			};
			this.ballBehavior.callback = function() {
                self.scene++;
			};
		}
		this.ballBehavior.tick();
	};

    function speedUp(theta, speed) {
		theta += speed * Math.PI / 180;
		if (Math.PI * 2 < theta) {
			theta = 0;
		}
        return theta;
    }

	/**
	 * after #ballEvent
	 */
	f.prototype.ballRollEvent = function() {
		if (!this.ballRollBehavior) {
			var self = this;
            var theta = 0.0;

            var metrics = ctx.measureText(this.num);
            var numWidth = metrics.width;

			this.ballRollBehavior = new MoveStraight({
				sx: this.ballBehavior.coords.dx,
				sy: this.ballBehavior.coords.dy,
				dx: 179,
				dy: 474 - this.ballImg.height
			}, 0.5);
            this.ballRollBehavior.draw = function(c) {
                ctx.save();

                ctx.translate(c.sx + c.mx, c.sy + c.my);
                ctx.drawImage(self.ballImg, 0, 0);
                ctx.fillStyle = '#FFF';

                if (!this.isFinished) {
                    theta = speedUp(theta, -SPEED);
                }
                ctx.translate((self.ballImg.width/2), (self.ballImg.height/2));
                ctx.rotate(theta);
                ctx.translate(-(self.ballImg.width/2), -(self.ballImg.height/2));
                ctx.fillText(self.num,
                             (self.ballImg.width/2) - (numWidth/2),
                             (self.ballImg.height/2) + 5);

                ctx.restore();
			};
			this.ballRollBehavior.callback = function() {
				console.log('end ballEvent');

				// initまで少し待つ
				setTimeout(function() {
					self.endCallback();
				}, 2000);
			};
		}
		this.ballRollBehavior.tick();
	};

	var Scale = (function() {
        var f = function() {
        };
        f.prototype.draw = function() {
        };
        return f;
    })();

	/**
	 * まっすぐ進むオブジェクトを定義する
	 * @param {Image} 
	 * @param {object} 行き先を指定 { sx, sy, dx, dy }
	 * @param {number} スピードを指定
	 */
	var MoveStraight = (function() {
		var f = function(coords, speed) {
			this.coords = coords;
			this.speed = speed;

			this.callback = null;
			this.isFinished = false;

			// 現在の位置からの差分
			this.coords.mx = 0;
			this.coords.my = 0;

			// 増分
			var a = Math.abs(coords.dx - coords.sx);
			var b = Math.abs(coords.dy - coords.sy);
			var distance = Math.sqrt( a^2 + b^2 );
			console.log('distance=' + distance);
			var ratio = speed / distance;
			this.coords.px = (coords.dx - coords.sx) * ratio;
			this.coords.py = (coords.dy - coords.sy) * ratio;
			console.log('px=' + this.coords.px + ', py=' + this.coords.py);
		};
		f.prototype.tick = function() {
			var c = this.coords;

			//console.log('mx=' + c.mx + ', my=' + c.my);

			if (this.draw) {
				this.draw(c);
			}

			var isFinished = false;
			if (this.isFinished) {
//				console.log('already finished!');
				return;
			} else {
				// 値増加
				c.mx += c.px;
				c.my += c.py;

				// 終了処理
				isFinished = (function(c) {
					var isX = false;
					if (c.px > 0) {
						isX = (c.sx + c.mx) > c.dx;
					} else {
						isX = (c.sx + c.mx) <= c.dx;
					}
					var isY = false;
					if (c.py > 0) {
						isY = (c.sy + c.my) > c.dy;
					} else {
						isY = (c.sy + c.my) <= c.dy;
					}
					if (isX && isY) {
						return true;
					}
                    console.log('isX', isX, 'isY', isY);
					return false;
				})(c);
			}
			if (isFinished && !this.isFinished) {
				// 初めてFinishedになった時に一度だけ来る
				this.isFinished = true;
				if (this.callback) {
					this.callback();
				}
			}
		};
		return f;
	})();
	
	// http://tech.kayac.com/archive/canvas-tutorial.html
	function drawCircle(theta, img) {
		// 回転の中心は原点なので、
		// 一度図形の中心を原点に移してから回転させて
		// 元の場所に戻す
		ctx.translate(519, 243);
		ctx.rotate(theta);
		ctx.translate(-img.width / 2, -img.height / 2);
		ctx.drawImage(img, 0, 0);
	}

	function drawHandle(theta, img) {
		ctx.translate(519, 243);
		ctx.rotate(theta);
		ctx.translate(-17, -17);
		ctx.drawImage(img, 0, 0);
	}

	return f;
})();
