
/**
 * Main
 */
var Main = (function() {
	var ctx;
	var images;
	var canvas;
	var isLooping = false;
	var bingo;
	var timerId = -1;
	var lotCache = null;

	/**
	 * 前回値と比較し、回した状態になっているかどうかをチェックします。
	 * @return {boolean} 前回と比較し、回していればtrueを返します。
	 */
	function isLotted(lot) {
		if (lotCache === null || !lot) {
			console.log('それぞれのデータが空の場合', lotCache, lot);
			return false;
		}
		if (lotCache.num != lot.num) {
			return true;
		}
		return false;
	}

	/**
	 * サーバに対して試しにビンゴのチェックをしてみる。
	 */
	function tryLot() {
		var lotTimeEl = $('#lot-time');

		Net.lot(function(data) {
			var isFirstTime = (lotCache === null);

			// 番号リスト更新
			var numbersEl = $('#numbers').empty();
			$.each(data.numbers, function(i, o) {
				$('<li/>').text(o).appendTo(numbersEl);
			});
//			if (window.settings.debug) {
//				$('<li/>').text(11).appendTo($('#numbers'));
//				$('<li/>').text(22).appendTo($('#numbers'));
//				$('<li/>').text(33).appendTo($('#numbers'));
//			}

			if (!isLotted(data)) {
				console.log('前と同じ番号の場合');
				if (data.numbers.length <= 0) {
					console.log('ひくbingoがなかった時。 指定時間後にリトライする');
					var nextRetry = data.next - data.now;
					//					var nextRetry = 3 * 60 * 1000;
					var nextRetryId = setInterval(function() {
						nextRetry -= 1000;
						lotTimeEl.text('抽選券待ちです…。' + Utils.timeHis(nextRetry));
						//console.log(nextRetry);
						if (nextRetry <= 0) {
							clearInterval(nextRetryId);
							tryLot();
						}
					}, 1000);

					lotCache = data;
					return;
				} else {
					console.log('次の抽選待ち状態');
				}
			} else {
				console.log('抽選がきた！ num=', data.num);
			}

			lotCache = data;

			console.log('check timer');
			// 次に回す時刻をチェックしてタイマーをセット
			var nowMillis = data.now;
			var nextMillis = data.next - data.now;

			if (timerId !== -1) {
				clearInterval(timerId);
			}
			timerId = setInterval(function() {
				nextMillis -= 1000;
				var str = Utils.timeHis(nextMillis);
				// console.log('time=' + str);
				lotTimeEl.text('次の抽選まで、あと' + str);
				if (nextMillis <= 0) {
					console.log('次に回す時間', str);
					clearInterval(timerId);
					timerId = -1;
					tryLot();
				}
			}, 1000);

			if (isFirstTime) {
				// 最初のlot
				// 時間だけ回して、待ち状態にしている
				console.log('first lot');
			} else {
				if (!isLooping && bingo) {
					isLooping = true;
					bingo.num = data.num;
					looper();
				} else {
					console.log('ループ中、もしくは初期化が終わっていません。');
				}
			}
		}, function() {
			// 失敗したら５秒後にリトライ
			console.log('通信失敗');
			lotTimeEl.text('取得に失敗しました。リトライしています…。');
			setTimeout(function() {
				console.log('リトライ');
				tryLot();
			}, 5000);
		});
	}

	function setEvents() {
	}

	/**
	 * Bingoの表示を初期化
	 */
	function initBingo() {
		// object 初期化
		bingo = new BingoEvents(ctx, images, end);
		console.log('start draw');
		bingo.draw();
	}

	function looper() {
		if (isLooping) {
			window.requestAnimationFrame(looper);
		} else {
			return;
		}

		bingo.looper();
	}

	/**
	 * ビンゴ引き終わった時
	 */
	function end() {
		console.log('end callback');
		isLooping = false;
		initBingo();
//        startTimer();
	}

	return {
		init: function() {
			// provide canvas
			canvas = $('#canvas')[0];
			if ( ! canvas || ! canvas.getContext ) {
				return false;
			}
			ctx = canvas.getContext('2d');

			setEvents();

			if (window.settings.debug) {
				Utils.showCoords($('#canvas')[0]);
			}

			Preload.images([
				'/img/kuji-handle.png',
				'/img/kuji-ball.png',
				'/img/kuji-body.png',
				'/img/kuji-base.png'
			], function(imgs) {
				images = imgs;
				initBingo();

				tryLot();
			});
		}
	};
})();

$(function() {
	Main.init();
});
