package jp.yokmama.slim3.bingodetemp.controller;

import jp.yokmama.slim3.bingodetemp.controller.StoreImportController;

import org.slim3.tester.ControllerTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class StoreImportControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        tester.start("/StoreImport");
        StoreImportController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
    }
}
