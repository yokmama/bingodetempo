package jp.yokmama.slim3.bingodetemp.controller.mnt;

import jp.yokmama.slim3.bingodetemp.controller.mnt.RunbackendController;

import org.slim3.tester.ControllerTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class RunbackendControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        tester.start("/mnt/runbackend");
        RunbackendController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
    }
}
