package jp.yokmama.slim3.bingodetemp.controller.json;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import jp.yokmama.slim3.bingodetemp.controller.json.SetStoreController;
import jp.yokmama.slim3.bingodetemp.meta.ParticipationMeta;
import jp.yokmama.slim3.bingodetemp.model.Participation;
import jp.yokmama.slim3.bingodetemp.model.Store;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;
import jp.yokmama.slim3.bingodetemp.utils.Utils;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.ModelRef;
import org.slim3.datastore.ModelRefAttributeMeta;
import org.slim3.tester.ControllerTestCase;

import com.google.appengine.api.datastore.Key;

public class SetStoreControllerTest extends ControllerTestCase {
    private final static Logger LOG = Logger
        .getLogger(SetStoreControllerTest.class
            .getName());

    DatabaseService service = new DatabaseService();

//    @Test
    public void run() throws Exception {
        tester.start("/json/SetStore");
        SetStoreController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));

        // TODO 参加日の変更
        String reqStoreId = tester.param("storeId");
        reqStoreId = "280781729";
        LOG.info("reqStoreId=" + reqStoreId);

        long storeId = Long.parseLong(reqStoreId);
        Key key = Datastore.createKey(Store.class, storeId);

        // Store existingStore = service.getStore(key);

        // TODO 参加日の変更
        ParticipationMeta participationMeta = new ParticipationMeta();
        ModelRefAttributeMeta<Participation, ModelRef<Store>, Store> storeRef =
            participationMeta.storeRef;
        List<Participation> list =
            Datastore.query(Participation.class).filter(
                storeRef.equal(key)).asList();

        LOG.info("storeId=" + storeId);
    }

    // @Test
    public void getParticipant() throws Exception {
        // TODO 参加日の変更
        String reqStoreId = "280781729";
        LOG.info("partId=" + reqStoreId);

        long storeId = Long.parseLong(reqStoreId);
        Key key = Datastore.createKey(Store.class, storeId);

        ParticipationMeta participationMeta = new ParticipationMeta();
        ModelRefAttributeMeta<Participation, ModelRef<Store>, Store> storeRef =
            participationMeta.storeRef;
        List<Store> list =
            Datastore.query(Store.class).filter(
                storeRef.equal(key)).asList();

        LOG.info("storeId=" + storeId);
    }

    @Test
    public void testModelName() throws Exception {
        String name = ParticipationMeta.get().storeRef.getName();

        assertEquals("storeRef", name);
    }

    @Test
    public void testUseParseDateUtil() throws Exception {
        Date parseDate = Utils.parseDate("2012/10/10");

        Calendar instance = Calendar.getInstance(Utils.jpnTz);
        instance.setTime(parseDate);

        assertEquals(2012, instance.get(Calendar.YEAR));
        assertEquals(9, instance.get(Calendar.MONTH));
        assertEquals(10, instance.get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void testDateFormat() throws Exception {
        Date parse = null;
        try {
            parse = DateFormat.getDateInstance().parse("2012/10/10");
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }

        assertThat(parse, is(notNullValue()));
        
        Calendar instance = Calendar.getInstance(Utils.jpnTz);
        instance.setTime(parse);

        assertEquals(2012, instance.get(Calendar.YEAR));
        assertEquals(9, instance.get(Calendar.MONTH));
        assertEquals(10, instance.get(Calendar.DAY_OF_MONTH));
    }

    // @Test
    public void testRelation() {
        Store store = new Store();
        store.setTitle("hoge");

        Participation participation = new Participation();
        participation.setSection("KYKK1");
        participation.getStoreRef().setModel(store);

        Participation participation2 = new Participation();
        participation2.setSection("KYKK2");
        participation2.getStoreRef().setModel(store);

        Datastore.put(store, participation, participation2);

        Store store2 = Datastore.get(Store.class, store.getKey());

        List<Participation> modelList =
            store2.getParticipationsRefs().getModelList();

        assertEquals(2, modelList.size());

        return;
    }

    // @Test
    public void testUpdateParticipation() {
        Store store = new Store();
        store.setTitle("hoge");

        Calendar cal = Calendar.getInstance(Utils.jpnTz);
        cal.set(Calendar.MONTH, 9);
        cal.set(Calendar.DAY_OF_MONTH, 5);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date date = cal.getTime();

        String section = "KYKK";
        String sectiondate = section + Utils.getfmtYYYMMDD().format(date);
        Date updateDate = Calendar.getInstance(Utils.jpnTz).getTime();

        // Key key =
        // Participation.createKey("KYKK", store.getTitle(), date);
        Participation participation = new Participation();
        // participation.setKey(key);
        participation.setSection(section);
        participation.setSectiondate(sectiondate);
        participation.setDate(date);
        participation.setUpdateDate(updateDate);
        participation.setService(store.getBudget());
        participation.setDesc(store.getDesc());
        participation.getStoreRef().setModel(store);

        Datastore.put(store, participation);

        store = Datastore.get(Store.class, store.getKey());

        List<Participation> participations =
            store.getParticipationsRefs().getModelList();

        assertEquals(1, participations.size());
        assertEquals(2005, participations.get(0).getSectiondate());

        return;
    }

    // @Test
    public void testStore() throws Exception {
        int count = tester.count("Store");
        LOG.info("count=" + count);

        // TODO 参加日の変更
        long storeId = -2138056527L;
        LOG.info("storeId=" + storeId);
        Key key = Datastore.createKey(Store.class, storeId);
        Store store = service.getStore(key);

        assertThat(store, is(notNullValue()));

        LOG.info("storeId=" + storeId);
    }

}
