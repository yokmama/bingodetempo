package jp.yokmama.slim3.bingodetemp.controller.json;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.logging.Logger;

import jp.yokmama.slim3.bingodetemp.controller.json.SetProfileController;
import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;
import jp.yokmama.slim3.bingodetemp.service.DatabaseService;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.ControllerTestCase;

import com.google.appengine.api.datastore.Key;

public class SetProfileControllerTest extends ControllerTestCase {
    private final static Logger LOG = Logger
        .getLogger(SetProfileControllerTest.class.getName());

    private DatabaseService service = new DatabaseService();

    @Test
    public void run() throws Exception {
        tester.start("/json/SetProfile");
        SetProfileController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
    }

    @Test
    public void testSetProfile() throws Exception {
        PrimarilyUser profile = new PrimarilyUser();
        profile.setNickname("nickname");
        profile.setSex(0);
        profile.setBloodType(0);
        profile.setShowRange(0);
        profile.setAppeal("appeal");

        Datastore.put(profile);

        PrimarilyUser profile2 = Datastore.get(PrimarilyUser.class, profile.getKey());

        assertEquals("nickname", profile2.getNickname());
    }

    @Test
    public void testInputProfile() throws Exception {
    }

    @Test
    public void testKey() throws Exception {
        String reqKey = "test_key";
        Key userKey = Datastore.createKey(PrimarilyUser.class, reqKey);
        System.out.println(userKey);

        PrimarilyUser user = service.getPrimarilyUser(userKey);
        if (user == null) {
            user = new PrimarilyUser();
            user.setKey(userKey);
        }

        assertThat(user, is(notNullValue()));
    }
}
