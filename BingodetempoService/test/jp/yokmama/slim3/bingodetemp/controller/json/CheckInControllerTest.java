package jp.yokmama.slim3.bingodetemp.controller.json;

import jp.yokmama.slim3.bingodetemp.controller.json.CheckInController;

import org.slim3.tester.ControllerTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class CheckInControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        tester.start("/json/CheckIn");
        CheckInController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
    }
}
