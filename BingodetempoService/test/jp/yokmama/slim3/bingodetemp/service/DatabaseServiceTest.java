package jp.yokmama.slim3.bingodetemp.service;

import jp.yokmama.slim3.bingodetemp.service.DatabaseService;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class DatabaseServiceTest extends AppEngineTestCase {

    private DatabaseService service = new DatabaseService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
