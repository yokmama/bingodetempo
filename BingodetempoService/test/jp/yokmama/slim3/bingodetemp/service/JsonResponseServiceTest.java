package jp.yokmama.slim3.bingodetemp.service;

import jp.yokmama.slim3.bingodetemp.service.JsonResponseService;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class JsonResponseServiceTest extends AppEngineTestCase {

    private JsonResponseService service = new JsonResponseService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
