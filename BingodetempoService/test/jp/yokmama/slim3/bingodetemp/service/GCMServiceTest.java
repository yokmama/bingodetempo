package jp.yokmama.slim3.bingodetemp.service;

import jp.yokmama.slim3.bingodetemp.service.GCMService;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class GCMServiceTest extends AppEngineTestCase {

    private GCMService service = new GCMService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
