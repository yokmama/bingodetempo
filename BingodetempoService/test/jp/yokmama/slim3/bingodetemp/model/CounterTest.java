package jp.yokmama.slim3.bingodetemp.model;

import jp.yokmama.slim3.bingodetemp.model.Counter;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class CounterTest extends AppEngineTestCase {

    private Counter model = new Counter();

    @Test
    public void test() throws Exception {
        assertThat(model, is(notNullValue()));
    }
}
