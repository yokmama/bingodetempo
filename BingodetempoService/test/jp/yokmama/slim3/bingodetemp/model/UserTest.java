package jp.yokmama.slim3.bingodetemp.model;

import jp.yokmama.slim3.bingodetemp.model.PrimarilyUser;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class UserTest extends AppEngineTestCase {

    private PrimarilyUser model = new PrimarilyUser();

    @Test
    public void test() throws Exception {
        assertThat(model, is(notNullValue()));
    }
}
